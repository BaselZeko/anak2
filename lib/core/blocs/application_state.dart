part of 'application_bloc.dart';

enum AppStatus { setup, ready }

class ApplicationState {
  AppLanguages appLanguages;
  AppStatus appStatus;
  UserRole userRole;

  ApplicationState(
      {this.appLanguages = AppLanguages.EN,
      this.appStatus = AppStatus.setup,
      this.userRole = UserRole.guest});

  ApplicationState copyWith({AppLanguages appLanguages, AppStatus appStatus, UserRole userRole}) {
    return ApplicationState(
        appLanguages: appLanguages ?? this.appLanguages,
        appStatus: appStatus ?? this.appStatus,
        userRole: userRole ?? this.userRole);
  }
}
