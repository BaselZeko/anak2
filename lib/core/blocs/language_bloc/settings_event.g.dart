// GENERATED CODE - DO NOT MODIFY BY HAND

part of settings_event;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$SetAppLanguage extends SetAppLanguage {
  @override
  final AppLanguages language;

  factory _$SetAppLanguage([void Function(SetAppLanguageBuilder) updates]) =>
      (new SetAppLanguageBuilder()..update(updates)).build();

  _$SetAppLanguage._({this.language}) : super._() {
    if (language == null) {
      throw new BuiltValueNullFieldError('SetAppLanguage', 'language');
    }
  }

  @override
  SetAppLanguage rebuild(void Function(SetAppLanguageBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SetAppLanguageBuilder toBuilder() =>
      new SetAppLanguageBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SetAppLanguage && language == other.language;
  }

  @override
  int get hashCode {
    return $jf($jc(0, language.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SetAppLanguage')
          ..add('language', language))
        .toString();
  }
}

class SetAppLanguageBuilder
    implements Builder<SetAppLanguage, SetAppLanguageBuilder> {
  _$SetAppLanguage _$v;

  AppLanguages _language;
  AppLanguages get language => _$this._language;
  set language(AppLanguages language) => _$this._language = language;

  SetAppLanguageBuilder();

  SetAppLanguageBuilder get _$this {
    if (_$v != null) {
      _language = _$v.language;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SetAppLanguage other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$SetAppLanguage;
  }

  @override
  void update(void Function(SetAppLanguageBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SetAppLanguage build() {
    final _$result = _$v ?? new _$SetAppLanguage._(language: language);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
