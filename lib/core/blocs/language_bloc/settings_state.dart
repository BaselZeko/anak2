library settings_state;

import 'package:built_value/built_value.dart';
import 'package:anak/core/localization/app_lang.dart';

part 'settings_state.g.dart';

abstract class ChangeLangugeState
    implements Built<ChangeLangugeState, ChangeLangugeStateBuilder> {

  String get error;
  AppLanguages get appLanguage;
  bool get isLoading;

  ChangeLangugeState._();

  factory ChangeLangugeState([updates(ChangeLangugeStateBuilder b)]) =
  _$ChangeLangugeState;

  factory ChangeLangugeState.init() {
    return ChangeLangugeState((b) => b
      ..error = ''
      ..appLanguage = AppLanguages.EN
      ..isLoading = false
    );
  }
}
