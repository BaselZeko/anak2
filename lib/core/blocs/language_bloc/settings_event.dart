library settings_event;

import 'package:built_value/built_value.dart';
import 'package:anak/core/localization/app_lang.dart';

part 'settings_event.g.dart';

abstract class SettingsEvent {}

abstract class SetAppLanguage extends SettingsEvent
    implements Built<SetAppLanguage, SetAppLanguageBuilder> {

  AppLanguages get language;

  SetAppLanguage._();

  factory SetAppLanguage([updates(SetAppLanguageBuilder b)]) = _$SetAppLanguage;
}