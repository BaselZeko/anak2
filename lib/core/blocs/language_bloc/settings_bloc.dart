import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:anak/core/blocs/language_bloc/settings_event.dart';
import 'package:anak/core/blocs/language_bloc/settings_state.dart';
import 'package:anak/core/localization/app_lang.dart';
import 'package:anak/core/services/localization_storage.dart';

class SettingsBloc extends Bloc<SettingsEvent, ChangeLangugeState> {
  SettingsBloc(this.localStorageService) : super(ChangeLangugeState.init());

  LocalStorageService localStorageService;

  @override
  Stream<ChangeLangugeState> mapEventToState(SettingsEvent event) async* {
    if (event is SetAppLanguage) {
      await localStorageService.setAppLanguage(event.language);
      globalLanguage = event.language;
      changeLanguage(event.language.index);
      yield state.rebuild((b) => b..appLanguage = mapIntToLang(globalLanguage.index));
    }
  }
}
