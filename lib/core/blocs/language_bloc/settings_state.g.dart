// GENERATED CODE - DO NOT MODIFY BY HAND

part of settings_state;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ChangeLangugeState extends ChangeLangugeState {
  @override
  final String error;
  @override
  final AppLanguages appLanguage;
  @override
  final bool isLoading;

  factory _$ChangeLangugeState(
          [void Function(ChangeLangugeStateBuilder) updates]) =>
      (new ChangeLangugeStateBuilder()..update(updates)).build();

  _$ChangeLangugeState._({this.error, this.appLanguage, this.isLoading})
      : super._() {
    if (error == null) {
      throw new BuiltValueNullFieldError('ChangeLangugeState', 'error');
    }
    if (appLanguage == null) {
      throw new BuiltValueNullFieldError('ChangeLangugeState', 'appLanguage');
    }
    if (isLoading == null) {
      throw new BuiltValueNullFieldError('ChangeLangugeState', 'isLoading');
    }
  }

  @override
  ChangeLangugeState rebuild(
          void Function(ChangeLangugeStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ChangeLangugeStateBuilder toBuilder() =>
      new ChangeLangugeStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ChangeLangugeState &&
        error == other.error &&
        appLanguage == other.appLanguage &&
        isLoading == other.isLoading;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc(0, error.hashCode), appLanguage.hashCode), isLoading.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ChangeLangugeState')
          ..add('error', error)
          ..add('appLanguage', appLanguage)
          ..add('isLoading', isLoading))
        .toString();
  }
}

class ChangeLangugeStateBuilder
    implements Builder<ChangeLangugeState, ChangeLangugeStateBuilder> {
  _$ChangeLangugeState _$v;

  String _error;
  String get error => _$this._error;
  set error(String error) => _$this._error = error;

  AppLanguages _appLanguage;
  AppLanguages get appLanguage => _$this._appLanguage;
  set appLanguage(AppLanguages appLanguage) =>
      _$this._appLanguage = appLanguage;

  bool _isLoading;
  bool get isLoading => _$this._isLoading;
  set isLoading(bool isLoading) => _$this._isLoading = isLoading;

  ChangeLangugeStateBuilder();

  ChangeLangugeStateBuilder get _$this {
    if (_$v != null) {
      _error = _$v.error;
      _appLanguage = _$v.appLanguage;
      _isLoading = _$v.isLoading;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ChangeLangugeState other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ChangeLangugeState;
  }

  @override
  void update(void Function(ChangeLangugeStateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ChangeLangugeState build() {
    final _$result = _$v ??
        new _$ChangeLangugeState._(
            error: error, appLanguage: appLanguage, isLoading: isLoading);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
