part of 'application_bloc.dart';

@immutable
abstract class ApplicationEvent {}

class InitEvent extends ApplicationEvent {}
