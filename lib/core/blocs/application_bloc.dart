import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:anak/core/localization/app_lang.dart';
import 'package:anak/core/param/no_param.dart';
import 'package:anak/core/resources/constants.dart';

import 'package:anak/core/resources/globals.dart';
import 'package:anak/core/services/device_uid_provider.dart';
import 'package:anak/core/services/localization_storage.dart';
import 'package:anak/core/services/user_local_management.dart';

import '../../di.dart';

part 'application_event.dart';

part 'application_state.dart';

class ApplicationBloc extends Bloc<ApplicationEvent, ApplicationState> {
  LocalStorageService localStorageService;
  UserLocalManagement userLocalManagement;

  ApplicationBloc(this.localStorageService, this.userLocalManagement)
      : super(ApplicationState(appLanguages: AppLanguages.EN));

  @override
  Stream<ApplicationState> mapEventToState(
    ApplicationEvent event,
  ) async* {
    if (event is InitEvent) {
      final language = await localStorageService.getAppLanguage();
      changeLanguage(language);
      UserRole userRole = await UserLocalManagement.checkUserRole();
      print("userRolr$userRole");
      yield state.copyWith(
          appLanguages: mapIntToLang(language), appStatus: AppStatus.ready, userRole: userRole);
    }
  }
  void initApp() {
    add(InitEvent());
  }
}
