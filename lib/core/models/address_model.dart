import 'location_model.dart';

class AddressModel {
  final String addressLine;
  final String countryName;

  final String countryCode;

  final String featureName;

  final String postalCode;

  final String adminArea;

  final String subAdminArea;

  final String locality;

  final String subLocality;

  final String thoroughfare;

  final String subThoroughfare;

  final LocationModel position;

  AddressModel(
      {this.addressLine,
      this.countryName,
      this.countryCode,
      this.featureName,
      this.postalCode,
      this.adminArea,
      this.subAdminArea,
      this.locality,
      this.subLocality,
      this.thoroughfare,
      this.position,
      this.subThoroughfare});
}
