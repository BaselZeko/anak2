class LocationModel {
  String lat;
  String long;

  LocationModel({this.lat, this.long});

  LocationModel.fromJson(Map<String, dynamic> json) {
    lat = json['lat'];
    long = json['long'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['lat'] = lat;
    data['long'] = long;

    return data;
  }
}
