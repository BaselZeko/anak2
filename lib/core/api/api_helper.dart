import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:anak/core/enums/api/HttpMethod.dart';
import 'package:anak/core/error/base_error.dart';
import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:anak/core/error/base_error.dart';
import 'package:anak/core/error/connection/socket_error.dart';
import 'package:anak/core/error/connection/unknown_error.dart';
import 'package:anak/core/factories/model_factory.dart';

import 'handler.dart';

class ApiHelper with ErrorHandler {
  Future<Either<BaseError, T>> sendRequest<T>({
    @required HttpMethod method,
    @required String url,
    Map<String, dynamic> data,
    Map<String, dynamic> queryParams,
    Map<String, String> headers,
    CancelToken cancelToken,
  }) async {
    assert(method != null);
    assert(url != null);

    try {
      Response response;
      print('canelToken: ${cancelToken}');
      switch (method) {
        case HttpMethod.GET:
          print("GET [$url]");
          response = await Dio().get(
            url,
            queryParameters: data,
            options: Options(
                headers: headers,
                followRedirects: false,
                validateStatus: (status) {
                  return status < 500;
                }),
            cancelToken: cancelToken,
          );
          break;

        case HttpMethod.POST:
          print("POST [$url] data : [$data] [${json.encode(data)}]");
          /*  final response1= await http.post(
            Uri.parse(url),
            body: data
          );
          print('response: ${response1.body}');*/
          response = await Dio().post(
            url,
            data: data,
            options: Options(
                headers: headers,
                followRedirects: false,
                validateStatus: (status) {
                  return status < 500;
                }),
            cancelToken: cancelToken,
          );

          break;
        case HttpMethod.PUT:
          print("PUT [$url]");

          response = await Dio().put(
            url,
            data: data,
            queryParameters: data,
            options: Options(
                headers: headers,
                followRedirects: false,
                validateStatus: (status) {
                  return status < 500;
                }),
            cancelToken: cancelToken,
          );
          break;
        case HttpMethod.DELETE:
          print("DELETE [$url] data : [$data] [${json.encode(data)}]");
          response = await Dio().delete(
            url,
            data: data,
            queryParameters: data,
            options: Options(
                headers: headers,
                followRedirects: false,
                validateStatus: (status) {
                  return status < 500;
                }),
            cancelToken: cancelToken,
          );
          break;
        default:
          break;
      }
      var decoded;
      log('123123123: ${response.data}');

      decoded =
          response.data is String ? json.decode(response.data) : response.data;

      //  Print.log(print: resposne.data);

      return Right(ModelFactory.getInstance().createModel<T>(decoded));
    } on DioError catch (e) {
      return Left(_handleError(e));
    } on SocketException catch (e) {
      return Left(SocketError());
    } catch (e) {
      print('e: ${e}');
      return Left(UnknownError());
    }
  }

  _handleError(error) => handleError(error);
}
