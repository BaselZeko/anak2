import 'package:anak/core/api/api_helper.dart';
import 'package:anak/core/enums/api/HttpMethod.dart';
import 'package:anak/core/error/base_error.dart';
import 'package:anak/core/error/custom_error.dart';
import 'package:anak/core/factories/model_factory.dart';
import 'package:anak/core/resources/constants.dart';
import 'package:anak/core/response/api_response.dart';
import 'package:flutter/foundation.dart';
import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

//import '../constants.dart';

abstract class RemoteDataSource {
  Future<Either<BaseError, Data>> request<Data, Response extends ApiResponse>({
    @required String responseStr,
    @required Response Function(dynamic) mapper,
    @required HttpMethod method,
    @required String url,
    Map<String, String> queryParameters,
    Map<String, dynamic> data,
    bool withAuthentication = false,
    String acceptLang = 'en',
    CancelToken cancelToken,
  }) async {
    assert(responseStr != null);
    assert(mapper != null);
    assert(method != null);
    assert(url != null);

    ModelFactory.getInstance().registerModel(responseStr, mapper);

    final Map<String, String> headers = {};

    // if (withAuthentication) {
    //   if (await UserRepository.hasToken) {
    //     final token = await UserRepository.authToken;
    //     print("token:$token");
    //     headers.putIfAbsent(HEADER_AUTH, () => 'Bearer $token');
    //   }
    // }
    headers.putIfAbsent(LANGUAGE, () => acceptLang);

    print('data: $data');

    final response = await ApiHelper().sendRequest<Response>(
        method: method,
        url: url,
        data: data,
        headers: headers,
        cancelToken: cancelToken);

    print('123 123 Response: $response');

    if (response.isLeft()) {
      return Left((response as Left<BaseError, Response>).value);
    } else if (response.isRight()) {
      final resValue = (response as Right<BaseError, Response>).value;
      print('has error : ${resValue.hasError}');
      if (resValue.hasError) return Left(CustomError(message: resValue.msg));
      return Right(resValue.result);
    }
    return null;
  }
}
