extension DateOnlyCompare on DateTime {
  bool isSameDate(DateTime other) {
    return year == other.year && month == other.month
        && day == other.day;
  }
}

extension GetDateFormat on DateTime {
 String getDateWithOutTime() {
 return toString().substring(0, toString().trim().indexOf(' '));
 }
}