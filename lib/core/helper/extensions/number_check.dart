extension NumberCheck on String {
  bool isDigit() {
    if (this == null || isEmpty) {
      return false;
    }
    final number = num.tryParse(this);
    return number != null;
  }
}
