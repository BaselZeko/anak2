import 'package:anak/core/localization/app_localization.dart';
import 'package:flutter/cupertino.dart';

extension Translation on String {
  String tr(BuildContext context) =>
      AppLocalizations.of(context).text(this);
}

