import 'package:flutter/material.dart';

class ScreensHelper {
  static double screenWidth;
  static double screenHeight;
  Orientation orientation;

  double width;
  double height;

  ScreensHelper(BuildContext context) {
    screenWidth = MediaQuery.of(context).size.width;
    screenHeight = MediaQuery.of(context).size.height;

  }

  ScreensHelper.of(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;
    orientation = MediaQuery.of(context).orientation;
  }

  double fixedWidth(double percent) {
    switch (orientation) {
      case Orientation.portrait:
        return (percent / 100.0) * width;
      case Orientation.landscape:
        return (percent / 100.0) * height;
    }
  }

  double fixedHeight(double percent) {
    switch (orientation) {
      case Orientation.portrait:
        return (percent / 100.0) * height;
      case Orientation.landscape:
        return (percent / 100.0) * width;

    }
  }

  static double fromDefaultWidth(double percent) {
    assert(percent != null && percent >= 0.0 && percent <= 100.0);
    return (percent / 100.0) * screenWidth;
  }

  static fromDefaultHeight(double percent) {
    assert(percent != null && percent >= 0.0 && percent <= 100.0);
    return ((percent / 100.0) * screenHeight);
  }

  double fromWidth(double percent) {
    assert(percent != null && percent >= 0.0 && percent <= 100.0);
    return (percent / 100.0) * width;
  }

  double fromHeight(double percent) {
    assert(percent != null && percent >= 0.0 && percent <= 100.0);
    return ((percent / 100.0) * height);
  }

/* static scaleText(double fontSize, {bool allowFontScalingSelf}) {
    return ScreenUtil().setSp(fontSize, allowFontScalingSelf: allowFontScalingSelf);
  }*/
}
