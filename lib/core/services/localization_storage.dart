import 'package:anak/core/localization/app_lang.dart';
import 'package:anak/core/resources/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LocalStorageService {
  static SharedPreferences prefs;

  static removeValues({String key}) async {
    prefs = await SharedPreferences.getInstance();
    prefs.remove(key);
  }

  static Future<bool> isExists(String key) async {
    prefs = await SharedPreferences.getInstance();
    return prefs.containsKey(key);
  }

  Future<void> setAppLanguage(AppLanguages value) async {
    (await SharedPreferences.getInstance()).setInt(APP_LANGUAGE, value.index);
  }

  Future<int> getAppLanguage() async {
    return (await SharedPreferences.getInstance()).getInt(APP_LANGUAGE) ??
        AppLanguages.EN.index;
  }
}