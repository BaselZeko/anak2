import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';

class DatePickerService {
  static Future<DateTime> showDatePicker(BuildContext context) async {
    DateTime chosenDateTime = DateTime.now();
    chosenDateTime = await showCupertinoModalPopup(
        context: context,
        builder: (_) => Container(
              height: 260,
              color: GlobalColors.datePickerBackGround,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: ScreensHelper.of(context).fromWidth(100),
                    height: 50,
                    margin: const EdgeInsets.only(bottom: 16),
                    // padding: EdgeInsets.all(8),
                    decoration: const BoxDecoration(
                        color: GlobalColors.white,
                        border: Border(
                            top: BorderSide(
                          color: GlobalColors.cupertinoBorder,
                        ))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        CupertinoButton(
                          child: Text('Cancel'),
                          onPressed: () => Navigator.of(context).pop(),
                        ),
                        CupertinoButton(
                          child: Text('Ok'),
                          onPressed: () =>
                              Navigator.of(context).pop(chosenDateTime),
                        )
                      ],
                    ),
                  ),
                  Container(
                    height: 180,
                    margin: EdgeInsets.only(bottom: 8),
                    child: CupertinoDatePicker(
                        mode: CupertinoDatePickerMode.date,
                        initialDateTime: DateTime.now(),
                        onDateTimeChanged: (val) {
                          chosenDateTime = val;
                        }),
                  ),

                  /* // Close the modal
                  CupertinoButton(
                    child: Text('OK'),
                    onPressed: () => Navigator.of(context).pop(),
                  ),*/
                ],
              ),
            ));

    return chosenDateTime;
  }
}
