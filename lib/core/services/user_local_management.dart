import 'dart:convert';

import 'package:anak/features/user_managment/domain/repositories/user_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:anak/core/models/location_model.dart';

const String userKey = 'USER_KEY';
const String cartKey = 'CART_KEY';
const String locationKey = 'LOCATION_KEY';
const String addressKey = 'ADDRESS_KEY';
const String addressNameKey = 'ADDRESS_NAME_KEY';
const String phoneNumber = 'PHONE_KEY';
const String locationPlace = 'PLACE';

enum UserRole { guest, normal, setLocation }

class UserLocalManagement {
  static final UserLocalManagement _singleton = UserLocalManagement._internal();
  SharedPreferences prefs;

  factory UserLocalManagement() {
    return _singleton;
  }

  UserLocalManagement._internal();

  Future<void> storeUser(Map<String, dynamic> user) async {
    prefs = await SharedPreferences.getInstance();
    final data = jsonEncode(user);
    prefs.setString(userKey, data);
  }

  Future<void> storePhone(phone) async {
    prefs = await SharedPreferences.getInstance();
    prefs.setString(phoneNumber, phone);
  }

  Future<String> getUserPhone() async {
    prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey(phoneNumber)) {
      return prefs.getString(phoneNumber);
    }
    return null;
  }

  Future<LocationModel> getUserLocation() async {
    prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey(locationKey)) {
      return LocationModel.fromJson(jsonDecode(prefs.getString(locationKey)));
    }
    return null;
  }

  Future<void> storeUserLocation(Map<String, dynamic> position) async {
    prefs = await SharedPreferences.getInstance();
    final data = jsonEncode(position);
    prefs.setString(locationKey, data);
  }

  Future<void> storeUserAddress(Map<String, dynamic> position) async {
    prefs = await SharedPreferences.getInstance();
    final data = jsonEncode(position);
    prefs.setString(addressKey, data);
  }





  Future<void> storeCartToken(Map<String, dynamic> cart) async {
    prefs = await SharedPreferences.getInstance();
    final data = jsonEncode(cart);
    prefs.setString(cartKey, data);
  }

  Future<void> saveLocationName(String name) async {
    prefs = await SharedPreferences.getInstance();
    prefs.setString(addressNameKey, name);
  }

  Future<String> getLocationName() async {
    prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey(addressKey)) {
      return prefs.getString(addressNameKey);
    }
    return '';
  }

  /*Future<LoginResultModel> getUser() async {
    prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey(userKey)) {
      return LoginResultModel.fromJson(jsonDecode(prefs.getString(userKey)));
    } else {
      return null;
    }
  }

  Future<AddToCartLocalModel> getCart() async {
    prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey(cartKey)) {
      return AddToCartLocalModel.fromJson(jsonDecode(prefs.getString(cartKey)));
    } else {
      return null;
    }
  }*/

  static Future<bool> emptyCart() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove(cartKey);
    return true;
  }

  // static Future<UserRole> deleteUser() async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   await UserRepository.deleteToken();
  //   await prefs.clear();
  //   return UserRole.guest;
  // }

  static Future<UserRole> checkUserRole() async {
    print('await UserRepository.hasToken: ${await UserRepository.hasToken}');
    UserRole role = await UserRepository.hasToken ? UserRole.normal : UserRole.guest;
    return role;
  }
}
