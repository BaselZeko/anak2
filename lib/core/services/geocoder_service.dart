// import 'package:vendor/core/models/address_model.dart';
// import 'package:vendor/core/models/location_model.dart';
// import 'package:geocoder/geocoder.dart';
// import 'package:geocoder/services/base.dart';
//
// class GeoCoderService {
//   static final GeoCoderService _instance = GeoCoderService._();
//
//   GeoCoderService._();
//
//   factory GeoCoderService() => _instance;
//
//   Geocoding geocoding = Geocoder.local;
//
//   Future<List<AddressModel>> getListAddressFromCoordinates({double lat, double long}) async {
//     List<Address> address = await geocoding.findAddressesFromCoordinates(Coordinates(lat, long));
//     if (address.isNotEmpty) {
//       return address
//           .map((e) => AddressModel(
//               addressLine: e.addressLine,
//               adminArea: e.adminArea,
//               countryCode: e.countryCode,
//               countryName: e.countryName,
//               featureName: e.featureName,
//               locality: e.locality,
//               postalCode: e.postalCode,
//               subAdminArea: e.subAdminArea,
//               subLocality: e.subLocality,
//               subThoroughfare: e.subThoroughfare,
//               thoroughfare: e.thoroughfare,
//               position: LocationModel(
//                   lat: e.coordinates.latitude.toString(),
//                   long: e.coordinates.longitude.toString())))
//           .toList();
//     }
//
//     return null;
//   }
//
//   Future<AddressModel> getAddressFromCoordinates({double lat, double long}) async {
//     List<Address> address = await geocoding.findAddressesFromCoordinates(Coordinates(lat, long));
//     if (address.isNotEmpty) {
//       return AddressModel(
//           addressLine: address.first.addressLine,
//           adminArea: address.first.adminArea,
//           countryCode: address.first.countryCode,
//           countryName: address.first.countryName,
//           featureName: address.first.featureName,
//           locality: address.first.locality,
//           postalCode: address.first.postalCode,
//           subAdminArea: address.first.subAdminArea,
//           subLocality: address.first.subLocality,
//           subThoroughfare: address.first.subThoroughfare,
//           thoroughfare: address.first.thoroughfare,
//           position: LocationModel(
//               lat: address.first.coordinates.latitude.toString(),
//               long: address.first.coordinates.longitude.toString()));
//     }
//
//     return null;
//   }
// }
