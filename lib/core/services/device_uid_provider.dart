import 'package:flutter_udid/flutter_udid.dart';

class DeviceUidProvider {
  Future<String> getDeviceUniqueId() async {
    return (await FlutterUdid.udid);
  }
}