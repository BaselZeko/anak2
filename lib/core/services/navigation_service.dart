import 'package:flutter/cupertino.dart';

class NavigationService {
  static final NavigationService _instance = NavigationService._();

  NavigationService._();

  factory NavigationService() => _instance;

  Future<T> navigateTo<T>(BuildContext context, Widget page) {
    return Navigator.push(
        context, CupertinoPageRoute(builder: (BuildContext context) => page));
  }

  Future<T> navigateWithReplace<T>(BuildContext context, Widget page) {
    return Navigator.pushReplacement(
        context, CupertinoPageRoute(builder: (BuildContext context) => page));
  }

  Future<T> navigateWithSlideAnimation<T>(BuildContext context, Widget page,
      {int duration = 600}) {
    return Navigator.of(context).push(PageRouteBuilder(
        pageBuilder: (context, animation, anotherAnimation) {
          return page;
        },
        transitionDuration: Duration(milliseconds: duration),
        transitionsBuilder: (context, animation, anotherAnimation, child) {
          const begin = Offset(0.0, 1.0);
          const end = Offset.zero;
          const curve = Curves.ease;

          final tween = Tween(begin: begin, end: end);
          final curvedAnimation = CurvedAnimation(
            parent: animation,
            curve: curve,
          );

          return SlideTransition(
            position: tween.animate(curvedAnimation),
            child: child,
          );
        }));
  }

  void navigateBack<T>(BuildContext context, {T data}) {
    return Navigator.pop(context, data);
  }
}
