import 'package:anak/core/error/base_error.dart';
import 'package:anak/core/result/result.dart';

abstract class UseCase<Type, Params> {
  Future<Result<BaseError, Type>> call(Params params);
}

abstract class StreamUseCase<Type, Params> {
  Stream<Type> call(Params params);
}
