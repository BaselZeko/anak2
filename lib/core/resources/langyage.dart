import 'package:flutter/material.dart';
import 'package:anak/core/blocs/language_bloc/settings_bloc.dart';
import 'package:anak/core/blocs/language_bloc/settings_event.dart';
import 'package:anak/core/helper/extensions/translation.dart';
import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/localization/app_lang.dart';
import 'package:anak/core/services/localization_storage.dart';

import 'colors.dart';

class SetLangWidget extends StatefulWidget {
  const SetLangWidget({Key key}) : super(key: key);

  @override
  _SetLangWidgetState createState() => _SetLangWidgetState();
}

class _SetLangWidgetState extends State<SetLangWidget> {
  final _bloc = SettingsBloc(LocalStorageService());

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<int>(
        stream: appLanguage.stream.distinct(),
        builder: (context, snapshot) {
          return Column(
            children: [
              const Text(
                'Choose the language',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              const SizedBox(
                height: 40,
              ),
              LangWidget(
                value: AppLanguages.AR,
                groupValue: mapIntToLang(snapshot.data),
                title: 'arabic'.tr(context),
                onLangChange: (language) {
                  _bloc.add(SetAppLanguage((b) => b..language = language));
                  Navigator.pop(context);
                },
              ),
              const SizedBox(
                height: 20,
              ),
              LangWidget(
                value: AppLanguages.EN,
                groupValue: mapIntToLang(snapshot.data),
                title: 'english'.tr(context),
                onLangChange: (language) {
                  _bloc.add(SetAppLanguage((b) => b..language = language));
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }
}

typedef OnLangChange = Function(AppLanguages language);

class LangWidget extends StatelessWidget {
  const LangWidget(
      {Key key,
        this.title,
        this.onLangChange,
        this.value,
        this.groupValue,
        this.width,
        this.height})
      : super(key: key);
  final String title;
  final AppLanguages value;
  final AppLanguages groupValue;
  final OnLangChange onLangChange;
  final double width;
  final double height;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onLangChange(value);
      },
      child: Container(
        height: height ?? 40,
        width: width ?? ScreensHelper.of(context).fromWidth(80),
        padding: const EdgeInsets.all(8.0),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            border: Border.all(color: GlobalColors.grey)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              height: 15,
              width: 15,
              margin: const EdgeInsetsDirectional.only(end: 8),
              decoration: BoxDecoration(
                  color: groupValue == value
                      ? GlobalColors.secondaryColor
                      : GlobalColors.transparent,
                  borderRadius: BorderRadius.circular(100),
                  border: Border.all(color: GlobalColors.grey)),
            ),
            Text(
              title,
              style: const TextStyle(fontWeight: FontWeight.w500, fontSize: 16),
            )
          ],
        ),
      ),
    );
  }
}
