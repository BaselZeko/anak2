abstract class GlobalImages {
  static const String gridState = 'assets/images/grid_state.svg';
  static const String normalState = 'assets/images/normal_state.svg';
  static const String order = 'assets/images/orders.svg';
  static const String home = 'assets/images/home_icon.svg';
  static const String profile = 'assets/images/profile.svg';
  static const String login = 'assets/images/login.svg';
  static const String branches = 'assets/images/branches.svg';
  static const String contactUs = 'assets/images/contact_us.svg';
  static const String splashLogo = 'assets/images/splash_logo.svg';
  static const String splashLogo2 = 'assets/images/primeSp.svg';
  static const String check = 'assets/images/check.svg';
  static const String cart = 'assets/images/cart.svg';
  static const String search = 'assets/images/search.svg';
  static const String introBackground = 'assets/images/intro_background.svg';
  static const String intro1 = 'assets/images/intro_1.png';
  static const String intro1b = 'assets/images/intro_1b.png';
  static const String intro2 = 'assets/images/intro_2.svg';
  static const String qr = 'assets/images/barcode.svg';
  static const String intro = 'assets/images/intro_';
  static const String myAddress = 'assets/images/222.png';
  static const String addAddress = 'assets/images/111.png';
  static const String swiper = 'assets/images/swiper_';
  static const String reorderItems = 'assets/images/reorder_items.svg';
  static const String offers = 'assets/images/offers.svg';
  static const String bestSellers = 'assets/images/best_sellers.svg';
  static const String favorites = 'assets/images/favorites.svg';
  static const String partner = 'assets/images/partner_';
  static const String product = 'assets/images/product_';
  static const String business = 'assets/images/cofes_';
  static const String verifyAccount = 'assets/images/verifyaccount.svg';
  static const String language = 'assets/images/language.svg';
  static const String logout = 'assets/images/logout.svg';
  static const String pravacy = 'assets/images/pravacy.svg';
  static const String support = 'assets/images/support.svg';
  static const String terms = 'assets/images/terms.svg';
  static const String wallet = 'assets/images/wallet.svg';
  static const String savedLocation = 'assets/images/saved.svg';
  static const String visa = 'assets/images/visa.svg';
  static const String next = 'assets/images/next.svg';
  static const String apple = 'assets/images/apple.svg';
  static const String google = 'assets/images/google.svg';
  static const String facebook = 'assets/images/facebook.svg';
  static const String price = 'assets/images/price.svg';
  static const String prand1 = 'assets/images/prand1.png';
  static const String store1 = 'assets/images/store1.png';
  static const String logo = 'assets/images/logo.png';
  static const String primary_logo = 'assets/images/pimary_logo.png';
  static const String river_island = 'assets/images/river_island.png';
  static const String model = 'assets/images/model.png';


  static const String shoppingCard = 'assets/images/shopping_card.png';
  static const String paymentSuccessfully = 'assets/images/payment_successfully.png';

  static const String coponsComponant = 'assets/images/coponComponent.png';
  ///icons
  static const String addToCard = 'assets/images/add_to_card.png';



  ///lines
  static const String dashLine = 'assets/images/dash_line.png';

  ///moreIcons
  static const String about = 'assets/images/more_icons/about.png';
  static const String copons = 'assets/images/more_icons/conpons.png';
  static const String fav = 'assets/images/more_icons/fav.png';
  static const String logoutIcon = 'assets/images/more_icons/logout.png';
  static const String orders = 'assets/images/more_icons/orders.png';
  static const String paymenet = 'assets/images/more_icons/payment.png';
  static const String setting = 'assets/images/more_icons/setting.png';
  static const String share = 'assets/images/more_icons/share.png';
  static const String sugg = 'assets/images/more_icons/sugg.png';
  static const String walt = 'assets/images/more_icons/walt.png';
  static const String person = 'assets/images/more_icons/person.png';


  ///payments card images
  static const String appleCard = 'assets/images/payment_card/apple_card.png';
  static const String mada = 'assets/images/payment_card/mada.png';
  static const String mada1 = 'assets/images/payment_card/mada1.png';
  static const String stc = 'assets/images/payment_card/stc.png';
  static const String visaCard = 'assets/images/payment_card/visa.png';
  static const String mastardCard = 'assets/images/payment_card/mastard_card.png';



}