import 'dart:ui';

import 'package:flutter/material.dart';

abstract class GlobalColors {

  static const Color mainColor1 = Color(0xffE1C084);
  static const Color mainColor2 = Color(0xffffffff);
  static const Color mainColor3 = Color(0xff844343);

  ///text
  static const Color primaryText = Color(0xffF13D59);
  static const Color secondaryText = Color(0xff902CCF);
  static const Color greyText = Color(0xff828282);
  static const Color blackText = Color(0xff373737);
  static const Color grey = Color(0xff828282);
  static const Color lightGrey = Color(0xffC4C4C4);
  static const Color lightPrimary = Color(0xffFFADAD);


  static const Color lightBlue = Color(0xff8AB6EB);


  ///dark
  static const Color primagryDark = Color(0xff0E1017);

  ///backgrounds
  static const Color greyBackground = Color(0xffEFEFEF);

  static const Color primaryColor = Color(0xffd81e41);
  static const Color secondaryColor = Color(0xff8D2CD3);


  static const Color blue = Color(0xff45C1F6);
  static const Color redAccent = Color(0xffF13D59);
  static const Color appBar = Color(0xffffffff);


  static const Color selectedColor = Color(0xffFFBE0D);
  static const Color brown = Color(0xff711D00);
  static const Color white = Color(0xFFFFFFFF);
  static const Color black = Color(0xFF000000);


  static const Color borderGrey = Color(0xfff2eff3);
  static const Color borderGrey2 = Color(0xffe3e3e3);
  static const Color greyBackGround = Color(0xFFEEEEEE);
  static const Color errorRed = Color(0xffFF0000);
  static const Color successGreen = Color(0xff00ff00);
  static const Color successGradiantGreen = Color(0x9900ff00);

  static const Color lightBlueAccent = Colors.lightBlueAccent;
  static const Color yellow = Colors.yellow;
  static const Color orange = Colors.orange;
  static const Color lightRed = Colors.red;
  static const Color green = Colors.green;
  static const Color datePickerBackGround = Color(0xFFd4d7dc);
  static const Color cupertinoBorder = Color(0xFF9a9a9a);

  static const Color transparent = Colors.transparent;
}
