import 'package:anak/features/cart/presentation/pages/cart_page.dart';
import 'package:anak/features/chat/presentation/pages/chat_list_page.dart';
import 'package:anak/features/home_page/presentation/home_page.dart';
import 'package:anak/features/home_page/presentation/notificaion_page.dart';
import '../../../features/more_pages/presentation/pages/more_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:anak/core/helper/extensions/translation.dart';
import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/resources/images.dart';
import 'package:rxdart/rxdart.dart';

final homePageStream = BehaviorSubject<int>();

class RootPage extends StatefulWidget {
  const RootPage({Key key}) : super(key: key);

  @override
  _RootPageState createState() => _RootPageState();
}

class _RootPageState extends State<RootPage> {
  int currentIndex = 0;
  PageController controller;
  int taps = 0;

  List<Widget> pages = [
    const HomePage(),
    const CartPage(),
    NotificaionPage(),
    ChatListPage(),
    MoreOptionsPage(),
  ];

  @override
  void initState() {
    controller = PageController(initialPage: 0);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ScreensHelper(context);
    return WillPopScope(
      onWillPop: () async {
        _closeApp();
        return false;
      },
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          floatingActionButtonLocation:
          FloatingActionButtonLocation.centerDocked,
          body: PageView.builder(
            controller: controller,
            itemCount: pages.length,
            itemBuilder: (BuildContext context, int index) {
              return pages[index];
            },
            onPageChanged: (value) {
              currentIndex = value;
              setState(() {});
            },
          ),
          bottomNavigationBar: BottomAppBar(
            shape: const CircularNotchedRectangle(),
            notchMargin: 7.0,
            color: Colors.transparent,
            elevation: 10.0,
            clipBehavior: Clip.antiAlias,
            child: Container(
              height: 60.0,
              decoration: const BoxDecoration(
                color: Colors.white,
              ),
              child: Row(
                children: [
                  Expanded(
                    child: BottomAppBarItem(
                      value: 0,
                      currentIndex: currentIndex,
                      icon: GlobalImages.profile,
                      icon2: Icons.home_filled,
                      isImage: false,
                      label: 'home'.tr(context),
                      onSelect: (value) {
                        controller.jumpToPage(value);
                        currentIndex = value;
                        setState(() {});
                      },
                      selectedColor: GlobalColors.primaryColor,
                    ),
                  ),
                  Expanded(
                    child: BottomAppBarItem(
                      value: 1,
                      currentIndex: currentIndex,
                      icon: GlobalImages.cart,
                      label: 'cart'.tr(context),
                      onSelect: (value) {
                        controller.jumpToPage(value);
                        currentIndex = value;
                        setState(() {});
                      },
                      selectedColor: GlobalColors.primaryColor,
                    ),
                  ),
                  Expanded(
                    child: BottomAppBarItem(
                      value: 2,
                      currentIndex: currentIndex,
                      icon: GlobalImages.next,
                      isImage: false,
                      icon2: Icons.notifications,
                      label: 'notification'.tr(context),
                      onSelect: (value) {
                        controller.jumpToPage(value);
                        currentIndex = value;
                        setState(() {});
                      },
                      selectedColor: GlobalColors.primaryColor,
                    ),
                  ),
                  Expanded(
                    child: BottomAppBarItem(
                      value: 3,
                      currentIndex: currentIndex,
                      icon: GlobalImages.favorites,
                      isImage: false,
                      icon2: Icons.message_rounded,
                      label: 'Message'.tr(context),
                      onSelect: (value) {
                        controller.jumpToPage(value);
                        currentIndex = value;
                        setState(() {});
                      },
                      selectedColor: GlobalColors.primaryColor,
                    ),
                  ),
                  Expanded(
                    child: BottomAppBarItem(
                      value: 4,
                      currentIndex: currentIndex,
                      icon: GlobalImages.addAddress,
                      icon2: Icons.more_horiz_rounded,
                      isImage: false,
                      label: 'more'.tr(context),
                      onSelect: (value) {
                        controller.jumpToPage(value);
                        currentIndex = value;
                        setState(() {});
                      },
                      selectedColor: GlobalColors.primaryColor,
                    ),
                  ),
                ],
              ),
            ),
          )),
    );
  }

  _closeApp() {
    if (taps == 1) {
      SystemNavigator.pop();
    }
    Fluttertoast.showToast(
      msg: "Please click back again to exit",
      toastLength: Toast.LENGTH_SHORT,
      fontSize: 12,
    );
    taps++;

    Future.delayed(const Duration(milliseconds: 500), () {
      taps = 0;
    });
  }
}

class BottomAppBarItem extends StatelessWidget {
  const BottomAppBarItem(
      {Key key,
        this.icon,
        this.label,
        this.icon2,
        this.isImage = true,
        this.onSelect,
        this.value,
        this.currentIndex,
        this.selectedColor})
      : super(key: key);
  final int value;
  final int currentIndex;
  final Color selectedColor;
  final String icon;
  final IconData icon2;
  final String label;
  final bool isImage;

  final void Function(int value) onSelect;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onSelect(value),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          if (isImage)
            SvgPicture.asset(
              icon,
              color: value == currentIndex ? selectedColor : GlobalColors.grey,
              width: 25,
              height: 25,
              fit: BoxFit.contain,
            ),
          if (!isImage)
            Icon(
              icon2,
              size: 25,
              color: value == currentIndex ? selectedColor : GlobalColors.grey,
            ),
          Text(
            label,
            style: TextStyle(
                fontSize: 14,
                color:
                value == currentIndex ? selectedColor : GlobalColors.grey),
          ),
        ],
      ),
    );
  }
}
