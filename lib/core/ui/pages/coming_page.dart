import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:anak/core/helper/screen_util/screen_helper.dart';

class ComingSoonPage extends StatefulWidget {
  const ComingSoonPage({Key key}) : super(key: key);

  @override
  _ComingSoonPageState createState() => _ComingSoonPageState();
}

class _ComingSoonPageState extends State<ComingSoonPage> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: ScreensHelper.of(context).fromHeight(100),
      child: const Center(child: Text('Coming Soon',style: TextStyle(color: Colors.orange,fontSize: 22),),),
    );
  }
}
