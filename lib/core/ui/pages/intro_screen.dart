import 'package:anak/features/user_managment/presentation/pages/login_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:anak/core/helper/extensions/translation.dart';
import 'package:anak/core/helper/screen_util/screen_helper.dart';

import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/resources/images.dart';

import 'package:anak/core/services/navigation_service.dart';

import 'package:anak/core/ui/widgets/custom_indecator/custom_indecator.dart';
import 'package:anak/core/ui/widgets/button.dart';

typedef OnSelect = void Function(int value);

class IntroScreen extends StatefulWidget {
  const IntroScreen({Key key}) : super(key: key);

  @override
  _IntroScreenState createState() => _IntroScreenState();
}

class _IntroScreenState extends State<IntroScreen> {
  PageController _controller;

  int currentIndex = 0;

  List<int> pages = [1, 2, 3];

  @override
  void initState() {
    _controller = PageController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          PageView.builder(
            itemCount: pages.length,
            itemBuilder: (context, index) {
              return IntroPage(
                page: index + 1,
                index: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 8.0),
                      child: CustomIndicator(
                        animationDuration: 500,
                        count: pages.length,
                        index: currentIndex,
                        color:
                            index == 1 ? GlobalColors.grey : GlobalColors.grey,
                        activeColor: index == 1
                            ? GlobalColors.primaryColor
                            : GlobalColors.white,
                        height: 2.5,
                      ),
                    ),
                  ],
                ),
                controller: _controller,
                counter: currentIndex,
              );
            },
            controller: _controller,
            onPageChanged: (value) {
              currentIndex = value;
              print(value);
              setState(() {});
            },
          ),
          Align(
            alignment: AlignmentDirectional.topEnd,
            child: Padding(
              padding:
                  EdgeInsets.only(top: MediaQuery.of(context).padding.top + 16),
              child: TextButton(
                  onPressed: () {
                    NavigationService()
                        .navigateTo(context, const LoginPage());
                  },
                  child: Text(
                    'skip'.tr(context),
                    style: TextStyle(
                        fontSize: 18,
                        color: currentIndex == 1
                            ? GlobalColors.white
                            : GlobalColors.primaryColor,
                        fontWeight: FontWeight.w300),
                  )),
            ),
          ),
        ],
      ),
    );
  }
}

class IntroPage extends StatefulWidget {
  IntroPage(
      {Key key,
      this.page,
      this.index,
      this.controller,
      this.counter,
  })
      : super(key: key);
  final int page;
  final Widget index;
  final PageController controller;

  int counter;

  @override
  State<IntroPage> createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:
          widget.page == 2 ? GlobalColors.primaryColor : GlobalColors.white,
      body: SizedBox(
        height: ScreensHelper.of(context).fromHeight(100),
        width: ScreensHelper.of(context).fromWidth(100),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top +
                      ScreensHelper.of(context).fromHeight(15)),
              child: SizedBox(
                width: ScreensHelper.of(context).fromWidth(100),
                height: ScreensHelper.of(context).fromHeight(40),
                child: Center(
                  child: Stack(
                    children: [
                      Image.asset(
                        GlobalImages.intro1b,
                        height: ScreensHelper.of(context).fromHeight(60),
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: Image.asset(
                          '${GlobalImages.intro}${widget.page}.png',
                          height: ScreensHelper.of(context).fromHeight(60),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 14.0),
              child: Container(
                color: widget.page == 2
                    ? GlobalColors.primaryColor
                    : GlobalColors.white,
                height: ScreensHelper.of(context).fromHeight(40),
                child: Stack(
                  clipBehavior: Clip.none,
                  children: [
                    Positioned(
                      top: 20,
                      right: 0,
                      left: 0,
                      child: Container(
                        width: ScreensHelper.of(context).fromWidth(100),
                        height: ScreensHelper.of(context).fromHeight(30),
                        decoration: BoxDecoration(
                            color: widget.page == 2
                                ? GlobalColors.white
                                : GlobalColors.primaryColor,
                            borderRadius: BorderRadius.circular(12)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            widget.index,
                            SizedBox(
                                width: MediaQuery.of(context).size.width * 0.6,
                                child: Text(
                                  "خطوات سهلة لتبدأ طلبك الآن فقط في نقرة واحدة سيصلك ماتريده معك في كل مكان",
                                  maxLines: 3,
                                  textDirection: TextDirection.rtl,
                                  textAlign: TextAlign.justify,
                                  style: TextStyle(
                                      fontSize: 16,
                                      color: widget.page == 2
                                          ? GlobalColors.primaryColor
                                          : GlobalColors.white,
                                      letterSpacing: 4,
                                      wordSpacing: 4),
                                )),
                            Button(
                              title: "ابدأ التسوق",
                              color: widget.page == 2
                                  ? GlobalColors.white
                                  : GlobalColors.primaryColor,
                              color2: widget.page != 2
                                  ? GlobalColors.white
                                  : GlobalColors.primaryColor,
                              width: ScreensHelper.of(context).fromWidth(50),
                              onPressed: () {},
                            ),
                          ],
                        ),
                      ),
                    ),
                    PositionedDirectional(
                      top: -4,
                      start: 16,
                      child: Container(
                        width: 55,
                        height: 55,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          color: Colors.white,
                        ),
                        child: Center(
                          child: InkWell(
                            onTap: () {
                              if (widget.counter >= 2) {
                                NavigationService()
                                    .navigateTo(context, const LoginPage());
                                return;
                              }
                              widget.counter++;
                              widget.controller.jumpToPage(widget.counter);
                              setState(() {});

                            },
                            child: Container(
                              width: 45,
                              height: 45,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(100),
                                color: GlobalColors.primaryColor,
                              ),
                              child: Center(
                                child: SvgPicture.asset(
                                  GlobalImages.next,
                                  color: GlobalColors.white,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            // Text(
            //   page != 4 ? '${"don't_worry".tr(context)} ${getTitle(page, context)}' : getTitle(page, context),
            //   textAlign: TextAlign.center,
            //   style: const TextStyle(
            //       color: GlobalColors.secondaryColor, fontWeight: FontWeight.w500, fontSize: 17),
            // ),
            // SizedBox(
            //   height: 60,
            //   child: Text(
            //     getDesc(page, context),
            //     textAlign: TextAlign.center,
            //     style: const TextStyle(color: GlobalColors.black, fontWeight: FontWeight.w500, fontSize: 17),
            //   ),
            // ),
          ],
        ),
      ),
    );
  }
}
