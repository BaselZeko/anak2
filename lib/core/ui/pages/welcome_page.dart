import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:anak/core/helper/extensions/translation.dart';
import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/resources/images.dart';
import 'package:anak/core/services/navigation_service.dart';
import 'package:anak/core/ui/widgets/button.dart';
import 'package:anak/features/user_managment/presentation/pages/login_page.dart';

class WelcomePage extends StatelessWidget {
  const WelcomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: GlobalColors.white,
      appBar: AppBar(
        backgroundColor: GlobalColors.white,
        elevation: 0.2,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back_ios,
            color: GlobalColors.black,
          ),
          onPressed: () => Navigator.pop(context),
        ),
        actions: [
          Container(
              margin: const EdgeInsetsDirectional.only(end: 16),
              child: SvgPicture.asset(
                GlobalImages.splashLogo,
                height: 24,
              ))
        ],
      ),
      body: SizedBox(
        height:
            ScreensHelper.of(context).fromHeight(100) - kToolbarHeight - MediaQuery.of(context).padding.top,
        width: ScreensHelper.of(context).fromWidth(100),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(
              '${GlobalImages.intro}4.jpg',
              height: ScreensHelper.of(context).fromHeight(26),
              fit: BoxFit.fill,
            ),
            const SizedBox(
              height: 50,
            ),
             Text(
              'moon_controls_the_project'.tr(context),
              textAlign: TextAlign.center,
              style: const TextStyle(color: GlobalColors.secondaryColor, fontWeight: FontWeight.w500, fontSize: 18),
            ),
             SizedBox(
              height: 150,
              child: Text(
                'moon2'.tr(context),
                textAlign: TextAlign.center,
                style: const TextStyle(color: GlobalColors.black, fontWeight: FontWeight.w500, fontSize: 18),
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            Button(
              title: 'register_your_number'.tr(context),
              onPressed: () {
                NavigationService().navigateTo(context, const LoginPage());
              },
            )
          ],
        ),
      ),
    );
  }
}
