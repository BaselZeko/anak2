import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/resources/images.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';


class SplashScreen extends StatefulWidget {
  const SplashScreen({Key key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation sizeAnimation;

  @override
  void initState() {
    controller = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 2200));
    sizeAnimation = Tween<double>(begin: 40.0, end: 120.0)
        .animate(CurvedAnimation(parent: controller, curve: Curves.bounceOut));
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    controller.forward();

    return Stack(
      children: [
        Container(decoration: const BoxDecoration(
          gradient: LinearGradient(colors: [
            GlobalColors.primaryColor,
            GlobalColors.secondaryColor
          ])
        ),),
        Scaffold(
          backgroundColor: GlobalColors.transparent,
          body: Center(
            child: AnimatedBuilder(
              animation: sizeAnimation,
              builder: (BuildContext context, Widget child) {
                return SvgPicture.asset(
                  GlobalImages.splashLogo,
                  width: sizeAnimation.value,
                  height: sizeAnimation.value,
                  color: GlobalColors.white,
                );
              },
            ),
          ),
        ),
      ],
    );
  }
}
