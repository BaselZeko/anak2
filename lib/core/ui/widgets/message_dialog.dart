import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/resources/images.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

enum ActionState { success, error }

class MessageDialog extends StatefulWidget {
  const MessageDialog({Key key, this.message, this.actionState, this.isWithActions = false}) : super(key: key);
  final String message;
  final ActionState actionState;
  final bool isWithActions;

  @override
  _MessageDialogState createState() => _MessageDialogState();
}

class _MessageDialogState extends State<MessageDialog> with TickerProviderStateMixin {
  AnimationController _controller;
  Animation<double> _animation;

  @override
  void initState() {
    _controller = AnimationController(
      duration: const Duration(milliseconds: 1300),
      lowerBound: 0.3,
      vsync: this,
    );

    _animation = CurvedAnimation(
      parent: _controller,
      curve: Curves.bounceInOut,
    );
    _controller.forward();
  if(!widget.isWithActions) {
    Future.delayed(const Duration(milliseconds: 2500)).then((value) {
      if (mounted) Navigator.of(context).pop();
    });
  }
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(22)), children: [

      Center(
        child: ScaleTransition(
          scale: _animation,
          child: widget.actionState == ActionState.success
              ? SvgPicture.asset(
                  GlobalImages.check,
                  color: GlobalColors.successGreen,
                  height: 40,
                  width: 40,
                )
              : const Icon(
                  Icons.clear,
                  color: GlobalColors.errorRed,
                  size: 50,
                ),
        ),
      ),
      const SizedBox(
        height: 8,
      ),
      Center(
        child: Text(
          widget.actionState == ActionState.success ? 'Success' : 'Failure',
          style: const TextStyle(
              fontWeight: FontWeight.bold, fontSize: 18, letterSpacing: 2, color: GlobalColors.black),
        ),
      ),
      const SizedBox(
        height: 8,
      ),
      Center(
        child: Text(widget.message,
            textAlign: TextAlign.center,
            style:
                const TextStyle(fontSize: 15, color: GlobalColors.greyText, fontWeight: FontWeight.w400)),
      ),
      if (widget.actionState == ActionState.success && widget.isWithActions)
        Row(
          children: [
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: const Text("Ok"),
            ),
            const SizedBox(
              width: 8,
            ),
            TextButton(
              onPressed: () {},
              child: const Text("Go to My Cart"),
            )
          ],
        )
    ]);
  }
}
