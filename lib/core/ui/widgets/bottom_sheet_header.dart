import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';

class BottomSheetHeader extends StatelessWidget {
  const BottomSheetHeader({this.title, this.onCloseSheet});

  final String title;
  final VoidCallback onCloseSheet;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreensHelper.of(context).fromWidth(100),
      padding: const EdgeInsets.symmetric(horizontal: 8),
      decoration: const BoxDecoration(
        color: GlobalColors.transparent,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(9), topRight: Radius.circular(9)),
      ),
      child: Column(
        children: [
          InkWell(
            onTap: () => Navigator.pop(context),
            child: const SizedBox(
              width: 80,
              height: 26,
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(10),
                      bottomLeft: Radius.circular(10)),
                ),
                child: Icon(
                  CupertinoIcons.chevron_compact_down,
                  size: 22,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
