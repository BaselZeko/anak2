import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'bottom_sheet_header.dart';

class CustomBottomSheet extends StatefulWidget {
  final Widget child;
  final double height;
  final bool withHeader;

  const CustomBottomSheet({Key key, this.child, this.height, this.withHeader = true}) : super(key: key);

  @override
  _CustomBottomSheetState createState() => _CustomBottomSheetState();
}

class _CustomBottomSheetState extends State<CustomBottomSheet> {
  bool value2 = false;

  @override
  void initState() {
    Future.delayed(const Duration(milliseconds: 1)).then((value) {
      value2 = true;
      if (mounted) {
        setState(() {});
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: [
        AnimatedContainer(
          duration: const Duration(milliseconds: 400),
          curve: Curves.bounceInOut,
          color: GlobalColors.transparent,
          height: widget.height ??
              ScreensHelper.of(context).fromHeight(
                  90) /* == null
              ? value2
                  ? ScreensHelper.of(context).fromHeight(90)
                  : ScreensHelper.of(context).fromHeight(95)
              : widget.height*/
          ,
          child: Container(
            width: ScreensHelper.of(context).fromWidth(100),
            padding: const EdgeInsets.only(top: 16),
            decoration: const BoxDecoration(
              color: GlobalColors.white,
              borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20)),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                if (widget.withHeader) const BottomSheetHeader(),
                widget.child,
              ],
            ),
          ),
        ),
      ],
    );
  }
}
