import 'package:flutter/material.dart';

typedef OnCountChanged = void Function(int value);

class CounterWidget extends StatefulWidget {
  const CounterWidget(
      {Key key,
      this.initValue,
      this.onCountChanged,
      this.height,
      this.width,
      this.onMinus,
      this.onAdd,
      this.edge,
      this.lowerBound,
      this.upperBound})
      : super(key: key);

  final int initValue;
  final int upperBound;
  final int lowerBound;
  final OnCountChanged onCountChanged;
  final VoidCallback onAdd;
  final VoidCallback onMinus;
  final double height;
  final double width;
  final EdgeInsetsGeometry edge;

  @override
  CounterWidgetState createState() => CounterWidgetState();
}

class CounterWidgetState extends State<CounterWidget> {
  int counter = 0;
  bool change = false;

  @override
  void initState() {
    counter = widget.initValue;
    super.initState();
  }

  @override
  void didUpdateWidget(covariant CounterWidget oldWidget) {
    print('widget.initValue : ${widget.initValue}');
    print('oldWidget.initValue : ${oldWidget.initValue}');
    print('counter : $counter');

    if (widget.initValue == oldWidget.initValue ) {
      counter = widget.initValue;
    }
    change = false;
    super.didUpdateWidget(oldWidget);
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.height ?? 50.0,
     width: widget.width ?? 100,
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.grey[300],
        ),
        borderRadius: const BorderRadius.horizontal(
          left: Radius.circular(10),
          right: Radius.circular(10),
        ),
      ),
      child: Padding(
        padding: widget.edge ?? const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            InkWell(
              onTap: widget.onMinus ??
                  () {
                    if (counter == widget.lowerBound) {
                      return;
                    } else {
                      change = true;

                      counter--;
                      widget.onCountChanged(counter);
                      setState(() {});
                    }
                  },
              child: Container(
                padding: const EdgeInsets.all(8),
                child: const Icon(
                  Icons.remove,
                  size: 14,
                ),
              ),
            ),
            Text(widget.onMinus != null? "${widget.initValue}" : "$counter"),
            InkWell(
              onTap: widget.onAdd ??
                  () {
                    if (widget.upperBound != null ||
                        counter == widget.upperBound) {
                      return;
                    }
                    counter++;
                    change = true;
                    widget.onCountChanged(counter);
                    setState(() {});
                  },
              child: Container(
                padding: const EdgeInsets.all(8),
                child: const Icon(
                  Icons.add,
                  size: 14,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
