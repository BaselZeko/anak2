import 'package:flutter/material.dart';
import 'package:anak/core/localization/app_lang.dart';
import 'package:anak/core/resources/colors.dart';

class CustomTextField extends StatefulWidget {
  CustomTextField(
      {Key key,
        this.align = TextAlign.start,
        this.controller,
        this.keyboardType,
        this.hint,
        this.fillColor,
        this.suffix,
        this.onChanged,
        this.enable = true,
        this.hintSize})
      : super(key: key);
  final TextEditingController controller;
  final TextInputType keyboardType;
  final String hint;
  final double hintSize;
  final Widget suffix;
  final TextAlign align;
  final ValueChanged onChanged;
  final Color fillColor;
  bool enable;

  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: widget.controller,
      enabled: widget.enable,
      textAlign: widget.align,
      textDirection:
      globalLanguage.index == 0 ? TextDirection.rtl : TextDirection.ltr,
      decoration: InputDecoration(
          prefixIcon: widget.suffix,
          filled: true,
          fillColor: widget.fillColor??GlobalColors.lightGrey,
          hintText: widget.hint,
          hintStyle: widget.hintSize != null
              ? TextStyle(fontSize: widget.hintSize,color: Colors.white)
              : null,
          contentPadding: EdgeInsets.symmetric(
              horizontal: 16, vertical: widget.hintSize == null ? 20 : 16),
          hintTextDirection:
          globalLanguage.index == 0 ? TextDirection.rtl : TextDirection.ltr,
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: const BorderSide(
                  color: GlobalColors.borderGrey, style: BorderStyle.solid)),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: const BorderSide(
                  color: GlobalColors.secondaryColor,
                  style: BorderStyle.solid,
                  width: 1)),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: const BorderSide(
                  color: GlobalColors.borderGrey, style: BorderStyle.solid))),
      keyboardType: widget.keyboardType,
      cursorHeight: 24,
      cursorRadius: const Radius.circular(8),
      onChanged: widget.onChanged,
    );
  }
}