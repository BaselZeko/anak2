import 'package:anak/core/error/connection_error.dart';
import 'package:anak/core/localization/app_localization.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:flutter/material.dart';

import '../../error/error_helper.dart';
import '../../error/http/unauthorized_error.dart';

class GeneralErrorWidget extends StatefulWidget {
  final VoidCallback callback;
  final Widget body;
  final error;
  final String message;

  const GeneralErrorWidget({
    Key key,
    this.callback,
    this.message,
    this.body,
    this.error,
  }) : super(key: key);

  @override
  _GeneralErrorWidgetState createState() => _GeneralErrorWidgetState();
}

class _GeneralErrorWidgetState extends State<GeneralErrorWidget> {
  final errorHelper = ErrorHelper();

  @override
  void initState() {
    if (this.widget.error is UnauthorizedError) {
      /// TODO : Handle UnauthorizedError (toWorkOn)
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            widget.body ?? Container(),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12.0),
              child: Text(
                widget.message ??
                    AppLocalizations.of(context).translate(errorHelper.getErrorMessage(widget.error)) ??
                    errorHelper.getErrorMessage(widget.error),
                textAlign: TextAlign.center,
                style: const TextStyle(
                  fontFamily: 'Raleway',
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            RaisedButton(
              onPressed: widget.callback,
              child: Text(
                AppLocalizations.of(context).translate('retry'),
                style: const TextStyle(
                  fontSize: 18,
                  color: GlobalColors.white,
                ),
              ),
              color: GlobalColors.primaryColor,
            ),
          ],
        ),
      ),
    );
  }
}
