import 'package:anak/core/resources/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

typedef OnSelect = void Function(int value);

class SelectableButton extends StatelessWidget {
  final int value;
  final int groupValue;
  final String title;
  final OnSelect onSelect;
  final double height;
  final double width;
  final bool selected;
   EdgeInsets margin;

  SelectableButton({
    @required this.value,
    @required this.groupValue,
    @required this.title,
    @required this.onSelect,
    this.selected,
    this.margin = EdgeInsets.zero,
    this.height,
    this.width,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (value != groupValue) {
          onSelect(value);
          // print("groupValue${groupValue}");
          // print("value$value");
        }
      },
      child: Container(
        //  width: width ?? 40,
        height: height ?? 30,
        margin: margin,
        padding: const EdgeInsets.all(8),
        decoration: BoxDecoration(
          // border: Border.all(color: GlobalColors.primaryColor, width: 1.2),
          borderRadius: BorderRadius.circular(50),
          color: selected
              ? GlobalColors.secondaryColor
              : GlobalColors.greyBackGround,
        ),
        child: Center(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Text(
              title,
              style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: 14,
                color: !selected
                    ? GlobalColors.black
                    : GlobalColors.white,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
