import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BottomLoader extends StatelessWidget {
  const BottomLoader({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        height: 24,
        width: 24,
        margin: const EdgeInsets.only(bottom: 4),
        child: const CircularProgressIndicator(strokeWidth: 1.5),
      ),
    );
  }
}