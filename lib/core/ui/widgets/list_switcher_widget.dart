import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/resources/images.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

enum ListState { grid, normal }

typedef OnStateChanged = void Function(ListState state);

class ListSwitcher extends StatefulWidget {
  const ListSwitcher({Key key, this.currentState, this.onStateChanged})
      : super(key: key);
  final ListState currentState;
  final OnStateChanged onStateChanged;

  @override
  _ListSwitcherState createState() => _ListSwitcherState();
}

class _ListSwitcherState extends State<ListSwitcher> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        GestureDetector(
          onTap: () {
            widget.onStateChanged(ListState.grid);
          },
          child: AnimatedContainer(
            padding: const EdgeInsets.all(8),
            height: 30,
            width: 30,
            curve: Curves.easeInOut,
            decoration: BoxDecoration(
              color: widget.currentState == ListState.grid
                  ? GlobalColors.redAccent
                  : GlobalColors.transparent,
              borderRadius: BorderRadius.circular(100),
            ),
            duration: const Duration(milliseconds: 200),
            child: SvgPicture.asset(
              GlobalImages.gridState,
              color: widget.currentState == ListState.grid
                  ? GlobalColors.white
                  : GlobalColors.black,
              width: 15,
              height: 15,
            ),
          ),
        ),
        const SizedBox(
          width: 8,
        ),
        GestureDetector(
          onTap: () {
            widget.onStateChanged(ListState.normal);
          },
          child: AnimatedContainer(
            duration: const Duration(milliseconds: 200),

            padding: const EdgeInsets.all(8),
            height: 30,
            width: 30,
            curve: Curves.easeInOut,
            decoration: BoxDecoration(
              color: widget.currentState == ListState.normal
                  ? GlobalColors.redAccent
                  : GlobalColors.transparent,
              borderRadius: BorderRadius.circular(100),
            ),
            child: SvgPicture.asset(
              GlobalImages.normalState,
              color: widget.currentState == ListState.normal
                  ? GlobalColors.white
                  : GlobalColors.black,
              width: 15,
              height: 15,
            ),
          ),
        ),
      ],
    );
  }
}
