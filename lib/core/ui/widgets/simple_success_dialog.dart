import 'package:flutter/material.dart';
import 'package:anak/core/helper/extensions/number_check.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/services/navigation_service.dart';
import 'package:anak/core/ui/pages/root_page.dart';
import 'package:anak/features/user_managment/presentation/pages/login_page.dart';

import 'button.dart';

class SuccessDialog extends StatefulWidget {
  final String title;

  const SuccessDialog({Key key, this.title}) : super(key: key);

  @override
  _SuccessDialogState createState() => _SuccessDialogState();
}

class _SuccessDialogState extends State<SuccessDialog> {
  final TextEditingController _controller = TextEditingController(text: '5');

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
        backgroundColor: GlobalColors.white,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
        title: const Center(child: Text('Check out Success')),
        titlePadding: const EdgeInsets.only(top: 32),
        contentPadding: const EdgeInsets.all(16),
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: Button(
                width: 160,
                title: widget.title,
                // onPressed: widget.onPressed,
                onPressed: () {
                  NavigationService().navigateWithReplace(context, const RootPage());
                },
              ),
            ),
          ),
        ]);
  }
}
