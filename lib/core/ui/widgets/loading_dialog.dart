import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoadingDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Platform.isIOS
        ? const Center(
            child: CupertinoActivityIndicator(
              radius: 16.0,
            ),
          )
        : const Center(child: CircularProgressIndicator());
  }

  void endLoading(context) {
    Navigator.pop(context);
  }
}
