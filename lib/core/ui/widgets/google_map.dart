// import 'dart:async';
//
// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
//
// class GoogleMapsWidget extends StatefulWidget {
//   const GoogleMapsWidget({Key key}) : super(key: key);
//
//   @override
//   _GoogleMapsWidgetState createState() => _GoogleMapsWidgetState();
// }
//
// class _GoogleMapsWidgetState extends State<GoogleMapsWidget> {
//   final Completer<GoogleMapController> _controller = Completer();
//   double lat = 30.029585;
//   double lng = 31.022356;
//   final LatLng initialLatLng = const LatLng(30.029585, 31.022356);
//   final LatLng destinationLatLng = const LatLng(30.060567, 30.962413);
//
//   final Set<Marker> _markers = <Marker>{};
//   BitmapDescriptor customIcon;
//
//   bool mapDarkMode = true;
//   String _darkMapStyle;
//   String _lightMapStyle;
//
//   final Set<Polyline> _polyline = {};
//   List<LatLng> polylineCoordinates = [];
//
//   @override
//   void initState() {
//     super.initState();
//   }
//
//   @override
//   void didChangeDependencies() {
//     super.didChangeDependencies();
//     setState(() {
//       _setMapStyle();
//     });
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Stack(children: [
//       GoogleMap(
//         mapType: MapType.normal,
//         rotateGesturesEnabled: true,
//         zoomGesturesEnabled: true,
//         trafficEnabled: false,
//         tiltGesturesEnabled: false,
//         scrollGesturesEnabled: true,
//         compassEnabled: true,
//         myLocationButtonEnabled: true,
//         zoomControlsEnabled: false,
//         mapToolbarEnabled: false,
//         markers: _markers,
//         polylines: _polyline,
//         initialCameraPosition: CameraPosition(
//           target: initialLatLng,
//           zoom: 14.47,
//         ),
//         onMapCreated: (GoogleMapController controller) {
//           _controller.complete(controller);
//           _setMapPins([const LatLng(30.029585, 31.022356)]);
//         },
//       ),
//       Positioned(
//           top: 100,
//           right: 30,
//           child: SizedBox(
//             height: 30,
//             width: 30,
//             child: IconButton(
//               icon: Icon(
//                 mapDarkMode ? Icons.brightness_4 : Icons.brightness_5,
//                 color: Theme.of(context).primaryColor,
//               ),
//               onPressed: () {
//                 setState(() {
//                   mapDarkMode = !mapDarkMode;
//                   _setMapStyle();
//                 });
//               },
//             ),
//           )),
//     ]);
//   }
//
//   _moveCamera(double zoom) async {
//     final CameraPosition myPosition = CameraPosition(
//       target: LatLng(lat, lng),
//       zoom: zoom ?? 14.4746,
//     );
//     final GoogleMapController controller = await _controller.future;
//     controller.moveCamera(CameraUpdate.newCameraPosition(myPosition));
//   }
//
//   _setMapPins(List<LatLng> markersLocation) {
//     _markers.clear();
//     setState(() {
//       markersLocation.forEach((markerLocation) {
//         _markers.add(Marker(
//           markerId: MarkerId(markerLocation.toString()),
//           position: markerLocation,
//           icon: customIcon,
//         ));
//       });
//       _markers.add(Marker(
//         markerId: MarkerId(destinationLatLng.toString()),
//         position: destinationLatLng,
//       ));
//     });
//   }
//
//   Future _setMapStyle() async {
//     final controller = await _controller.future;
//     if (mapDarkMode) {
//       controller.setMapStyle(_darkMapStyle);
//     } else {
//       controller.setMapStyle(_lightMapStyle);
//     }
//   }
// }
