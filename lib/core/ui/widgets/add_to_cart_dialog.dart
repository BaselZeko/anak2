import 'package:flutter/material.dart';
import 'package:anak/core/helper/extensions/number_check.dart';
import 'package:anak/core/resources/colors.dart';

import 'button.dart';
import 'custom_text_field.dart';

class AddToCartDialog extends StatefulWidget {
  const AddToCartDialog({Key key, this.counter}) : super(key: key);
  final int counter;

  @override
  _AddToCartDialogState createState() => _AddToCartDialogState();
}

class _AddToCartDialogState extends State<AddToCartDialog> {
  TextEditingController _controller;

  @override
  void initState() {
    _controller = TextEditingController(text: widget.counter.toString());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
        backgroundColor: GlobalColors.white,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(70)),
        title: const Center(child: Text('Enter Quantity')),
        titlePadding: const EdgeInsets.only(top: 32),
        contentPadding: const EdgeInsets.all(16),
        children: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 48.0, vertical: 16),
            height: 80,
            width: 20,
            child: Wrap(
              children: [
                CustomTextField(
                  controller: _controller,
                  keyboardType: TextInputType.number,
                  align: TextAlign.center,
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: Button(
                width: 160,
                title: 'Confirm',
                // onPressed: widget.onPressed,
                onPressed: () {
                  if (_controller.text.isDigit()) {
                    Navigator.pop(context, int.parse(_controller.text));
                  }
                },
              ),
            ),
          ),
        ]);
  }
}
