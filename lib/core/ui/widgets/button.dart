import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:flutter/material.dart';

class Button extends StatelessWidget {
  const Button(
      {Key key,
      this.height,
      this.fontSize,
      this.onPressed,
        this.radius,
      this.title,
        this.color,
        this.color2,
      this.marginBottom,
      this.width})
      : super(key: key);
  final String title;
  final VoidCallback onPressed;
  final double width;
  final double height;
  final double fontSize;
  final double marginBottom;
  final Color color;
  final Color color2;
  final double radius;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        width: width ?? ScreensHelper.of(context).fromWidth(80),
        height: height ?? 40,
        margin: marginBottom == null
            ? const EdgeInsets.only(bottom: 8)
            : EdgeInsets.only(bottom: marginBottom),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(radius??10),
          color:color2,
        ),
        child: Center(
          child: Text(
            title,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: fontSize ?? 18,
                color: color),
          ),
        ),
      ),
    );
  }
}
