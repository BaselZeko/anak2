import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:anak/core/services/navigation_service.dart';

ContinuousRectangleBorder continuousRectangleBorder = const ContinuousRectangleBorder();

class CustomAppbar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final Widget leading;
  final List<Widget> actions;
  PreferredSizeWidget bottom;

  CustomAppbar({Key key, this.title, this.actions, this.leading, this.bottom}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0.3,
      title: Text(
        title,
        style: GoogleFonts.actor(color: GlobalColors.black, fontWeight: FontWeight.bold),
      ),
      actions: actions,
      shape: continuousRectangleBorder,
      centerTitle: true,
      leading: leading,
      //backgroundColor: Config.primaryColor,
      flexibleSpace: Container(
        decoration: const BoxDecoration(color: GlobalColors.appBar),
      ),
      bottom: bottom,
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(AppBar().preferredSize.height);
}
