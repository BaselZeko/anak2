import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:flutter/material.dart';

typedef OnSelectedTab = void Function(int index);

class BottomNavBarItem {
  final String icons;
  final String label;

  BottomNavBarItem({this.icons, this.label});
}

class CustomBottomNavBar extends StatefulWidget {
  const CustomBottomNavBar(
      {Key key,
      @required this.items,
      @required this.onSelectedTab,
      @required this.currentIndex,
      this.iconSize,
      this.backgroundColor,
      this.labelSize,
      @required this.selectedColor,
      @required this.unSelectedColor})
      : super(key: key);

  final List<BottomNavBarItem> items;
  final OnSelectedTab onSelectedTab;
  final int currentIndex;
  final Color selectedColor;
  final Color unSelectedColor;
  final Color backgroundColor;
  final double iconSize;
  final double labelSize;

  @override
  _CustomBottomNavBarState createState() => _CustomBottomNavBarState();
}

class _CustomBottomNavBarState extends State<CustomBottomNavBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      //padding: EdgeInsets.symmetric(vertical: 12),
      height: kBottomNavigationBarHeight  ,
      width: ScreensHelper.of(context).fromWidth(100),
      decoration: BoxDecoration(
        color: widget.backgroundColor ?? GlobalColors.primaryColor,
        gradient: const LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            colors: <Color>[
              GlobalColors.primaryColor,
              GlobalColors.primaryColor,
            ]),
        borderRadius: const BorderRadius.only(
            topRight: Radius.circular(19), topLeft: Radius.circular(19)),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: widget.items
            .map((e) => Expanded(
                  child: InkResponse(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SvgPicture.asset(e.icons,
                            height:widget.iconSize ?? 24 ,
                            width: widget.iconSize ?? 24,
                            color:
                                widget.items.indexOf(e) == widget.currentIndex
                                    ? widget.selectedColor
                                    : widget.unSelectedColor),
                        const SizedBox(height: 4,),
                        Text(
                          e.label,
                          style: TextStyle(
                              fontSize: widget.labelSize,
                              color:
                                  widget.items.indexOf(e) == widget.currentIndex
                                      ? widget.selectedColor
                                      : widget.unSelectedColor),
                        )
                      ],
                    ),
                    onTap: () {
                      widget.onSelectedTab(widget.items.indexOf(e));
                    },
                  ),
                ))
            .toList(),
      ),
    );
  }
}
