import 'package:flutter/material.dart';
import 'package:anak/core/resources/colors.dart';

class EmptyPage extends StatelessWidget {
  const EmptyPage({Key key, this.icon,@required this.text}) : super(key: key);
  final String text;
  final Widget icon;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          if (icon != null) icon,
          Container(
            margin: const EdgeInsets.only(top: 8),
            child: Text(
              text,
              style: const TextStyle(
                  color: GlobalColors.greyText,
                  fontWeight: FontWeight.w500,
                  fontSize: 16),
            ),
          )
        ],
      ),
    );
  }
}
