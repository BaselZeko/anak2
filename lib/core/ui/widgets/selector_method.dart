import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/resources/langyage.dart';
import 'package:flutter/material.dart';

typedef OnSelectChange = Function(String choice);

class SelectorMethod extends StatelessWidget {
  const SelectorMethod(
      {Key key,
        this.title,
        this.onSelectChange,
        this.choice,
        this.groupValue,
        this.width,
        this.height})
      : super(key: key);
  final String title;
  final String choice;
  final  String groupValue;
  final OnSelectChange onSelectChange;
  final double width;
  final double height;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onSelectChange(choice);
      },
      child: Container(
        height: height ?? 40,
        width: width ?? ScreensHelper.of(context).fromWidth(80),
        padding: const EdgeInsets.all(8.0),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            border: Border.all(color: GlobalColors.grey)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              height: 15,
              width: 15,
              margin: const EdgeInsetsDirectional.only(end: 8),
              decoration: BoxDecoration(
                  color: groupValue == choice
                      ? GlobalColors.primaryColor
                      : GlobalColors.transparent,
                  borderRadius: BorderRadius.circular(100),
                  border: Border.all(color: GlobalColors.grey)),
            ),
            Text(
              title,
              style: const TextStyle(fontWeight: FontWeight.w500, fontSize: 16),
            )
          ],
        ),
      ),
    );
  }
}