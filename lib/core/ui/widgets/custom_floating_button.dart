import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/resources/images.dart';

class AnimatedFloatingButton extends StatefulWidget {
  const AnimatedFloatingButton({Key key, this.onPress, this.text}) : super(key: key);
  final String text;
  final VoidCallback onPress;

  @override
  _AnimatedFloatingButtonState createState() => _AnimatedFloatingButtonState();
}

class _AnimatedFloatingButtonState extends State<AnimatedFloatingButton> with SingleTickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      lowerBound: 0.5,
      upperBound: 1.5,
      reverseDuration: const Duration(seconds: 2),
      duration: const Duration(seconds: 2),
    )..repeat();

    _controller.addListener(() {
      setState(() {});
    });
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.onPress,
      child: SizedBox(
        height: 60,
        child: Stack(
          alignment: Alignment.center,
          children: [
            Opacity(
              opacity: (1 - _controller.value).abs(),
              child: Container(
                height: _controller.value > 1.0 ? 56 - (1 - _controller.value) : 56 + _controller.value,
                width: _controller.value > 1.0
                    ? ScreensHelper.of(context).fromWidth(96.4) - (1 - _controller.value)
                    : ScreensHelper.of(context).fromWidth(96.4) + _controller.value,
                decoration: BoxDecoration(
                    color: GlobalColors.secondaryColor,
                    borderRadius: BorderRadius.circular(50),
                    gradient: const LinearGradient(colors: [
                      GlobalColors.secondaryColor,
                      GlobalColors.primaryColor,
                    ])),
              ),
            ),
            Container(
              width: ScreensHelper.of(context).fromWidth(95),
              height: 50,
              decoration: BoxDecoration(
                  color: GlobalColors.secondaryColor,
                  borderRadius: BorderRadius.circular(50),
                  gradient: const LinearGradient(colors: [
                    GlobalColors.secondaryColor,
                    GlobalColors.primaryColor,
                  ])),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 14),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 4.0),
                          child: Text(
                            widget.text,
                            style: const TextStyle(
                                fontSize: 16, color: GlobalColors.white, fontWeight: FontWeight.bold),
                          ),
                        ),
                        SvgPicture.asset(
                          GlobalImages.cart,
                          width: 20,
                          height: 20,
                          color: Colors.white,
                        ),
                      ],
                    ),
                    const Text(
                      'View Cart',
                      style: TextStyle(fontSize: 18, color: GlobalColors.white, fontWeight: FontWeight.w400),
                    ),
                   /* const Text(
                      '0.00 SAR',
                      style: TextStyle(fontSize: 16, color: GlobalColors.white, fontWeight: FontWeight.bold),
                    )*/
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
