import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:anak/core/services/user_local_management.dart';
import 'package:anak/core/ui/pages/root_page.dart';

import 'core/blocs/application_bloc.dart';
import 'core/helper/extensions/material_color_converter.dart';
import 'core/localization/app_lang.dart';
import 'core/localization/app_localization.dart';
import 'core/resources/colors.dart';
import 'core/services/localization_storage.dart';
import 'core/ui/pages/intro_screen.dart';
import 'core/ui/pages/splash_screen.dart';
import 'features/cart/presentation/pages/cart_page.dart';
import 'features/cheak_order/presentation/cheak_payment.dart';
import 'features/home_page/presentation/home_page.dart';
import 'features/home_page/presentation/prands_page.dart';

class MyApp extends StatefulWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final _applicationBloc =
  ApplicationBloc(LocalStorageService(), UserLocalManagement());

  @override
  void initState() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    _applicationBloc.initApp();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: _applicationBloc,
      builder: (context, state) {
        return StreamBuilder(
            stream: appLanguage.stream.distinct(),
            initialData: (state as ApplicationState).appLanguages.index ==
                AppLanguages.AR.index
                ? const Locale('ar', '')
                : const Locale('en', ''),
            builder: (context, language) {
              return MaterialApp(
                  builder: (context, child) {
                    return ScrollConfiguration(
                      behavior: MyBehavior(),
                      child: child,
                    );
                  },
                  title: 'vendor',
                  debugShowCheckedModeBanner: false,
                  theme: ThemeData(
                      primarySwatch:
                      GlobalColors.primaryColor.toMaterialColor(),
                      fontFamily:
                      'cocon' //Changing this will change the color of the TabBar
                  ),

                  ///Localization
                  // locale: language.data == AppLanguages.EN.index
                  //     ? const Locale('en', '')
                  //     : const Locale('ar', ''),
                  locale:const Locale('ar', '') ,
                  localizationsDelegates: const [
                    AppLocalizations.delegate,
                    GlobalMaterialLocalizations.delegate,
                    GlobalWidgetsLocalizations.delegate,
                    DefaultCupertinoLocalizations.delegate,
                    GlobalCupertinoLocalizations.delegate
                  ],
                  supportedLocales: const [
                    Locale('en', ''), // English
                    Locale('ar', ''), // Arabic
                  ],

                  // home:const PrandsPage());
                  // home: const RootPage());
                  home:  CheakPayment());
            });
      },
    );
  }
}

class MyBehavior extends ScrollBehavior {
  @override
  Widget buildOverscrollIndicator(
      BuildContext context, Widget child, ScrollableDetails details) {
    return child;
  }
}
