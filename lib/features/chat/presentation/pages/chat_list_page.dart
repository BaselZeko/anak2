import 'package:anak/core/helper/extensions/translation.dart';
import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/ui/animation/fade_tans_animation.dart';
import 'package:anak/core/ui/widgets/custom_text_field.dart';
import 'package:anak/features/cart/presentation/pages/cart_page.dart';
import 'package:anak/features/chat/presentation/widgets/chat_item_widget.dart';
import 'package:flutter/material.dart';

class ChatListPage extends StatefulWidget {
  @override
  _ChatListPageState createState() => _ChatListPageState();
}

class _ChatListPageState extends State<ChatListPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            height: ScreensHelper.of(context).fromHeight(100),
            width: ScreensHelper.of(context).fromWidth(100),
            color: GlobalColors.primaryColor,
          ),
          Positioned(
              top: ScreensHelper.of(context).fromHeight(5),
              child: SizedBox(
                width: ScreensHelper.of(context).fromWidth(100),
                height: 40,
                child: Padding(
                 padding: EdgeInsets.symmetric(horizontal: ScreensHelper.of(context).fromWidth(5)),
                  child: CustomTextField(
                    hint: "looking_for_something".tr(context),
                    suffix: const Icon(
                      Icons.search,
                      color: Colors.white,
                    ),
                    fillColor: GlobalColors.primaryColor,
                    hintSize: 14,
                  ),
                ),
              )),
          Positioned(
            top: ScreensHelper.of(context).fromHeight(12),
            child: ClipPath(
              clipper: BackgroundClipper(),
              child: Container(
                height: ScreensHelper.of(context).fromHeight(90),
                width: ScreensHelper.of(context).fromWidth(100),
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(80),
                      topRight: Radius.circular(80),
                    )),
                child: Padding(
                  padding: EdgeInsets.only(
                    bottom: ScreensHelper.of(context).fromHeight(12),
                      left: ScreensHelper.of(context).fromWidth(4),
                      right: ScreensHelper.of(context).fromWidth(4),
                      top: ScreensHelper.of(context).fromHeight(5)),
                  child: ListView.separated(
                    shrinkWrap: true,
                    itemCount: 10,
                    itemBuilder: (context, index) {
                      return FadeTransAnimation(
                          delayInMillisecond: 500 + (100 * index),
                          child: ChatItemWidget()); /////////////////
                    },
                    separatorBuilder: (context, index) {
                      return SizedBox(
                        height: ScreensHelper.of(context).fromHeight(2),
                        child: const Divider(
                          color: Color(0xffeeeeee),
                          thickness: 1.2,
                        ),
                      );
                    },
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
