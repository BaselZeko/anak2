import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/resources/images.dart';
import 'package:flutter/material.dart';


class ChatItemWidget extends StatefulWidget {
  @override
  _ChatItemWidgetState createState() => _ChatItemWidgetState();
}

class _ChatItemWidgetState extends State<ChatItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [

        Image.asset(
          GlobalImages.store1,
          width: ScreensHelper.of(context).fromWidth(15),
          height: ScreensHelper.of(context).fromWidth(15),
          fit: BoxFit.cover,
        ),

        Expanded(
          child: Container(
            height: ScreensHelper.of(context).fromWidth(15),
            padding: EdgeInsets.symmetric(
                horizontal: ScreensHelper.of(context).fromHeight(1.5)
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.start,
              children:  const[
                Text(
                  "copon1",
                  style: TextStyle(
                      fontSize: 17,
                      color: GlobalColors.blackText,
                      fontWeight: FontWeight.w600),
                  overflow: TextOverflow.ellipsis,
                ),
                Text(
                  'صالحة من 2/2/2022 الى 1/3/2022',
                  style: TextStyle(fontSize: 16,color: GlobalColors.greyText),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
