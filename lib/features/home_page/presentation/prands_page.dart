import 'package:anak/core/helper/extensions/translation.dart';
import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/resources/images.dart';
import 'package:anak/core/ui/animation/fade_tans_animation.dart';
import 'package:anak/core/ui/widgets/button.dart';
import 'package:anak/core/ui/widgets/custom_text_field.dart';
import 'package:anak/features/cart/presentation/pages/cart_page.dart';
import 'package:anak/features/home_page/widgets/prands_widgets/prand_card.dart';
import 'package:anak/features/home_page/widgets/special_store.dart';
import 'package:anak/features/home_page/widgets/store_card.dart';
import 'package:flutter/material.dart';

class PrandsPage extends StatefulWidget {
  const PrandsPage({Key key}) : super(key: key);
  @override
  _PrandsPageState createState() => _PrandsPageState();
}

class _PrandsPageState extends State<PrandsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            height: ScreensHelper.of(context).fromHeight(100),
            width: ScreensHelper.of(context).fromWidth(100),
            color: GlobalColors.primaryColor,
          ),
          Positioned(
              top: ScreensHelper.of(context).fromHeight(4),
              child: SizedBox(
                width: ScreensHelper.of(context).fromWidth(100),
                height: 40,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    const Icon(Icons.list,color: GlobalColors.white,),
                    SizedBox(
                      width: ScreensHelper.of(context).fromWidth(75),
                      child: CustomTextField(
                        hint: "looking_for_something".tr(context),
                        suffix: const Icon(
                          Icons.search,
                          color: Colors.white,
                        ),
                        fillColor: GlobalColors.primaryColor,
                        hintSize: 14,
                      ),
                    ),
                    const Icon(Icons.arrow_back,color: GlobalColors.white),

                  ],
                ),
              )),
          Positioned(
            top: ScreensHelper.of(context).fromHeight(12),
            child: ClipPath(
              clipper: BackgroundClipper(),
              child: Container(
                height: ScreensHelper.of(context).fromHeight(90),
                width: ScreensHelper.of(context).fromWidth(100),
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(80),
                      topRight: Radius.circular(80),
                    )),
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: ScreensHelper.of(context).fromWidth(4),
                      vertical: ScreensHelper.of(context).fromHeight(5)),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(
                          height: 15,
                        ),
                        const Text(
                          "البراندات حقتنا",
                          style: TextStyle(
                            fontSize: 18,
                            color: GlobalColors.blackText,
                            fontWeight: FontWeight.w600,
                          ),
                        ),

                        ListView.separated(
                          physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: 4,
                          itemBuilder: (context, index) {
                            return FadeTransAnimation(
                                delayInMillisecond: 500 + (100 * index),
                                child: PrandCardWidget()); /////////////////
                          },
                          separatorBuilder: (context, index) {
                            return const Divider(
                              color: Color(0xffeeeeee),
                              thickness: 1.2,
                            );
                          },
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                        SpecialStoreWidget(),
                        const SizedBox(
                          height: 15,
                        ),
                        ListView.separated(
                          physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: 6,
                          itemBuilder: (context, index) {
                            return FadeTransAnimation(
                                delayInMillisecond: 500 + (100 * index),
                                child: PrandCardWidget()); /////////////////
                          },
                          separatorBuilder: (context, index) {
                            return const Divider(
                              color: Color(0xffeeeeee),
                              thickness: 1.2,
                            );
                          },
                        ),
                        SizedBox(
                          height: ScreensHelper.of(context).fromWidth(20),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
