import 'package:anak/core/helper/extensions/translation.dart';
import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/resources/images.dart';
import 'package:anak/core/ui/animation/fade_tans_animation.dart';
import 'package:anak/core/ui/widgets/custom_text_field.dart';
import 'package:anak/features/home_page/data/model/data.dart';
import 'package:anak/features/home_page/presentation/product_info_page.dart';
import 'package:anak/features/home_page/widgets/discount_coupon.dart';
import 'package:anak/features/home_page/widgets/porducts.dart';
import 'package:anak/features/home_page/widgets/special_store.dart';
import 'package:anak/features/home_page/widgets/store_card.dart';
import 'package:anak/features/home_page/widgets/sub_category.dart';
import 'package:animations/animations.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: ScreensHelper.of(context).fromHeight(100),
        width: ScreensHelper.of(context).fromWidth(100),
        color: GlobalColors.primaryColor,
        child: SingleChildScrollView(
          child: Column(
            children: [
              const SizedBox(
                height: 40,
              ),
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                width: ScreensHelper.of(context).fromWidth(90),
                height: 40,
                child: CustomTextField(
                  hint: "looking_for_something".tr(context),
                  suffix: const Icon(
                    Icons.search,
                    color: Colors.white,
                  ),
                  fillColor: GlobalColors.primaryColor,
                  hintSize: 14,
                ),
              ),
              SizedBox(
                height: ScreensHelper.of(context).fromHeight(90),
                child: Stack(
                  children: [
                    Container(
                      color: GlobalColors.primaryColor,
                    ),
                    SizedBox(
                      width: ScreensHelper.of(context).fromWidth(100),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                              width: ScreensHelper.of(context).fromWidth(5)),
                          SizedBox(
                            width: ScreensHelper.of(context).fromWidth(30),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                SizedBox(
                                  height:
                                      ScreensHelper.of(context).fromHeight(5),
                                ),
                                const Text(
                                  "20%",
                                  style: TextStyle(
                                      color: GlobalColors.lightPrimary,
                                      fontSize: 50,
                                      fontWeight: FontWeight.bold),
                                ),
                                SizedBox(
                                    width:
                                        ScreensHelper.of(context).fromWidth(20),
                                    child: const Text(
                                      "كاش باك حتى 30 ريال",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color: GlobalColors.white,
                                        fontSize: 16,
                                      ),
                                    )),
                              ],
                            ),
                          ),
                          Image.asset(
                            GlobalImages.intro1,
                            width: ScreensHelper.of(context).fromWidth(60),
                          ),
                          SizedBox(
                              width: ScreensHelper.of(context).fromWidth(5)),
                        ],
                      ),
                    ),
                    Positioned(
                      top: ScreensHelper.of(context).fromHeight(25),
                      child: Container(
                        height: ScreensHelper.of(context).fromHeight(100),
                        width: ScreensHelper.of(context).fromWidth(100),
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(50),
                              topLeft: Radius.circular(50)),
                          color: Colors.white,
                        ),
                        padding: EdgeInsets.symmetric(
                          horizontal: ScreensHelper.of(context).fromWidth(4),
                          vertical: ScreensHelper.of(context).fromHeight(5),
                        ),
                        child: SingleChildScrollView(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: const [
                                  Text(
                                    "What do you want today",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16),
                                  ),
                                  Text(
                                    "Show All",
                                    style: TextStyle(
                                        color: Colors.grey, fontSize: 14),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: ScreensHelper.of(context).fromHeight(1),
                              ),
                              SizedBox(
                                height:
                                    ScreensHelper.of(context).fromHeight(14),
                                child: ListView.builder(
                                  scrollDirection: Axis.horizontal,
                                  shrinkWrap: true,
                                  itemCount: 6,
                                  itemBuilder: (context, index) {
                                    return FadeTransAnimation(
                                      delayInMillisecond: 200,
                                      child: const SubCategoryWidget(),
                                    );
                                  },
                                ),
                              ),
                              const SizedBox(
                                height: 30,
                              ),
                              const Text(
                                "latest thing in souk",
                                style: TextStyle(
                                    color: GlobalColors.blackText,
                                    fontSize: 18,
                                    fontWeight: FontWeight.w900),
                              ),
                              GridView.builder(
                                  padding: const EdgeInsets.only(bottom: 60),
                                  physics: const BouncingScrollPhysics(),
                                  shrinkWrap: true,
                                  gridDelegate:
                                      const SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 2,
                                    childAspectRatio: 0.65,
                                  ),
                                  itemCount: productList.length,
                                  itemBuilder: (context, index) {
                                    return Transform.translate(
                                      offset: const Offset(0.0, 30),
                                      child: OpenContainer(
                                        transitionDuration: const Duration(seconds: 1),
                                        openBuilder: (context, action) {
                                          return ProductInfoPage();
                                        },
                                        closedBuilder: (context, action) {
                                          return Product(
                                            index: index,
                                          );
                                        },
                                      ),
                                    );
                                  }),
                              DiscountCouponWidget(),
                              const SizedBox(
                                height: 30,
                              ),
                              SpecialStoreWidget(),
                              const SizedBox(
                                height: 20,
                              ),
                              const Text(
                                "أفضل المتاجر الالكترونية",
                                style: TextStyle(
                                  fontSize: 17,
                                  color: GlobalColors.blackText,
                                  fontWeight: FontWeight.w600,
                                ),
                                overflow: TextOverflow.ellipsis,
                              ),
                              SizedBox(
                                height: ScreensHelper.of(context).fromHeight(1),
                              ),
                              SizedBox(
                                height:
                                    ScreensHelper.of(context).fromHeight(10),
                                child: ListView.builder(
                                  scrollDirection: Axis.horizontal,
                                  shrinkWrap: true,
                                  itemCount: 6,
                                  itemBuilder: (context, index) {
                                    return FadeTransAnimation(
                                      delayInMillisecond: 200,
                                      child: const Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 4.0),
                                        child: CircleAvatar(
                                          radius: 35,
                                          backgroundImage: AssetImage(
                                            GlobalImages.intro1,
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              ),
                              SizedBox(
                                height: ScreensHelper.of(context).fromHeight(50),
                              )
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
}
