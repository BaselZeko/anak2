import 'package:anak/core/helper/extensions/translation.dart';
import 'package:anak/core/services/navigation_service.dart';
import 'package:anak/core/ui/animation/fade_tans_animation.dart';
import 'package:anak/core/ui/widgets/button.dart';
import 'package:anak/features/home_page/widgets/star_widget.dart';
import 'package:anak/features/more_pages/presentation/widgets/copon_widget.dart';
import 'package:flutter/material.dart';
import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/resources/images.dart';
import 'package:anak/features/cart/presentation/pages/cart_page.dart';

class ProductInfoPage extends StatefulWidget {
  @override
  _ProductInfoPageState createState() => _ProductInfoPageState();
}

class _ProductInfoPageState extends State<ProductInfoPage> {
  List<Color> listColors = [
    GlobalColors.primaryColor,
    GlobalColors.blackText,
    GlobalColors.yellow,
    GlobalColors.blue,
  ];

  List<int> sized = [
    38,40,42,44,46,48
  ];

  Widget getcolors(Color color, Widget child,double size) {
    return Container(
      width: size,
      height: size,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(4),
        color: color,
      ),
      child: child,
    );
  }

  Widget listWidgets<T>(T list,double size) {
    var value = list as List;
    return SizedBox(
      height: size,
      child: ListView.separated(
        physics: const NeverScrollableScrollPhysics(),
        scrollDirection: Axis.horizontal,
        shrinkWrap: true,
        itemCount: value.length,
        itemBuilder: (context, index) {
          if(value is List<Color>){
            return FadeTransAnimation(
              delayInMillisecond: 500 + (100 * index),
              child: getcolors(listColors[index], const SizedBox(),size),
            );
          }else{
            return FadeTransAnimation(
              delayInMillisecond: 500 + (100 * index),
              child: getcolors(Colors.transparent,
                Text(
                sized[index].toString(),
                style:const TextStyle(
                  fontSize: 16,
                  color: GlobalColors.blackText,
                  fontWeight: FontWeight.w500,
                ),
                overflow: TextOverflow.ellipsis,
              ),size),
            );
          }

        },
        separatorBuilder: (BuildContext context, int index) {
          return const SizedBox(
            width: 8,
          );
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    double rating = 3.5;
    return Scaffold(
      body: Stack(
        children: [
          Container(
            height: ScreensHelper.of(context).fromHeight(100),
            width: ScreensHelper.of(context).fromWidth(100),
            color: GlobalColors.primaryColor,
          ),
          SizedBox(
              width: ScreensHelper.of(context).fromWidth(100),
              height: ScreensHelper.of(context).fromHeight(45),
              child: Image.asset(
                GlobalImages.model,
                fit: BoxFit.fill,
              )),
          Positioned(
            // left: ScreensHelper.of(context).fromWidth(35),
            top: ScreensHelper.of(context).fromHeight(33),
            child: SizedBox(
              width: ScreensHelper.of(context).fromWidth(100),
              child: Center(
                child: InkWell(
                  onTap: () {
                    // Navigator.pop(context);
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(width: 1.7, color: Colors.white),
                      borderRadius: const BorderRadius.all(
                        Radius.circular(50),
                      ),
                      color: GlobalColors.primaryColor,
                    ),
                    padding: const EdgeInsets.all(8),
                    child: const Icon(
                      Icons.favorite_border,
                      size: 20,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
          ),
          Positioned(
              left: ScreensHelper.of(context).fromWidth(43),
              top: ScreensHelper.of(context).fromHeight(6),
              child: SizedBox(
                  width: ScreensHelper.of(context).fromWidth(100),
                  child: Center(
                      child: InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: const Icon(
                            Icons.arrow_back,
                            size: 24,
                            color: GlobalColors.primaryColor,
                          ))))),
          Positioned(
            top: ScreensHelper.of(context).fromHeight(37),
            child: ClipPath(
              clipper: BackgroundClipper(),
              child: Container(
                height: ScreensHelper.of(context).fromHeight(90),
                width: ScreensHelper.of(context).fromWidth(100),
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(50),
                      topRight: Radius.circular(50),
                    )),
                child: Center(
                  child: Container(
                    padding: EdgeInsets.symmetric(
                        horizontal: ScreensHelper.of(context).fromWidth(5),
                        vertical: ScreensHelper.of(context).fromHeight(5)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Text(
                                  "170.6" " " + "sar".tr(context),
                                  style: const TextStyle(
                                      fontSize: 19,
                                      color: GlobalColors.blackText,
                                      fontWeight: FontWeight.w700),
                                  overflow: TextOverflow.ellipsis,
                                ),
                                SizedBox(
                                  width: ScreensHelper.of(context).fromWidth(5),
                                ),
                                Text(
                                  "180.1" " " + "sar".tr(context),
                                  style: const TextStyle(
                                    fontSize: 15,
                                    color: GlobalColors.greyText,
                                    fontWeight: FontWeight.w400,
                                    decoration: TextDecoration.lineThrough,
                                  ),
                                  overflow: TextOverflow.ellipsis,
                                ),
                                SizedBox(
                                  width: ScreensHelper.of(context).fromWidth(5),
                                ),
                                const Text(
                                  "10 %",
                                  style: TextStyle(
                                    fontSize: 15,
                                    color: GlobalColors.primaryColor,
                                    fontWeight: FontWeight.w400,
                                  ),
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 12,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: const [
                                Text(
                                  "جاكيت نسائي طويل",
                                  style: TextStyle(
                                      fontSize: 17,
                                      color: GlobalColors.blackText,
                                      fontWeight: FontWeight.w400),
                                  overflow: TextOverflow.ellipsis,
                                ),
                                Text(
                                  "نساء ,جاكيت",
                                  style: TextStyle(
                                      fontSize: 15,
                                      color: GlobalColors.greyText,
                                      fontWeight: FontWeight.w400),
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 12,
                            ),
                            const Text(
                              "جاكيت طويل قطن نسبة القطن 70% و نسبة النايلون 30% يوجد منه اكثر من لون وطوله 80 سم ومبطن داخله بفرو ناعم",
                              style: TextStyle(
                                  fontSize: 14,
                                  color: GlobalColors.greyText,
                                  fontWeight: FontWeight.w400),
                            ),
                            const SizedBox(
                              height: 12,
                            ),
                            Row(
                              children: [
                                StarRating(
                                  rating: rating,
                                  size: 22,
                                  onRatingChanged: (rating) => setState(() {}),
                                ),
                                SizedBox(
                                  width: ScreensHelper.of(context).fromWidth(5),
                                ),
                                Row(
                                  children: [
                                    Text(
                                      rating.toString(),
                                      style: const TextStyle(
                                        fontSize: 15,
                                        color: GlobalColors.blackText,
                                        fontWeight: FontWeight.w600,
                                      ),
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                    SizedBox(
                                        width: 10,
                                        height: ScreensHelper.of(context)
                                            .fromHeight(2),
                                        child: const VerticalDivider(
                                          color: GlobalColors.greyText,
                                          width: 4,
                                          thickness: 1.2,
                                        )),
                                    const Text(
                                      "16 orders",
                                      style: TextStyle(
                                        fontSize: 15,
                                        color: GlobalColors.greyText,
                                        fontWeight: FontWeight.w400,
                                      ),
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ],
                                ),
                                Spacer(),
                                const Text(
                                  "all comment",
                                  style: TextStyle(
                                    fontSize: 16,
                                    color: GlobalColors.blackText,
                                    fontWeight: FontWeight.w500,
                                  ),
                                  overflow: TextOverflow.ellipsis,
                                ),
                                const SizedBox(
                                  width: 4,
                                ),
                                const Icon(
                                  Icons.arrow_forward,
                                  color: GlobalColors.greyText,
                                  size: 20,
                                )
                              ],
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        const SizedBox(
                          height: 2,
                          child: Divider(
                            color: Color(0xffE5E5E5),
                            thickness: 1,
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        const Text(
                          "colors",
                          style:  TextStyle(
                            fontSize: 18,
                            color: GlobalColors.blackText,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                        const SizedBox(
                          height: 8,
                        ),
                        listWidgets<List<Color>>(listColors,27),
                        const SizedBox(
                          height: 10,
                        ),
                        const Text(
                          "sizes",
                          style:  TextStyle(
                            fontSize: 18,
                            color: GlobalColors.blackText,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                        const SizedBox(
                          height: 8,
                        ),
                        listWidgets<List<int>>(sized,33),
                        SizedBox(
                          height: ScreensHelper.of(context).fromWidth(10),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
