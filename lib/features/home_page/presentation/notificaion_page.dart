import 'package:anak/core/helper/extensions/translation.dart';
import 'package:anak/core/ui/animation/fade_tans_animation.dart';
import 'package:anak/features/home_page/widgets/notificaton_widget.dart';
import 'package:anak/features/more_pages/presentation/widgets/walt_info_widget.dart';
import 'package:flutter/material.dart';
import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/resources/images.dart';
import 'package:anak/features/cart/presentation/pages/cart_page.dart';

class NotificaionPage extends StatefulWidget {
  @override
  _NotificaionPageState createState() => _NotificaionPageState();
}

class _NotificaionPageState extends State<NotificaionPage> {

  bool notifiActive = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            height: ScreensHelper.of(context).fromHeight(100),
            width: ScreensHelper.of(context).fromWidth(100),
            color: GlobalColors.primaryColor,
          ),
          Positioned(
              top: ScreensHelper.of(context).fromHeight(6),
              child: SizedBox(
                  width: ScreensHelper.of(context).fromWidth(100),
                  child: const Center(
                      child: Text(
                        "الإشعارات",
                        style: TextStyle(fontSize: 22, color: Colors.white),
                      )))),

          Positioned(
            top: ScreensHelper.of(context).fromHeight(12),
            child: ClipPath(
              clipper: BackgroundClipper(),
              child: Container(
                height: ScreensHelper.of(context).fromHeight(90),
                width: ScreensHelper.of(context).fromWidth(100),
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(60),
                    topRight: Radius.circular(60),
                  ),
                ),
                padding: EdgeInsets.symmetric(
                  horizontal: ScreensHelper.of(context).fromWidth(3),
                  vertical: ScreensHelper.of(context).fromHeight(8),
                ),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: ScreensHelper.of(context).fromHeight(2),
                      ),
                      Container(
                        width: ScreensHelper.of(context).fromWidth(90),
                        decoration: BoxDecoration(
                          color: Color(0xffFfffff),
                          borderRadius: const BorderRadius.all(Radius.circular(12)),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey.withOpacity(0.2),
                                blurRadius: 1.0,
                                spreadRadius: 1,
                                offset: const Offset(-2, 1) // changes position of shadow
                            ),
                          ],
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              notifiActive == true
                                  ? "إلغاء تفعيل الاشعارات"
                                  : "تفعيل الاشعارات",
                              style: const TextStyle(
                                  color: GlobalColors.blackText,
                                  fontSize: 17,
                                  fontWeight: FontWeight.w600),
                              textAlign: TextAlign.center,
                            ),
                            Switch.adaptive(
                              value: notifiActive,
                              onChanged: (bool value) {
                                setState(() {
                                  notifiActive = !notifiActive;
                                });
                              },
                              activeColor: GlobalColors.primaryColor,
                            ),
                          ],
                        ),

                      ),
                      SizedBox(
                        height: ScreensHelper.of(context).fromHeight(1),
                      ),
                      ListView.builder(
                        physics: const NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: 10,
                        itemBuilder: (context, index) {
                          return FadeTransAnimation(
                            delayInMillisecond: 500 + (100 * index),
                            child: Padding(
                              padding: const EdgeInsets.only(bottom: 8.0),
                              child: NotificatonWidget(
                                imageUrl: GlobalImages.prand1,
                                lable: "Lorem ipsum dolor sit amet, consectetur orem ipsum dolor sit amet, consectetur orem ipsum dolor sit amet, consectetur orem ipsum dolor sit amet, consectetur orem ipsum dolor sit amet, consectetur orem ipsum dolor sit amet, consectetur orem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
                                date: "26/2/2022 9:85 AM",
                              ),
                            )
                          );
                        },
                      ),
                      SizedBox(
                        height: ScreensHelper.of(context).fromHeight(5),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
