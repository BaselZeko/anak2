
import 'package:anak/features/home_page/data/model/prands.dart';

final _product1 = PrandModel(
    name: 'Ella Armchair',
    location: 'Carl MH Barenbrug',
    imageUrl: 'assets/images/product1.jpg',
    color: 'White',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, ',
    madeIn: 'Russia',
    rating: 4,
    style: 'Modern',
    addvertismentType: "advert",
    services:"أطفال , نساء,رجال"

);

final _product2 = PrandModel(
    name: 'Ella Armchair',
    location: 'Carl MH Barenbrug',
    imageUrl: 'assets/images/product1.jpg',
    color: 'White',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, ',
    madeIn: 'Russia',
    rating: 4,
    style: 'Modern',
    addvertismentType: "advert",
    services:"أطفال , نساء,رجال"

);

final _product3 = PrandModel(
    name: 'Ella Armchair',
    location: 'Carl MH Barenbrug',
    imageUrl: 'assets/images/product1.jpg',
    color: 'White',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, ',
    madeIn: 'Russia',
    rating: 4,
    style: 'Modern',
    addvertismentType: "advert",
    services:"أطفال , نساء,رجال"

);

final _product4 = PrandModel(
    name: 'Ella Armchair',
    location: 'Carl MH Barenbrug',
    imageUrl: 'assets/images/product1.jpg',
    color: 'White',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, ',
    madeIn: 'Russia',
    rating: 4,
    style: 'Modern',
    addvertismentType: "advert",
    services:"أطفال , نساء,رجال"

);

final _product5 = PrandModel(
    name: 'Ella Armchair',
    location: 'Carl MH Barenbrug',
    imageUrl: 'assets/images/product1.jpg',
    color: 'White',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, ',
    madeIn: 'Russia',
    rating: 4,
    style: 'Modern',
    addvertismentType: "advert",
    services:"أطفال , نساء,رجال"

);

final _product6 = PrandModel(
    name: 'Ella Armchair',
    location: 'Carl MH Barenbrug',
    imageUrl: 'assets/images/product1.jpg',
    color: 'White',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, ',
    madeIn: 'Russia',
    rating: 4,
    style: 'Modern',
    addvertismentType: "advert",
    services:"أطفال , نساء,رجال"

);

final _product7 = PrandModel(
    name: 'Ella Armchair',
    location: 'Carl MH Barenbrug',
    imageUrl: 'assets/images/product1.jpg',
    color: 'White',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, ',
    madeIn: 'Russia',
    rating: 4,
    style: 'Modern',
    addvertismentType: "advert",
    services:"أطفال , نساء,رجال"

);


final List <PrandModel> prandList = [
  _product4,
  _product2,
  _product3,
  _product1,
  _product5,
  _product6,
  _product7,
];

List<String> categoryList = [
  'Interiors',
  'Furniture',
  'Moods',
  'Creators',
  'Home Appliances'
];

