class ProductModel{
  String name;
  String imageUrl;
  int price;
  String manufacturer;
  String description;
  String color;
  int rating;
  String style;
  String madeIn;
  bool thereDescount;
  double priceAfterDescount;

  ProductModel({
     this.name,
     this.imageUrl,
     this.price,
     this.manufacturer,
     this.description,
     this.color,
     this.rating,
     this.style,
     this.madeIn,
    this.priceAfterDescount,
    this.thereDescount
  });
}

