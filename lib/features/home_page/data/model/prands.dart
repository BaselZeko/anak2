class PrandModel{
  String name;
  String services;
  String imageUrl;
  String description;
  String location;
  String color;
  int rating;
  String style;
  String madeIn;
  String addvertismentType;

  PrandModel({
    this.name,
    this.imageUrl,
    this.services,
    this.location,
    this.description,
    this.color,
    this.rating,
    this.style,
    this.madeIn,
    this.addvertismentType,
  });
}

