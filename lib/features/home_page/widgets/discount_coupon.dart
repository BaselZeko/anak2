import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/resources/images.dart';
import 'package:flutter/material.dart';

class DiscountCouponWidget extends StatefulWidget {
  @override
  _DiscountCouponWidgetState createState() => _DiscountCouponWidgetState();
}

class _DiscountCouponWidgetState extends State<DiscountCouponWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreensHelper.of(context).width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          border: Border.all(
              color: GlobalColors.primaryColor, width: 1.5)),
      height: ScreensHelper.of(context).fromHeight(15),
      // padding: EdgeInsets.symmetric(horizontal: ScreensHelper.of(context).fromWidth(2)),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          SizedBox(
            width: ScreensHelper.of(context).fromWidth(20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                Text(
                  "30%",
                  style: TextStyle(
                      fontSize: 24,
                      color: GlobalColors.primaryColor,
                      fontWeight: FontWeight.w600),
                  overflow: TextOverflow.ellipsis,
                ),
                SizedBox(height: 10,),
                Text(
                  "خصم",
                  style: TextStyle(
                      fontSize: 14,
                      color: GlobalColors.primaryColor,
                      fontWeight: FontWeight.w400),
                  overflow: TextOverflow.ellipsis,
                ),
              ],
            ),
          ),

          SizedBox(
              height: ScreensHelper.of(context).fromHeight(12),
              child: Image.asset(GlobalImages.dashLine)),
          
          SizedBox(      
            width: ScreensHelper.of(context).fromWidth(2),
          ),

          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children:  [
                const Text(
                  "كوبون خصم ",
                  style: TextStyle(
                      fontSize: 16,
                      color: GlobalColors.blackText,
                      fontWeight: FontWeight.w600),
                  overflow: TextOverflow.ellipsis,
                ),
                const SizedBox(height: 8,),
                SizedBox(
                  width: MediaQuery.of(context).size.width*0.5,
                  child: const Text(
                    "قم بدعوة خمس من أصدقاءك واستفد من كوبون خصم",
                    style: TextStyle(
                        fontSize: 14,
                        color: GlobalColors.greyText,
                        fontWeight: FontWeight.w600),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
