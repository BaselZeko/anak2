import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:flutter/material.dart';

class NotificatonWidget extends StatelessWidget {
  String imageUrl;
  String lable;
  String date;
  NotificatonWidget({this.imageUrl, this.lable, this.date});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: ScreensHelper.of(context).fromHeight(10) + 40,
      child: Stack(
        children: [
          Positioned(
              top: ScreensHelper.of(context).fromHeight(10) - 12,
              left: ScreensHelper.of(context).fromWidth(6),
              child: Container(
                  decoration: BoxDecoration(
                    color: Color(0xffF8F8F8),
                    borderRadius: const BorderRadius.only(
                      bottomRight: Radius.circular(12),
                      bottomLeft: Radius.circular(12),
                    ),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.grey.withOpacity(0.2),
                          blurRadius: 1.0,
                          spreadRadius: 1,
                          offset:
                              const Offset(-2, 1) // changes position of shadow
                          ),
                    ],
                  ),
                  padding: EdgeInsets.only(
                      top: 20,
                      bottom: 8,
                      left: ScreensHelper.of(context).fromWidth(1),
                      right: ScreensHelper.of(context).fromWidth(1)),
                  width: ScreensHelper.of(context).fromWidth(35),
                  child: Center(
                      child: Text(
                    date,
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                        fontSize: 14,
                        color: GlobalColors.greyText,
                        fontWeight: FontWeight.w400),
                  )))),
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              width: ScreensHelper.of(context).fromWidth(90),
              height: ScreensHelper.of(context).fromHeight(10),
              decoration: BoxDecoration(
                color: Color(0xffFfffff),
                borderRadius: const BorderRadius.all(Radius.circular(12)),
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey.withOpacity(0.2),
                      blurRadius: 1.0,
                      spreadRadius: 1,
                      offset: const Offset(-2, 1) // changes position of shadow
                      ),
                ],
              ),
              padding: EdgeInsets.symmetric(
                  horizontal: ScreensHelper.of(context).fromWidth(2)),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  CircleAvatar(
                    radius: ScreensHelper.of(context).fromHeight(6),
                    backgroundColor: Colors.white,
                    backgroundImage: AssetImage(imageUrl),
                  ),
                  Expanded(
                    child: Container(
                      height: ScreensHelper.of(context).fromHeight(10),
                      padding: EdgeInsets.symmetric(
                          horizontal:
                              ScreensHelper.of(context).fromHeight(1.5),
                      vertical:
                          ScreensHelper.of(context).fromHeight(2)
                      ),
                      child: Text(
                        lable,
                        style: const TextStyle(
                            fontSize: 16,
                            color: GlobalColors.blackText,
                            fontWeight: FontWeight.w600),
                        textAlign: TextAlign.start,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
