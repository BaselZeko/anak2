import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/resources/images.dart';
import 'package:anak/features/home_page/data/model/prand_data.dart';
import 'package:anak/features/home_page/data/model/prands.dart';
import 'package:flutter/material.dart';

class PrandCardWidget extends StatefulWidget {
  bool isFav;
  PrandCardWidget({this.isFav});

  @override
  _PrandCardWidgetState createState() => _PrandCardWidgetState();
}

class _PrandCardWidgetState extends State<PrandCardWidget> {

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        ///prand image
        SizedBox(
          height: ScreensHelper.of(context).fromHeight(14),
          width: ScreensHelper.of(context).fromWidth(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children:  [
              Image.asset(
                GlobalImages.prand1,
                width: 60,
                height: 60,
                fit: BoxFit.contain,
              ),
              const SizedBox(
                height: 3,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                   Text(
                    "4.2",
                    style: TextStyle(
                        fontSize: 14,
                        color: GlobalColors.blackText,
                        fontWeight: FontWeight.w400),
                    overflow: TextOverflow.ellipsis,
                     textAlign: TextAlign.center,
                  ),
                  SizedBox(width: 2,),
                  Icon(
                    Icons.star,
                    color: Color(0xffFBBC05),
                    size: 18,
                  )
                ],
              ),
            ],
          ),
        ),

        ///divider

        SizedBox(
            height: ScreensHelper.of(context).fromHeight(10),
            child: const VerticalDivider(
              color: Color(0xffeeeeee),
              width: 4,
              thickness: 1.2,
            )),

        ///prand info
        Expanded(
          child: Container(
            padding: EdgeInsets.symmetric(
                horizontal: ScreensHelper.of(context).fromHeight(1.5)
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children:  [
                    const Text(
                      "ساكور - sackor ",
                      style: TextStyle(
                          fontSize: 17,
                          color: GlobalColors.blackText,
                          fontWeight: FontWeight.w600),
                      overflow: TextOverflow.ellipsis,
                    ),
                    Container(
                      color: GlobalColors.primaryColor,
                      padding: const EdgeInsets.symmetric(
                          horizontal: 10,vertical: 3),
                      child: const Text(
                        'اعلان',
                        style: TextStyle(color: GlobalColors.white),
                        textAlign: TextAlign.center,
                      ),
                    )
                  ],
                ),
                const  SizedBox(
                  height: 3,
                ),
                const Text(
                  "أزياء وملابس رجالي ,نسائي",
                  style: TextStyle(
                      fontSize: 13,
                      color: GlobalColors.grey,
                      fontWeight: FontWeight.w400),
                  overflow: TextOverflow.ellipsis,
                ),
                const SizedBox(
                  height: 3,
                ),
                Row(
                  children: const [
                    Icon(
                      Icons.location_on,
                      color: GlobalColors.primaryColor,
                      size: 18,
                    ),
                    Text(
                      "السعودية ,جدة",
                      style: TextStyle(
                          fontSize: 14,
                          color: GlobalColors.blackText,
                          fontWeight: FontWeight.w400),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],
                ),
                const SizedBox(
                  height: 2,
                ),
                Row(
                  children: [
                    Row(
                      children: const [
                        Icon(
                          Icons.keyboard_arrow_left,
                          color: GlobalColors.lightGrey,
                        ),
                        Text(
                          "أسرع شي",
                          style: TextStyle(
                              fontSize: 13,
                              color: GlobalColors.lightGrey,
                              fontWeight: FontWeight.w400),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ],
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    SizedBox(
                        height: ScreensHelper.of(context).fromHeight(2),
                        child: const VerticalDivider(
                          color: Color(0xffeeeeee),
                          width: 4,
                          thickness: 1.2,
                        )),
                    const SizedBox(
                      width: 8,
                    ),
                    const Text(
                      "التوصيل 10 ريال",
                      style: TextStyle(
                          fontSize: 13,
                          color: GlobalColors.lightBlue,
                          fontWeight: FontWeight.w400),
                      overflow: TextOverflow.ellipsis,
                    ),
                    const Spacer(),
                    widget.isFav == false ? const Icon(
                      Icons.favorite_border,
                      color: GlobalColors.primaryColor,
                      size: 20,
                    ): const Icon(
                      Icons.favorite,
                      color: GlobalColors.primaryColor,
                      size: 20,
                    )
                  ],
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
