import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/ui/animation/fade_tans_animation.dart';
import 'package:anak/features/home_page/widgets/store_card.dart';
import 'package:flutter/material.dart';

class SpecialStoreWidget extends StatefulWidget {
  @override
  _SpecialStoreWidgetState createState() => _SpecialStoreWidgetState();
}

class _SpecialStoreWidgetState extends State<SpecialStoreWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreensHelper.of(context).fromWidth(100),
      decoration: BoxDecoration(
        color: GlobalColors.greyBackGround,
        borderRadius: BorderRadius.circular(12),
      ),
      padding: EdgeInsets.symmetric(
        horizontal: ScreensHelper.of(context).fromWidth(4),
        vertical: ScreensHelper.of(context).fromHeight(2),
      ),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: const [
              Text(
                "متاجر تعجبك",
                style: TextStyle(
                    color: GlobalColors.primaryColor,
                    fontWeight: FontWeight.w600,
                    fontSize: 16),
              ),
              Text(
                "إظهار الكل",
                style: TextStyle(
                    color: GlobalColors.greyText, fontWeight: FontWeight.w400),
              ),
            ],
          ),
          SizedBox(
            height: ScreensHelper.of(context).fromHeight(1.5),
          ),
          SizedBox(
            height: 100,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              shrinkWrap: true,
              itemCount: 6,
              itemBuilder: (context, index) {
                return FadeTransAnimation(
                  delayInMillisecond: 200,
                  child: StoreCard(),
                ); /////////////////
              },
            ),
          ),
          SizedBox(
            height: ScreensHelper.of(context).fromHeight(3),
          ),
          SizedBox(
            height: 100,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              shrinkWrap: true,
              itemCount: 6,
              itemBuilder: (context, index) {
                return FadeTransAnimation(
                  delayInMillisecond: 200,
                  child: StoreCard(),
                ); /////////////////
              },
            ),
          ),
        ],
      ),
    );
  }
}
