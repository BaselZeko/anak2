import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/resources/images.dart';
import 'package:flutter/material.dart';

class StoreCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Container(
        // color: Colors.blue,
        width: 70,
        // height: ScreensHelper.of(context).fromHeight(12),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              decoration: BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.circular(8)
              ),
              child: Image.asset(
                GlobalImages.store1,
                width: 70,
                height: 70,
                fit: BoxFit.contain,
              ),
            ),
            SizedBox(
              height: 4,
            ),
            Text(
              "قميص",
              style: TextStyle(
                  color: GlobalColors.greyText, fontWeight: FontWeight.w400),
              overflow: TextOverflow.ellipsis,
            ),
          ],
        ),
      ),
    );
  }
}
