import 'package:anak/core/helper/extensions/translation.dart';
import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/resources/images.dart';
import 'package:anak/core/services/navigation_service.dart';
import 'package:anak/features/home_page/data/model/data.dart';
import 'package:anak/features/home_page/data/model/product.dart';
import 'package:anak/features/home_page/presentation/product_info_page.dart';
import 'package:animations/animations.dart';
import 'package:flutter/material.dart';

class Product extends StatefulWidget {
  Product({Key key, this.index}) : super(key: key);
  int index;

  @override
  _ProductState createState() => _ProductState();
}

class _ProductState extends State<Product> {
  @override
  Widget build(BuildContext context) {
    ProductModel productModel = productList[widget.index];

    return Padding(
        padding: const EdgeInsets.all(5),
        child: Card(
          shadowColor: Colors.white,
          child: Container(
            decoration: BoxDecoration(
              color: Colors.red,
              borderRadius: BorderRadius.circular(14),
            ),
            child: Stack(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Image.asset(productModel.imageUrl,fit: BoxFit.fill,height: ScreensHelper.of(context).fromHeight(33),
                    width: ScreensHelper.of(context).width,) ,
                ),

                Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SizedBox(
                      width: ScreensHelper.of(context).fromWidth(4),
                      height: ScreensHelper.of(context).fromWidth(17),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            width: ScreensHelper.of(context).fromWidth(4),
                            height: ScreensHelper.of(context).fromWidth(4),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4),
                              color: GlobalColors.mainColor1,
                            ),
                          ),
                          Container(
                            width: ScreensHelper.of(context).fromWidth(4),
                            height: ScreensHelper.of(context).fromWidth(4),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4),
                                color: GlobalColors.mainColor2,
                                border: Border.all(
                                    color: Colors.black, width: 1)),
                          ),
                          Container(
                            width: ScreensHelper.of(context).fromWidth(4),
                            height: ScreensHelper.of(context).fromWidth(4),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4),
                              color: GlobalColors.mainColor3,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),


                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    height: ScreensHelper.of(context).fromHeight(18),
                    width: ScreensHelper.of(context).width,
                    decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(8),
                          topLeft: Radius.circular(8),
                        )),padding: const EdgeInsets.symmetric(
                      vertical: 8.0, horizontal: 10),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        const Text(
                          "قميص بمقاس كبير من القطن الأصلي",
                          style: TextStyle(
                            fontSize: 15,
                              color: GlobalColors.blackText,
                              fontWeight: FontWeight.w500),
                          overflow: TextOverflow.ellipsis,
                        ),
                        SizedBox(
                          width: ScreensHelper.of(context).width,
                          child: const Text(
                            "نساء, قميص",
                            style: TextStyle(
                                fontSize: 14,
                                color: GlobalColors.greyText,
                                fontWeight: FontWeight.w400),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),



                        Row(
                          children: [
                            Text(
                              "51.6" " " + "sar".tr(context),
                              style: const TextStyle(
                                fontSize: 15,
                                  color: GlobalColors.blackText,
                                  fontWeight: FontWeight.w600),
                              overflow: TextOverflow.ellipsis,
                            ),
                            const SizedBox(width: 8),
                            Image.asset(
                              GlobalImages.shoppingCard,
                              width: 16,
                              height: 16,
                            ),
                          ],
                        ),
                        productModel.thereDescount==true?SizedBox(
                          width: ScreensHelper.of(context).width,
                          child:Text(
                            "51.6" " " + "sar".tr(context),
                            style: const TextStyle(
                                decoration: TextDecoration.lineThrough,
                                fontSize: 13,
                                color: GlobalColors.greyText,
                                fontWeight: FontWeight.w400),
                            overflow: TextOverflow.ellipsis,
                          ),

                        ):SizedBox(),
                        Row(
                          children: [
                            const Text(
                              "مانغو",
                              style: TextStyle(
                                fontSize: 15,
                                  color: GlobalColors.blackText,
                                  fontWeight: FontWeight.w400),
                              overflow: TextOverflow.ellipsis,
                            ),
                            const SizedBox(width: 8),
                            ClipRect(
                                child: Image.asset(
                              GlobalImages.intro1,
                              height: 25,
                              fit: BoxFit.fill,
                            )),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                // Positioned(
                //   left:ScreensHelper.of(context).fromWidth(4),
                //   top:  productModel.thereDescount!=null?ScreensHelper.of(context).fromHeight(19):ScreensHelper.of(context).fromHeight(21),
                //   width: 40,
                //   child: CircleAvatar(
                //     backgroundColor: GlobalColors.primagryDark,
                //     child: SizedBox(
                //       width: 25,
                //       height: 25,
                //       child: Image.asset(GlobalImages.addToCard),
                //     ),
                //   ),
                // ),



              ],
            ),
          ),
        ));
  }
}
