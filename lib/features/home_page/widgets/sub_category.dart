import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/images.dart';
import 'package:flutter/material.dart';

class SubCategoryWidget extends StatefulWidget {
  const SubCategoryWidget({Key key}) : super(key: key);

  @override
  _SubCategoryWidgetState createState() => _SubCategoryWidgetState();
}

class _SubCategoryWidgetState extends State<SubCategoryWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal:8),
      child: Container(
        width: ScreensHelper.of(context).fromWidth(20),
        padding:  const EdgeInsets.only(top:5),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(14),
            border: Border.all(color: Colors.black,width: 1.5)),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            children: [
              ClipRect(
                  child: Image.asset(
                GlobalImages.intro1,
                height: 60,
                width: 60,
                fit: BoxFit.fill,
              )),
              const Text("Name")
            ],
          ),
        ),
      ),
    );
  }
}
