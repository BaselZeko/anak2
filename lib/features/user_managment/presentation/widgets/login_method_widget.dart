import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:anak/core/resources/images.dart';

class LoginMethodWidget extends StatelessWidget {
  const LoginMethodWidget({Key key, this.image}) : super(key: key);
  final String image;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 50,
      height: 50,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(7),
        border: Border.all(color: Colors.grey[300],width: 2),
        color: Colors.grey[200],
      ),
      child: Center(
        child: SvgPicture.asset(
          image,
          width: 30,
          height: 30,
        ),
      ),
    );
  }
}
