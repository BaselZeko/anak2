import 'package:flutter/cupertino.dart';

class MyClipper extends CustomClipper<Path>{
  @override
  Path getClip(Size size) {

    Path path0 = Path();
    path0.moveTo(size.width*-0.0044000,size.height*0.0113250);
    path0.quadraticBezierTo(size.width*-0.0015000,size.height*0.7567250,size.width*-0.0068500,size.height*1.0070625);
    path0.lineTo(size.width*0.9801500,size.height*1.0070625);
    path0.quadraticBezierTo(size.width*0.9962250,size.height*0.2605375,size.width*0.9992250,size.height*0.0126000);
    path0.cubicTo(size.width*0.9809250,size.height*0.0125250,size.width*0.9588500,size.height*0.0124375,size.width*0.9398500,size.height*0.0124375);
    path0.cubicTo(size.width*0.9224250,size.height*0.0125875,size.width*0.9108500,size.height*0.0433500,size.width*0.8601250,size.height*0.0413250);
    path0.cubicTo(size.width*0.8118750,size.height*0.0430875,size.width*0.8020250,size.height*0.0124750,size.width*0.7843500,size.height*0.0124375);
    path0.cubicTo(size.width*0.5816250,size.height*0.0115500,size.width*0.6119750,size.height*0.0118125,size.width*-0.0044000,size.height*0.0113250);
    path0.close();
    return path0;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
   return true;
  }

}