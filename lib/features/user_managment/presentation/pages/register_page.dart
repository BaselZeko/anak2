import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:anak/core/helper/extensions/translation.dart';
import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/resources/images.dart';
import 'package:anak/core/ui/animation/fade_tans_animation.dart';
import 'package:anak/core/ui/widgets/button.dart';
import 'package:anak/core/ui/widgets/custom_phone_number.dart';
import 'package:anak/core/validators/base_validator.dart';
import 'package:anak/core/validators/email_validator.dart';
import 'package:anak/core/validators/password_validators.dart';
import 'package:anak/core/validators/required_validator.dart';
import 'package:anak/features/user_managment/presentation/widgets/login_method_widget.dart';
import 'package:anak/features/user_managment/presentation/widgets/my_clipper.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  TextEditingController name = TextEditingController();
  TextEditingController password = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController confirmPassword = TextEditingController();
  final _passwordKey = GlobalKey<FormFieldState<String>>();
  bool isValidate = false;
  bool visible = false;
  String number;
  String _chosenValue;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: GlobalColors.white,
      body: Container(
        width: ScreensHelper.of(context).fromWidth(100),
        height: ScreensHelper.of(context).fromHeight(100),
        color: GlobalColors.primaryColor,
        child: Stack(
          children: [
            Align(
              alignment: Alignment.center,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30.0),
                child: Container(
                  height: ScreensHelper.of(context).fromHeight(85),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: GlobalColors.white),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 14.0, vertical: 20),
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(
                            height: 40,
                          ),
                          Row(
                            children: const [
                              Text(
                                "Create Account",
                                style: TextStyle(
                                    fontSize: 24,
                                    color: GlobalColors.primaryColor),
                              )
                            ],
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                          FadeTransAnimation(
                            delayInMillisecond: 300,
                            direction: AxisDirection.down,
                            child: Container(
                              margin: const EdgeInsets.only(bottom: 8),
                              child: TextFormField(
                                textDirection: TextDirection.ltr,
                                validator: (value) {
                                  return BaseValidator.validateValue(value,
                                      [RequiredValidator(), EmailValidator()], true);
                                },
                                onChanged: (value) {
                                  if (isValidate) {

                                  }
                                },
                                controller: name,
                                decoration: InputDecoration(
                                  labelText: "Full Name",
                                  fillColor: Colors.white,
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(12.0),
                                    borderSide: const BorderSide(
                                      color: GlobalColors.primaryColor,
                                    ),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(12.0),
                                    borderSide: BorderSide(
                                      color: Colors.grey[400],
                                      width: 1.0,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 15,
                          ),
                          FadeTransAnimation(
                            delayInMillisecond: 300,
                            direction: AxisDirection.down,
                            child: CustomPhoneField(
                              disableLengthCheck: true,
                              decoration: InputDecoration(
                                labelText: 'Phone Number',
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(12.0),
                                  borderSide: const BorderSide(
                                    color: GlobalColors.primaryColor,
                                  ),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(12.0),
                                  borderSide: BorderSide(
                                    color: Colors.grey[400],
                                    width: 1.0,
                                  ),
                                ),
                              ),
                              initialCountryCode: 'SA',
                              onChanged: (phone) {
                                print(phone.completeNumber);
                              },
                            ),
                          ),
                          const SizedBox(
                            height: 15,
                          ),
                          FadeTransAnimation(
                            delayInMillisecond: 300,
                            direction: AxisDirection.down,
                            child: Container(
                              margin: const EdgeInsets.only(bottom: 8),
                              child: TextFormField(
                                textDirection: TextDirection.ltr,
                                validator: (value) {
                                  return BaseValidator.validateValue(value,
                                      [RequiredValidator(), EmailValidator()], true);
                                },
                                onChanged: (value) {
                                  if (isValidate) {

                                  }
                                },
                                controller: name,
                                decoration: InputDecoration(
                                  labelText: "Email (Optional) ",
                                  fillColor: Colors.white,
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(12.0),
                                    borderSide: const BorderSide(
                                      color: GlobalColors.primaryColor,
                                    ),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(12.0),
                                    borderSide: BorderSide(
                                      color: Colors.grey[400],
                                      width: 1.0,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 15,
                          ),
                          FadeTransAnimation(
                            delayInMillisecond: 300,
                            direction: AxisDirection.down,
                            child: Container(
                              margin: const EdgeInsets.only(bottom: 8),
                              child: DropdownButtonFormField<String>(
                                decoration: InputDecoration(
                                  suffixIcon:const Icon(Icons.arrow_drop_down_sharp),
                                  labelText: "Gender",
                                  fillColor: Colors.white,
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(12.0),
                                    borderSide: const BorderSide(
                                      color: GlobalColors.primaryColor,
                                    ),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(12.0),
                                    borderSide: BorderSide(
                                      color: Colors.grey[400],
                                      width: 1.0,
                                    ),
                                  ),
                                ),
                                iconDisabledColor:Colors.white ,
                                focusColor:Colors.white,
                                value: _chosenValue,
                                style: const TextStyle(color: Colors.white),
                                iconEnabledColor:Colors.white,
                                items: <String>[
                                  'Male',
                                  'Female',
                                ].map<DropdownMenuItem<String>>((String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value,style:const TextStyle(color:Colors.black),),
                                  );
                                }).toList(),
                                hint:const Text(
                                  "Gender",

                                ),
                                onChanged: (String value) {
                                  setState(() {
                                    _chosenValue = value;
                                  });
                                },
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 15,
                          ),
                          FadeTransAnimation(
                            delayInMillisecond: 400,
                            direction: AxisDirection.down,
                            child: Container(
                              margin: const EdgeInsets.only(bottom: 16),
                              child: TextFormField(
                                textDirection: TextDirection.ltr,
                                validator: (value) {
                                  return BaseValidator.validateValue(
                                      value,
                                      [
                                        RequiredValidator(),
                                        PasswordValidator(minLength: 7)
                                      ],
                                      true);
                                },
                                onChanged: (value) {
                                  if (isValidate) {
                                    _passwordKey.currentState.validate();
                                  }
                                },
                                controller: password,
                                decoration: InputDecoration(
                                  helperText:"It must be 8 characters long",
                                  suffixIcon: IconButton(
                                    icon: Icon(visible
                                        ? Icons.visibility
                                        : Icons.visibility_off),
                                    onPressed: () {
                                      visible = !visible;
                                      setState(() {});
                                    },
                                  ),
                                  labelText: "Password",
                                  fillColor: Colors.white,
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(12.0),
                                    borderSide: const BorderSide(
                                      color: GlobalColors.primaryColor,
                                    ),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(12.0),
                                    borderSide: BorderSide(
                                      color: Colors.grey[400],
                                      width: 1.0,
                                    ),
                                  ),
                                ),
                                // key: _passwordKey,
                                obscureText: !visible,
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 15,
                          ),
                          FadeTransAnimation(
                            delayInMillisecond: 400,
                            direction: AxisDirection.down,
                            child: Container(
                              margin: const EdgeInsets.only(bottom: 16),
                              child: TextFormField(
                                textDirection: TextDirection.ltr,
                                validator: (value) {
                                  return BaseValidator.validateValue(
                                      value,
                                      [
                                        RequiredValidator(),
                                        PasswordValidator(minLength: 7)
                                      ],
                                      true);
                                },
                                onChanged: (value) {
                                  // if (isValidate) {
                                  //   _passwordKey.currentState.validate();
                                  // }
                                },
                                controller: password,
                                decoration: InputDecoration(
                                  suffixIcon: IconButton(
                                    icon: Icon(visible
                                        ? Icons.visibility
                                        : Icons.visibility_off),
                                    onPressed: () {
                                      visible = !visible;
                                      setState(() {});
                                    },
                                  ),
                                  labelText: "Confirm Password",
                                  fillColor: Colors.white,
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(12.0),
                                    borderSide: const BorderSide(
                                      color: GlobalColors.primaryColor,
                                    ),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(12.0),
                                    borderSide: BorderSide(
                                      color: Colors.grey[400],
                                      width: 1.0,
                                    ),
                                  ),
                                ),
                                // key: _passwordKey,
                                obscureText: !visible,
                              ),
                            ),
                          ),

                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: const [
                              Text(
                                "forgot your password?",
                                style: TextStyle(
                                    fontSize: 18,
                                    color: GlobalColors.primaryColor),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 50,
                          ),
                          Center(
                            child: Button(
                              onPressed: () {},
                              radius: 4,
                              width: ScreensHelper.of(context).fromWidth(65),
                              height: ScreensHelper.of(context).fromHeight(7),
                              color: GlobalColors.white,
                              title: "Create Account",
                              color2: GlobalColors.primaryColor,
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Row(children: const <Widget>[
                            Expanded(child: Divider()),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              "Or",
                              style: TextStyle(color: GlobalColors.grey),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Expanded(child: Divider()),
                          ]),
                          const SizedBox(
                            height: 20,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children:  [
                              FadeTransAnimation(
                                delayInMillisecond: 300,
                                direction: AxisDirection.left,
                                child: const LoginMethodWidget(
                                  image: GlobalImages.apple,
                                ),
                              ),
                              const SizedBox(
                                width:8,
                              ),
                              FadeTransAnimation(
                                delayInMillisecond: 300,
                                direction: AxisDirection.left,
                                child: const LoginMethodWidget(
                                  image: GlobalImages.facebook,
                                ),
                              ),
                              const SizedBox(
                                width:8,
                              ),
                              FadeTransAnimation(
                                delayInMillisecond: 300,
                                direction: AxisDirection.left,
                                child: const LoginMethodWidget(
                                  image: GlobalImages.google,
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          Center(
                            child: RichText(
                              text: TextSpan(
                                  text: "have an account , ",
                                  style: const TextStyle(
                                      color: GlobalColors.black,
                                      fontWeight: FontWeight.w500),
                                  children: [

                                    TextSpan(
                                        text: 'Login ',
                                        style: const TextStyle(
                                          color: GlobalColors.primaryColor,
                                          fontWeight: FontWeight.bold,
                                        ),
                                        recognizer: TapGestureRecognizer()
                                          ..onTap = () {
                                            Navigator.pop(context);
                                          }),

                                  ]),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
