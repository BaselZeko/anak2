import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/resources/images.dart';
import 'package:anak/core/services/navigation_service.dart';
import 'package:anak/core/ui/animation/fade_tans_animation.dart';
import 'package:anak/core/ui/widgets/button.dart';
import 'package:anak/core/ui/widgets/custom_phone_number.dart';
import 'package:anak/core/validators/base_validator.dart';
import 'package:anak/core/validators/password_validators.dart';
import 'package:anak/core/validators/required_validator.dart';
import 'package:anak/features/user_managment/presentation/pages/register_page.dart';
import 'package:anak/features/user_managment/presentation/widgets/login_method_widget.dart';
import 'package:anak/features/user_managment/presentation/widgets/my_clipper.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController password = TextEditingController();
  final _passwordKey = GlobalKey<FormFieldState<String>>();
  bool isValidate = false;
  bool visible = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: GlobalColors.white,
      body: Container(
        width: ScreensHelper.of(context).fromWidth(100),
        height: ScreensHelper.of(context).fromHeight(100),
        color: GlobalColors.primaryColor,
        child: Stack(
          children: [
            Align(
              alignment: Alignment.center,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30.0),
                child: Stack(
                  clipBehavior: Clip.none,
                  children: [
                    Container(
                      height: ScreensHelper.of(context).fromHeight(85),
                      width: ScreensHelper.of(context).fromWidth(85),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(22),
                          color: GlobalColors.white),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 14.0, vertical: 20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const SizedBox(
                              height: 30,
                            ),
                            Row(
                              children: const [
                                Text(
                                  "login",
                                  style: TextStyle(
                                      fontSize: 24,
                                      color: GlobalColors.primaryColor),
                                )
                              ],
                            ),
                            const SizedBox(
                              height: 40,
                            ),
                            FadeTransAnimation(
                              delayInMillisecond: 300,
                              direction: AxisDirection.down,
                              child: CustomPhoneField(
                                disableLengthCheck: true,
                                decoration: InputDecoration(
                                  labelText: 'Phone Number',
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(12.0),
                                    borderSide: const BorderSide(
                                      color: GlobalColors.primaryColor,
                                    ),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(12.0),
                                    borderSide: BorderSide(
                                      color: Colors.grey[400],
                                      width: 1.0,
                                    ),
                                  ),
                                ),
                                initialCountryCode: 'SA',
                                onChanged: (phone) {
                                  print(phone.completeNumber);
                                },
                              ),
                            ),
                            const SizedBox(
                              height: 80,
                            ),
                            FadeTransAnimation(
                              delayInMillisecond: 400,
                              direction: AxisDirection.down,
                              child: Container(
                                margin: const EdgeInsets.only(bottom: 16),
                                child: TextFormField(
                                  textDirection: TextDirection.ltr,
                                  validator: (value) {
                                    return BaseValidator.validateValue(
                                        value,
                                        [
                                          RequiredValidator(),
                                          PasswordValidator(minLength: 7)
                                        ],
                                        true);
                                  },
                                  onChanged: (value) {
                                    if (isValidate) {
                                      _passwordKey.currentState.validate();
                                    }
                                  },
                                  controller: password,
                                  decoration: InputDecoration(
                                    suffixIcon: IconButton(
                                      icon: Icon(visible
                                          ? Icons.visibility
                                          : Icons.visibility_off),
                                      onPressed: () {
                                        visible = !visible;
                                        setState(() {});
                                      },
                                    ),
                                    labelText: "Password",
                                    fillColor: Colors.white,
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.circular(12.0),
                                      borderSide: const BorderSide(
                                        color: GlobalColors.primaryColor,
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.circular(12.0),
                                      borderSide: BorderSide(
                                        color: Colors.grey[400],
                                        width: 1.0,
                                      ),
                                    ),
                                  ),
                                  key: _passwordKey,
                                  obscureText: !visible,
                                ),
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: const [
                                Text(
                                  "forgot your password?",
                                  style: TextStyle(
                                      fontSize: 18,
                                      color: GlobalColors.primaryColor),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 50,
                            ),
                            Center(
                              child: FadeTransAnimation(
                                delayInMillisecond: 300,
                                direction: AxisDirection.down,
                                child: Button(
                                  onPressed: () {},
                                  radius: 4,
                                  width:
                                      ScreensHelper.of(context).fromWidth(65),
                                  height:
                                      ScreensHelper.of(context).fromHeight(7),
                                  color: GlobalColors.white,
                                  title: "Login",
                                  color2: GlobalColors.primaryColor,
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            Center(
                              child: FadeTransAnimation(
                                delayInMillisecond: 300,
                                direction: AxisDirection.down,
                                child: Button(
                                  onPressed: () {},
                                  radius: 4,
                                  width:
                                      ScreensHelper.of(context).fromWidth(65),
                                  height:
                                      ScreensHelper.of(context).fromHeight(7),
                                  color: GlobalColors.white,
                                  title: "Guest",
                                  color2: GlobalColors.primaryColor,
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Row(children: const <Widget>[
                              Expanded(child: Divider()),
                              SizedBox(
                                width: 10,
                              ),
                              Text(
                                "Or",
                                style: TextStyle(color: GlobalColors.grey),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Expanded(child: Divider()),
                            ]),
                            const SizedBox(
                              height: 20,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                FadeTransAnimation(
                                  delayInMillisecond: 300,
                                  direction: AxisDirection.left,
                                  child: const LoginMethodWidget(
                                    image: GlobalImages.apple,
                                  ),
                                ),
                                const SizedBox(
                                  width: 8,
                                ),
                                FadeTransAnimation(
                                  delayInMillisecond: 300,
                                  direction: AxisDirection.left,
                                  child: const LoginMethodWidget(
                                    image: GlobalImages.facebook,
                                  ),
                                ),
                                const SizedBox(
                                  width: 8,
                                ),
                                FadeTransAnimation(
                                  delayInMillisecond: 300,
                                  direction: AxisDirection.left,
                                  child: const LoginMethodWidget(
                                    image: GlobalImages.google,
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            Center(
                              child: RichText(
                                text: TextSpan(
                                    text: "don't have an account , ",
                                    style: const TextStyle(
                                        color: GlobalColors.black,
                                        fontWeight: FontWeight.w500),
                                    children: [
                                      TextSpan(
                                          text: 'Create a new account ',
                                          style: const TextStyle(
                                            color: GlobalColors.primaryColor,
                                            fontWeight: FontWeight.bold,
                                          ),
                                          recognizer: TapGestureRecognizer()
                                            ..onTap = () {
                                              NavigationService().navigateTo(
                                                  context,
                                                  const RegisterPage());
                                            }),
                                    ]),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    PositionedDirectional(
                      end: 25,
                      top: -20,
                      child: Container(
                        width: 45,
                        height: 45,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(100),
                          color: Colors.red,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class MyClapper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path0 = Path();
    path0.moveTo(size.width * 0.4153167, size.height * 0.1428143);
    path0.lineTo(size.width * 0.4169833, size.height * 0.7856714);
    path0.lineTo(size.width * 0.7519833, size.height * 0.7871000);
    path0.lineTo(size.width * 0.7503167, size.height * 0.1428143);
    path0.quadraticBezierTo(size.width * 0.7250667, size.height * 0.1397143,
        size.width * 0.7221500, size.height * 0.1648857);
    path0.cubicTo(
        size.width * 0.7155333,
        size.height * 0.1753143,
        size.width * 0.6970500,
        size.height * 0.1730286,
        size.width * 0.6946833,
        size.height * 0.1627143);
    path0.quadraticBezierTo(size.width * 0.6943333, size.height * 0.1403429,
        size.width * 0.6666667, size.height * 0.1428571);
    path0.quadraticBezierTo(size.width * 0.6038292, size.height * 0.1428464,
        size.width * 0.4153167, size.height * 0.1428143);
    path0.close();
    return path0;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return true;
  }
}
