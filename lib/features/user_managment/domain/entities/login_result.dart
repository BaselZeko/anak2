import 'package:anak/core/entities/base_entity.dart';

class LoginResult extends BaseEntity {
  int id;
  String firstName;
  String middleName;
  String lastName;
  String email;
  bool isDisabled;

  LoginResult(
      {this.id,
      this.firstName,
      this.middleName,
      this.lastName,
      this.email,
      this.isDisabled});

  @override
  List<Object> get props =>
      [id, firstName, middleName, lastName, email, isDisabled];
}
