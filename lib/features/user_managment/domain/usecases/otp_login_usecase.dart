import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:anak/core/error/base_error.dart';
import 'package:anak/core/param/base_param.dart';
import 'package:anak/core/result/result.dart';
import 'package:anak/core/services/device_uid_provider.dart';
import 'package:anak/core/usecases/base_use_case.dart';
import 'package:anak/features/user_managment/data/models/login_result_model.dart';
import 'package:anak/features/user_managment/data/repositories/concrete_user_repository.dart';
import 'package:anak/features/user_managment/data/requests/login_request.dart';
import 'package:anak/features/user_managment/domain/entities/login_result.dart';

class LoginOTPParams extends BaseParams {
  final LoginRequest data;

  LoginOTPParams({
    @required this.data,
    CancelToken cancelToken,
  }) : super(cancelToken: cancelToken);
}

class LoginOTPUsecase extends UseCase<LoginModel, LoginOTPParams> {
  final ConcreteUserRepository repository;
  final DeviceUidProvider deviceUidProvider;

  LoginOTPUsecase({
    this.repository,
    this.deviceUidProvider,
  });

  @override
  Future<Result<BaseError, LoginModel>> call(LoginOTPParams params) async {
    final String deviceUid = await deviceUidProvider.getDeviceUniqueId();

    return repository.loginOTP(
      data: LoginRequest(otp: params.data.otp),
      cancelToken: params.cancelToken,
    );
  }
}
