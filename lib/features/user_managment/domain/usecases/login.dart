/*
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:vendor/core/error/base_error.dart';
import 'package:vendor/core/param/base_param.dart';
import 'package:vendor/core/result/result.dart';
import 'package:vendor/core/services/device_uid_provider.dart';
import 'package:vendor/core/usecases/base_use_case.dart';
import 'package:vendor/features/user_managment/data/repositories/concrete_user_repository.dart';
import 'package:vendor/features/user_managment/data/requests/login_request.dart';
import 'package:vendor/features/user_managment/domain/entities/login_result.dart';


class LoginParams extends BaseParams {
  final LoginRequest data;

  LoginParams({
    @required this.data,
    CancelToken cancelToken,
  }) : super(cancelToken: cancelToken);
}

class Login extends UseCase<LoginResult, LoginParams> {
  final ConcreteUserRepository repository;
  final DeviceUidProvider deviceUidProvider;

  Login({
    this.repository,
    this.deviceUidProvider,
  });

  @override
  Future<Result<BaseError, LoginResult>> call(LoginParams params) async {
    final String deviceUid = await deviceUidProvider.getDeviceUniqueId();

    return repository.login(
      data: LoginRequest(
          email: params.data.email,
          password: params.data.password,
          deviceId: deviceUid),
      cancelToken: params.cancelToken,
    );
  }
}
*/
