import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:anak/core/error/base_error.dart';
import 'package:anak/core/param/base_param.dart';
import 'package:anak/core/result/result.dart';
import 'package:anak/core/services/device_uid_provider.dart';
import 'package:anak/core/usecases/base_use_case.dart';
import 'package:anak/features/user_managment/data/repositories/concrete_user_repository.dart';
import 'package:anak/features/user_managment/data/requests/send_otp.dart';

class SendOtpParams extends BaseParams {
  final SendOtpRequest data;


  SendOtpParams({
    @required this.data,
    CancelToken cancelToken,
  }) : super(cancelToken: cancelToken);
}

class SendOtpUseCase extends UseCase<bool, SendOtpParams> {
  final ConcreteUserRepository repository;
  final DeviceUidProvider deviceUidProvider;

  SendOtpUseCase({
    this.repository,
    this.deviceUidProvider
  });

  @override
  Future<Result<BaseError, bool>> call(SendOtpParams params) async {
    final String deviceUid = await deviceUidProvider.getDeviceUniqueId();

    return repository.sendOtp(
      data: SendOtpRequest(phoneNumber: params.data.phoneNumber,deviceId: deviceUid),
      cancelToken: params.cancelToken,
    );
  }
}
