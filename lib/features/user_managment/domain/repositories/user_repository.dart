import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:anak/core/error/base_error.dart';
import 'package:anak/core/repositories/repository.dart';
import 'package:anak/core/resources/constants.dart';
import 'package:anak/core/result/result.dart';
import 'package:anak/features/user_managment/data/models/login_result_model.dart';
import 'package:anak/features/user_managment/data/requests/login_request.dart';
import 'package:anak/features/user_managment/data/requests/send_otp.dart';
import 'package:anak/features/user_managment/domain/entities/login_result.dart';

abstract class UserRepository extends Repository {
  final _storage = const FlutterSecureStorage();

  Future<Result<BaseError, LoginResult>> login({
    @required LoginRequest data,
    CancelToken cancelToken,
  });

  Future<Result<BaseError, LoginModel>> loginOTP({
    @required LoginRequest data,
    CancelToken cancelToken,
  });

  Future<Result<BaseError, bool>> sendOtp({
    @required SendOtpRequest data,
    CancelToken cancelToken,
  });

  /* Future<Result<BaseError, RegisterResult>> register({
    @required RegisterRequest data,
    CancelToken cancelToken,
  });

  Future<Result<BaseError, LoginResult>> loginWithFacebook({
    CancelToken cancelToken,
  });

  Future<Result<BaseError, ResetPasswordResult>> resetPassword({
    @required ResetPasswordRequest data,
    CancelToken cancelToken,
  });

  Future<Result<BaseError, LoginResult>> editProfile({
    @required EditProfileRequest data,
    CancelToken cancelToken,
  });

  Future<Result<BaseError, EditPasswordResultEntity>> editPassword({
    @required EditPasswordRequest data,
    CancelToken cancelToken,
  });
*/
  static Future<void> deleteToken() async {
    await const FlutterSecureStorage().delete(key: KEY_TOKEN);
    return;
  }

  Future<void> persistToken(String token) async {
    await _storage.write(key: KEY_TOKEN, value: token);
    return;
  }

  static Future<String> get authToken async {
    return await const FlutterSecureStorage().read(key: KEY_TOKEN);
  }

  static Future<bool> get hasToken async {
    final token = await const FlutterSecureStorage().read(key: KEY_TOKEN);
    if (token != null && token.isNotEmpty) return true;
    return false;
  }

/*static Future<void> deleteProfile() async {
    await LocalStorageService.removeValues(key: Profile.ID);
    await LocalStorageService.removeValues(key: Profile.FIRST_NAME);
    await LocalStorageService.removeValues(key: Profile.LAST_NAME);
    await LocalStorageService.removeValues(key: Profile.MOBILE);
    await LocalStorageService.removeValues(key: Profile.EMAIL);
    await LocalStorageService.removeValues(key: Profile.AVATAR);
    await LocalStorageService.removeValues(key: Profile.STATE);
    await LocalStorageService.removeValues(key: Profile.POINTS);
    await LocalStorageService.removeValues(key: Profile.CITY_NAME);
    await LocalStorageService.removeValues(key: Profile.CART_ITEMS_COUNT);
    return;
  }

  static Future<void> persistProfile(Profile profile) async {
    await LocalStorageService.putValue(
        key: Profile.ID, value: profile.id);
    await LocalStorageService.putValue(
        key: Profile.FIRST_NAME, value: profile?.firstName);
    await LocalStorageService.putValue(
        key: Profile.MOBILE, value: profile?.mobile);
    await LocalStorageService.putValue(
        key: Profile.LAST_NAME, value: profile.lastName == null ? '' : profile?.lastName);
    await LocalStorageService.putValue(
        key: Profile.EMAIL, value: profile?.email == null ? '' : profile?.email);
    await LocalStorageService.putValue(
        key: Profile.AVATAR, value: profile?.avatar == null ? '' : profile?.avatar);
    await LocalStorageService.putValue(
        key: Profile.STATE, value: profile?.state == null ? '' : profile?.state);
    await LocalStorageService.putValue(
        key: Profile.POINTS, value: profile?.points == null ? '' : profile?.points);
    await LocalStorageService.putValue(
        key: Profile.CART_ITEMS_COUNT, value: profile.cartItemsCount == null ? '' : profile.cartItemsCount);
    await LocalStorageService.putValue(
        key: Profile.CITY_NAME, value: profile.cityName == null ? '' : profile.cityName);

    return;
  }

  static Future<Profile> get getProfile async {
    final id = await LocalStorageService.getValue(key: Profile.ID, type: VarTypes.INTEGER);

    final firstName =
    await LocalStorageService.getValue(key: Profile.FIRST_NAME, type: VarTypes.STRING);

    final lastName = await LocalStorageService.getValue(key: Profile.LAST_NAME, type: VarTypes.STRING);

    final phone =  await LocalStorageService.getValue(key: Profile.MOBILE, type: VarTypes.STRING);

    final email =  await LocalStorageService.getValue(key: Profile.EMAIL , type: VarTypes.STRING);

    final image =  await LocalStorageService.getValue(key: Profile.AVATAR, type: VarTypes.STRING);

    final state =await LocalStorageService.getValue(key: Profile.STATE, type: VarTypes.STRING);

    final points = await LocalStorageService.getValue(key: Profile.POINTS, type: VarTypes.STRING);

    final cityName = await LocalStorageService.getValue(key: Profile.CITY_NAME, type: VarTypes.STRING);

    final cartItemsCount =
    await LocalStorageService.getValue(key: Profile.CART_ITEMS_COUNT, type: VarTypes.STRING);

    return Profile(
        id: id,
        firstName: firstName,
        lastName: lastName,
        email: email,
        avatar: image,
        mobile: phone,
        points: points,
        state: state,
        cityName: cityName,
        cartItemsCount: cartItemsCount);
  }*/
}
