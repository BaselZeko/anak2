import 'package:anak/core/response/api_response.dart';

class SendOtpResponse extends ApiResponse<bool> {
  SendOtpResponse(
    String msg,
    bool hasError,
    bool result,
  ) : super(msg, hasError, result);

  factory SendOtpResponse.fromJson(json) {
    print('hi there');
    String message = json['message'];
    print('json[status] ${json['status']}');
    int code = json['status'];
    bool isError = true;
    if (code == 200) {
      isError = false;
    }
    return SendOtpResponse(message, isError, true);
  }
}
