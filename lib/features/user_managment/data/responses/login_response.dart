import 'package:anak/core/response/api_response.dart';
import 'package:anak/features/user_managment/data/models/login_result_model.dart';

class LoginResponse extends ApiResponse<LoginModel> {
  LoginResponse(
    String msg,
    bool hasError,
    LoginModel result,
  ) : super(msg, hasError, result);

  factory LoginResponse.fromJson(json) {
    print('hi there');
    String message = json['message'];
    int code = json['status'];
    bool isError = true;
    LoginModel model;

    if (code == 200) {
      /* json['data']['token'] = json['token'];
      model = LoginResultModel.fromJson(json['data']);*/
      model = LoginModel.fromJson(json);
      isError = false;
    } else if (code != 422) {
      message = 'Something went wrong';
    }

    return LoginResponse(message, isError, model);
  }
}
