import 'package:json_annotation/json_annotation.dart';
/*

@JsonSerializable()
class RegisterResultModel extends BaseModel<RegisterResult> {
  String name;
  String email;
  String lastName;
  String phone;
  String avatar;
  String id;
  String token;
  String cartItemsCount;
  String points;

  RegisterResultModel(
      {this.name,
      this.email,
      this.points,
      this.lastName,
      this.phone,
      this.avatar,
      this.id,
      this.token,
      this.cartItemsCount});

*/
/*  RegisterResultModel.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    avatar = json['avatar'];
    id = json['id'];
    token = json['token'];
    cartItemsCount = json['cart_items_count'];
  }*//*


  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['lname'] = this.lastName;
    data['email'] = this.email;
    data['points'] = this.points;
    data['phone'] = this.phone;
    data['avatar'] = this.avatar;
    data['id'] = this.id;
    data['token'] = this.token;
    data['cart_items_count'] = this.cartItemsCount;
    return data;
  }

  factory RegisterResultModel.fromJson(Map<String, dynamic> json) =>
      RegisterResultModel(
        name: json['name'],
        lastName: json['lname'],
        email: json['email'],
        phone: json['phone'],
        avatar: json['avatar'],
        points: json['points'],
        id: json['id'],
        token: json['token'],
        cartItemsCount: json['cart_items_count'],
      );

  @override
  RegisterResult toEntity() => RegisterResult(
      id: int.parse(this.id), itemsInCart: int.parse(cartItemsCount));
}
*/
