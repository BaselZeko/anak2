import 'package:anak/core/models/base_model.dart';
import 'package:anak/features/user_managment/domain/entities/login_result.dart';

class LoginModel extends BaseModel<LoginResult> {
  int id;
  String firstName;
  String middleName;
  String lastName;
  String email;
  String token;
  bool isDisabled;

  LoginModel(
      {this.id,
      this.firstName,
      this.middleName,
      this.lastName,
      this.email,
      this.isDisabled,
      this.token});

  LoginModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    middleName = json['middle_name'];
    lastName = json['last_name'];
    email = json['email'];
    isDisabled = json['is_disabled'];
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['first_name'] = firstName;
    data['middle_name'] = middleName;
    data['last_name'] = lastName;
    data['email'] = email;
    data['is_disabled'] = isDisabled;
    return data;
  }

  @override
  LoginResult toEntity() => LoginResult(
      id: id,
      lastName: lastName,
      email: email,
      firstName: firstName,
      isDisabled: isDisabled,
      middleName: middleName);
}

// class LoginOTPModel {
//
// }