import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:anak/core/error/base_error.dart';
import 'package:anak/core/result/result.dart';
import 'package:anak/core/services/user_local_management.dart';
import 'package:anak/features/user_managment/data/datasources/user_remote_data_source.dart';
import 'package:anak/features/user_managment/data/models/login_result_model.dart';
import 'package:anak/features/user_managment/data/requests/login_request.dart';
import 'package:anak/features/user_managment/data/requests/send_otp.dart';
import 'package:anak/features/user_managment/domain/entities/login_result.dart';
import 'package:anak/features/user_managment/domain/repositories/user_repository.dart';

class ConcreteUserRepository extends UserRepository {
  final UserRemoteDataSource remoteDataSource;
  final UserLocalManagement userLocalManagement;

  ConcreteUserRepository({this.remoteDataSource, this.userLocalManagement});

  @override
  Future<Result<BaseError, LoginResult>> login({
    @required LoginRequest data,
    CancelToken cancelToken,
  }) async {
    final remoteResult = await remoteDataSource.login(
      data: data,
      cancelToken: cancelToken,
    );

    // Persist token and profile if exists.
    if (remoteResult.isRight()) {
      final data = (remoteResult as Right<BaseError, LoginModel>).value;
      if (data.token != null && data.token.isNotEmpty) {
        await persistToken(data.token);
      }

      await userLocalManagement.storeUser(data.toJson());
    }

    return execute<LoginModel, LoginResult>(
      remoteResult: remoteResult,
    );
  }

  @override
  Future<Result<BaseError, LoginModel>> loginOTP(
      {LoginRequest data, CancelToken cancelToken}) async {
    final phone = await userLocalManagement.getUserPhone();
    final result = await remoteDataSource.loginOTP(data: LoginRequest(otp: data.otp, phone: phone));
    if (result.isRight()) {
      final user = (result as Right<BaseError, LoginModel>).value;
      if (user.token != null && user.token.isNotEmpty) {
        await persistToken(user.token);
      }
    }
    return executeWithoutConvert(remoteResult: result);
  }

  @override
  Future<Result<BaseError, bool>> sendOtp(
      {@required SendOtpRequest data, CancelToken cancelToken}) async {
    final remoteResult = await remoteDataSource.sendOtp(
      data: data,
      cancelToken: cancelToken,
    );
    if (remoteResult.isRight()) {
      await userLocalManagement.storePhone(data.phoneNumber.toString());
    }
    if (remoteResult.isLeft()) {
      print("fallllllll");
    }
    // Persist token and profile if exists.

    return executeWithoutConvert(
      remoteResult: remoteResult,
    );
  }
}

// @override
// Future<Result<BaseError, LoginResult>> loginWithFacebook(
//     {CancelToken cancelToken}) {
//   // TODO: implement loginWithFacebook
//   throw UnimplementedError();
// }
/*
  @override
  Future<Result<BaseError, RegisterResult>> register(
      {RegisterRequest data, CancelToken cancelToken}) async {
    final remoteResult = await remoteDataSource.register(
      data: data,
      cancelToken: cancelToken,
    );

    // Persist token & profile.
    if (remoteResult.isRight()) {
      final data =
          (remoteResult as Right<BaseError, RegisterResultModel>).value;
      await persistToken(data.token);
      await UserRepository.persistProfile(Profile(
          id: int.parse(data.id),
          firstName: data.name,
          lastName: data.lastName,
          email: data.email,
          avatar: data.avatar,
          mobile: data.phone,
          points:data.points,
          cartItemsCount: data.cartItemsCount));

      updateCount(data.cartItemsCount);
    }

    return execute<RegisterResultModel, RegisterResult>(
      remoteResult: remoteResult,
    );
  }

  @override
  Future<Result<BaseError, ResetPasswordResult>> resetPassword(
      {ResetPasswordRequest data, CancelToken cancelToken}) async {
    final result = await remoteDataSource.resetPassword(
      data: data,
      cancelToken: cancelToken,
    );
    return execute<ResetPasswordResultModel, ResetPasswordResult>(
        remoteResult: result);
  }

  @override
  Future<Result<BaseError, LoginResult>> editProfile(
      {EditProfileRequest data, CancelToken cancelToken}) async {
    final remoteResult = await remoteDataSource.editProfile(
      data: data,
      cancelToken: cancelToken,
    );

    // Persist token and profile if exists.
    if (remoteResult.isRight()) {
      final data = (remoteResult as Right<BaseError, LoginResultModel>).value;
      */ /*if (data.token != null && data.token.isNotEmpty) {
        await persistToken(data.token);
      }*/ /*
      prof.email = data.email;
      prof.firstName = data.name;
      prof.lastName = data.lastName;
      prof.avatar =  data.avatar;
      prof.mobile = data.mobile;
      await UserRepository.persistProfile(prof);
    }

    return execute<LoginResultModel, LoginResult>(
      remoteResult: remoteResult,
    );
  }

  @override
  Future<Result<BaseError, EditPasswordResultEntity>> editPassword(
      {EditPasswordRequest data, CancelToken cancelToken}) async {
    final remoteResult = await remoteDataSource.editPassword(
      data: data,
      cancelToken: cancelToken,
    );

    return execute(
      remoteResult: remoteResult,
    );
  }*/
