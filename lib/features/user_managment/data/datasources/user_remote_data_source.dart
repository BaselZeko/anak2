import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:anak/core/datasources/remote_data_source.dart';
import 'package:anak/core/error/base_error.dart';
import 'package:anak/features/user_managment/data/models/login_result_model.dart';
import 'package:anak/features/user_managment/data/requests/login_request.dart';
import 'package:anak/features/user_managment/data/requests/send_otp.dart';

abstract class UserRemoteDataSource extends RemoteDataSource {
  Future<Either<BaseError, LoginModel>> login({
    @required LoginRequest data,
    CancelToken cancelToken,
  });

  Future<Either<BaseError, LoginModel>> loginOTP({
    @required LoginRequest data,
    CancelToken cancelToken,
  });

  Future<Either<BaseError, bool>> sendOtp({
    @required SendOtpRequest data,
    CancelToken cancelToken,
  });
/* Future<Either<BaseError, RegisterResultModel>> register({
    @required RegisterRequest data,
    CancelToken cancelToken,
  });

  Future<Either<BaseError, ResetPasswordResultModel>> resetPassword({
    @required ResetPasswordRequest data,
    CancelToken cancelToken,
  });

  Future<Either<BaseError, LoginResultModel>> editProfile(
      {@required EditProfileRequest data, CancelToken cancelToken});

  Future<Either<BaseError, EditPasswordResultModel>> editPassword(
      {@required EditPasswordRequest data, CancelToken cancelToken});

*/
}
