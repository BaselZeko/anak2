import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:anak/core/enums/api/HttpMethod.dart';
import 'package:anak/core/error/base_error.dart';
import 'package:anak/core/resources/apis.dart';
import 'package:anak/features/user_managment/data/datasources/user_remote_data_source.dart';
import 'package:anak/features/user_managment/data/models/login_result_model.dart';
import 'package:anak/features/user_managment/data/requests/login_request.dart';
import 'package:anak/features/user_managment/data/requests/send_otp.dart';
import 'package:anak/features/user_managment/data/responses/login_response.dart';
import 'package:anak/features/user_managment/data/responses/send_otp_response.dart';

class ConcreteUserRemoteDataSource extends UserRemoteDataSource {
  @override
  Future<Either<BaseError, LoginModel>> login({
    @required LoginRequest data,
    CancelToken cancelToken,
  }) {
    return request<LoginModel, LoginResponse>(
      responseStr: 'LoginResponse',
      mapper: (json) => LoginResponse.fromJson(json),
      method: HttpMethod.POST,
      url: loginUrl,
      data: data.toJson(),
      cancelToken: cancelToken,
    );
  }

  @override
  Future<Either<BaseError, LoginModel>> loginOTP({LoginRequest data, CancelToken cancelToken}) {
    return request<LoginModel, LoginResponse>(
      responseStr: 'LoginResponse',
      mapper: (json) => LoginResponse.fromJson(json),
      method: HttpMethod.POST,
      url: loginUrl,
      data: data.toJson(),
      cancelToken: cancelToken,
    );
  }
  @override
  Future<Either<BaseError, bool>> sendOtp(
      {SendOtpRequest data, CancelToken cancelToken}) {
    return request<bool, SendOtpResponse>(
      responseStr: 'SendOtpResponse',
      mapper: (json) => SendOtpResponse.fromJson(json),
      method: HttpMethod.POST,
      url: sendOtpUrl,
      data: data.toJson(),
      cancelToken: cancelToken,
    );
  }
/*  @override
  Future<Either<BaseError, RegisterResultModel>> register(
      {RegisterRequest data, CancelToken cancelToken}) {
    return request<RegisterResultModel, RegisterResponse>(
        responseStr: 'RegisterResponse',
        mapper: (json) => RegisterResponse.fromJson(json),
        method: HttpMethod.POST,
        data: data.toJson(),
        url: REGISTER);
  }

  @override
  Future<Either<BaseError, ResetPasswordResultModel>> resetPassword(
      {ResetPasswordRequest data, CancelToken cancelToken}) {
    return request<ResetPasswordResultModel, ResetPasswordResponse>(
        responseStr: 'ResetPasswordResponse',
        mapper: (json) => ResetPasswordResponse.fromJson(json),
        method: HttpMethod.POST,
        data: data.toJson(),
        url: RESET_PASSWORD);
  }

  @override
  Future<Either<BaseError, LoginResultModel>> editProfile(
      {EditProfileRequest data, CancelToken cancelToken}) {
    return request<LoginResultModel, LoginResponse>(
      responseStr: 'LoginResponse',
      mapper: (json) => LoginResponse.fromJson(json),
      method: HttpMethod.PUT,
      data: data.toJson(),
      withAuthentication: true,
      url: EDIT_PROFILE_URL + '?lang_id=$lang',
    );
  }

  @override
  Future<Either<BaseError, EditPasswordResultModel>> editPassword(
      {EditPasswordRequest data, CancelToken cancelToken}) {
    return request<EditPasswordResultModel, EditPasswordResponse>(
      responseStr: 'EditPasswordResponse',
      mapper: (json) => EditPasswordResponse.fromJson(json),
      method: HttpMethod.PUT,
      data: data.toJson(),
      withAuthentication: true,
      url: UPDATE_PASSWORD_URL,
    );
  }*/
}
