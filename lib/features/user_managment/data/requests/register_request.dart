
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';


class RegisterRequest {
  final String firstName;
  final String lastName;

  final String password;

  final String phone;

  RegisterRequest({@required this.firstName, @required this.lastName,
    @required this.password, @required this.phone});

  Map<String, dynamic> toJson() {

  }
}
