import 'package:flutter/foundation.dart';

class LoginRequest {
  final String phone;
  final String otp;


  LoginRequest({
    this.phone,
    @required this.otp,


  });

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = {};

    data['phone_number'] = phone;
    data['otp'] = "0000";
    data['otp_type'] = 1;

    return data;
  }
}
