import 'package:flutter/foundation.dart';

class SendOtpRequest {
  final String phoneNumber;
  final String otp_type;
  final String deviceId;

  SendOtpRequest({this.phoneNumber, this.otp_type, this.deviceId});

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = {};

    data['phone_number'] = phoneNumber;
    data['device_id'] = deviceId;
    data['otp_type'] = 1;
    return data;
  }
}
