import 'package:anak/core/helper/extensions/translation.dart';
import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/resources/images.dart';
import 'package:anak/features/cart/presentation/pages/cart_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CartWidget extends StatelessWidget {
  const CartWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 4),
      child: Row(
        children: [
          ClipPath(
            clipper: Clipper1(),
            child: Container(
              height: ScreensHelper.of(context).fromHeight(24),
              width: ScreensHelper.of(context).fromWidth(38),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25),
                color: Colors.grey[200],
              ),
              child: Image.asset(
                GlobalImages.intro1,
                fit: BoxFit.fill,
              ),
            ),
          ),
          const SizedBox(
            width: 13,
          ),
          Expanded(
            child: Container(
              height: ScreensHelper.of(context).fromHeight(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.1),
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: const Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 8.0, vertical: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text("Product Name"),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "Category Name",
                          style: TextStyle(
                              fontSize: 12, color: GlobalColors.greyText),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            SvgPicture.asset(GlobalImages.price),
                            const SizedBox(
                              width: 10,
                            ),
                            Text(
                              "60.45" " " + "sar".tr(context),
                              style:
                                  const TextStyle(fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Container(
                                  width: 20,
                                  height: 20,
                                  decoration: const BoxDecoration(
                                      shape: BoxShape.rectangle,
                                      color: Colors.black),
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                const Text("/ S(36) "),
                              ],
                            ),
                            Container(
                              width: ScreensHelper.of(context).fromWidth(20),
                              height: ScreensHelper.of(context).fromHeight(3),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    offset: const Offset(0, 1),
                                    blurRadius: 5,
                                    color: Colors.grey.withOpacity(0.3),
                                  ),
                                ],
                              ),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Expanded(
                                    child: Container(
                                      child: const Icon(
                                        Icons.remove,
                                        color: Colors.white,
                                      ),
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(100),
                                          color: GlobalColors.primaryColor),
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  const Center(child: Text("1")),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  Expanded(
                                    child: Container(
                                      child: const Icon(
                                        Icons.add,
                                        color: Colors.white,
                                      ),
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(100),
                                          color: GlobalColors.primaryColor),
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 14.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            width: 30,
                            height: 30,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(color: Colors.red),
                            ),
                            child: const Center(
                              child: Icon(
                                Icons.edit,
                                size: 14,
                                color: GlobalColors.primaryColor,
                              ),
                            ),
                          ),
                          Container(
                            width: 30,
                            height: 30,
                            child: const Center(
                              child: Icon(
                                Icons.delete,
                                size: 14,
                                color: GlobalColors.primaryColor,
                              ),
                            ),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(color: Colors.red)),
                          ),
                          Container(
                            width: 30,
                            height: 30,
                            child: const Center(
                              child: Icon(
                                Icons.favorite_border_outlined,
                                size: 14,
                                color: GlobalColors.primaryColor,
                              ),
                            ),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(color: Colors.red)),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class Clipper1 extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    // var controlPoint = Offset(size.width / 2, size.height / 2);
    // var ratio = size.height / size.width;
    // var i = size.width / 100;
    // var j = i;

    // var startPoint=Offset(size.width/2-size.width/4, 80);
    // var endtPoint=Offset(size.width/8-size.width/2, 80);

    Path path0 = Path();
    path0.lineTo(0, size.height);
    path0.lineTo(size.width, size.height);
    path0.lineTo(size.width, size.height / 2);
    path0.lineTo(size.width, 0);

    // path0.lineTo(0, size.height);
    // path0.lineTo(size.width, size.height);
    // path0.lineTo(size.width, 50*j);
    // path0.lineTo(75*i,50*j);
    // path0.quadraticBezierTo(50*i, 100*j, 25*i,50*j );
    // path0.lineTo(0,50*j);
    // path0.lineTo(50 , size.height);
    // path0.quadraticBezierTo(100, size.height-200, 150, size.height);
    // path0.lineTo(200, size.height);
    // path0.quadraticBezierTo(250, size.height-200, 300, size.height);
    // path0.lineTo(size.width, size.height);
    // path0.lineTo(0, 0);

    path0.close();
    return path0;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return true;
  }
}
