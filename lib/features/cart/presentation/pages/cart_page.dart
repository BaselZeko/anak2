import 'package:anak/core/helper/extensions/translation.dart';
import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/ui/animation/fade_tans_animation.dart';
import 'package:anak/core/ui/widgets/button.dart';
import 'package:anak/features/cart/presentation/widgets/cart_widget.dart';
import 'package:flutter/material.dart';

class CartPage extends StatefulWidget {
  const CartPage({Key key}) : super(key: key);

  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          height: ScreensHelper.of(context).fromHeight(100),
          width: ScreensHelper.of(context).fromWidth(100),
          color: GlobalColors.primaryColor,
        ),
        Positioned(
            top: ScreensHelper.of(context).fromHeight(8),
            child: SizedBox(
                width: ScreensHelper.of(context).fromWidth(100),
                child: const Center(
                    child: Text(
                  "Cart",
                  style: TextStyle(fontSize: 24, color: Colors.white),
                )))),
        Positioned(
          top: ScreensHelper.of(context).fromHeight(11),
          child: ClipPath(
            clipper: BackgroundClipper(),
            child: Container(
              height: ScreensHelper.of(context).fromHeight(90),
              width: ScreensHelper.of(context).fromWidth(100),
              decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(80),
                    topRight: Radius.circular(80),
                  )),
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: ScreensHelper.of(context).fromWidth(6),
                    vertical: ScreensHelper.of(context).fromHeight(5)),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(
                        height: 15,
                      ),
                      const Text(
                        "your shopping basket",
                        style: TextStyle(fontSize: 18),
                      ),
                      ListView.builder(
                        physics: const NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: 5,
                        itemBuilder: (context, index) {
                          return FadeTransAnimation(
                            delayInMillisecond: 500 + (100 * index),
                            child: const CartWidget(),
                          );
                        },
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: Container(
                          width: ScreensHelper.of(context).fromWidth(100),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                offset: const Offset(0, 1),
                                blurRadius: 5,
                                color: Colors.grey.withOpacity(0.3),
                              ),
                            ],
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(12.0),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    const Text(
                                      "Products price",
                                      style: TextStyle(
                                        fontSize: 17,
                                      ),
                                    ),
                                    Text(
                                      "221.34" " " + "sar".tr(context),
                                      style: const TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18,
                                          color: GlobalColors.primaryColor),
                                    )
                                  ],
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 8.0),
                                child: Button(
                                  title: "Complete",
                                  width:
                                      ScreensHelper.of(context).fromWidth(30),
                                  color: GlobalColors.white,
                                  color2: GlobalColors.primaryColor,
                                  onPressed: () {},
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: ScreensHelper.of(context).fromWidth(20),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class BackgroundClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    // var controlPoint = Offset(size.width / 2, size.height / 2);
    // var ratio = size.height / size.width;
    // var i = size.width / 100;
    // var j = i;

    // var startPoint=Offset(size.width/2-size.width/4, 80);
    // var endtPoint=Offset(size.width/8-size.width/2, 80);

    Path path0 = Path();
    path0.lineTo(0, size.height);
    path0.lineTo(size.width, size.height);
    path0.lineTo(size.width, 0);
    path0.lineTo(size.width / 2 + 35, 0);
    path0.quadraticBezierTo(
        size.width / 2 + 20, 0, size.width / 2 + 15, 15);
    path0.quadraticBezierTo(
        size.width / 2 , size.height / 13, size.width / 2 - 20, 0);

    // path0.lineTo(0, size.height);
    // path0.lineTo(size.width, size.height);
    // path0.lineTo(size.width, 50*j);
    // path0.lineTo(75*i,50*j);
    // path0.quadraticBezierTo(50*i, 100*j, 25*i,50*j );
    // path0.lineTo(0,50*j);
    // path0.lineTo(50 , size.height);
    // path0.quadraticBezierTo(100, size.height-200, 150, size.height);
    // path0.lineTo(200, size.height);
    // path0.quadraticBezierTo(250, size.height-200, 300, size.height);
    // path0.lineTo(size.width, size.height);
    // path0.lineTo(0, 0);

    path0.close();
    return path0;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return true;
  }
}
