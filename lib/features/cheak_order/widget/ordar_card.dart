import 'package:anak/core/helper/extensions/translation.dart';
import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/resources/images.dart';
import 'package:flutter/material.dart';

class OrderCardWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              Image.asset(
                GlobalImages.store1,
                width: 95,
                height: 95,
                fit: BoxFit.cover,
              ),
              Positioned(
                left: 10,
                top: 10,
                child: CircleAvatar(
                  radius: 11,
                  backgroundColor: Colors.white,
                  child: Padding(
                    padding: const EdgeInsets.all(1.5), // Border radius
                    child: ClipOval(child: Image.asset(GlobalImages.prand1)),
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 8,
          ),
          SizedBox(
            width: 95,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "قميص",
                  style: TextStyle(
                      color: GlobalColors.greyText.withOpacity(0.6), fontWeight: FontWeight.w400),
                  overflow: TextOverflow.ellipsis,
                ),

                const Text(
                  "2XL",
                  style: TextStyle(
                      fontSize: 15,
                      color: GlobalColors.primaryColor,
                      fontWeight: FontWeight.w600),
                  overflow: TextOverflow.ellipsis,
                ),
              ],
            ),
          ),

          Text(
            "51.6" " " + "sar".tr(context),
            style: const TextStyle(
                fontSize: 14,
                color: Colors.black87,
                fontWeight: FontWeight.w600),
            overflow: TextOverflow.ellipsis,
          ),
        ],
      ),
    );
  }
}
