import 'package:anak/core/helper/extensions/translation.dart';
import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/resources/images.dart';
import 'package:anak/core/ui/animation/fade_tans_animation.dart';
import 'package:anak/features/cheak_order/presentation/cheak_payment.dart';
import 'package:flutter/material.dart';

class SecondPaymentWidget extends StatefulWidget {
  @override
  _SecondPaymentWidgetState createState() => _SecondPaymentWidgetState();
}

class _SecondPaymentWidgetState extends State<SecondPaymentWidget> {
  bool waltActive = true;
  int cartIndex = 0;
  List<String> cards = [
    GlobalImages.mastardCard,
    GlobalImages.visaCard,
    GlobalImages.mada,
    GlobalImages.mada1,
    GlobalImages.appleCard,
  ];

  Widget paymentCard(String imageUrl, int index) {
    return InkWell(
      onTap: () {
        setState(() {
          cartIndex = index;
        });
      },
      child: Container(
        decoration: BoxDecoration(
            // color: Colors.red,
            border: Border.all(
                width: 1.5,
                color: cartIndex == index
                    ? GlobalColors.primaryColor
                    : GlobalColors.lightGrey),
            borderRadius: const BorderRadius.all(Radius.circular(8))),
        padding: EdgeInsets.symmetric(horizontal: 50),
        child: Transform.scale(
          scale: 1.7,
          child: Image.asset(
            imageUrl,
            width: 35,
            height: 35,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Map<String, String> listInfo = {
      "حذاء كعب عالي": "20.5" " " + "sar".tr(context),
      "جاكيت نسائي اسود قصير": "40.3" " " + "sar".tr(context),
      "حقيبة يد جلد": "10.0" " " + "sar".tr(context),
      "قميص قطن": "13.2" " " + "sar".tr(context)
    };
    List<String> keyList = listInfo.keys.toList();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 15,
        ),
        const Text(
          "طريقة الدقع",
          style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w600,
              color: GlobalColors.blackText),
        ),
        SizedBox(
          height: ScreensHelper.of(context).fromHeight(3),
        ),
        SizedBox(
          height: ScreensHelper.of(context).fromHeight(12),
          child: ListView.separated(
            // physics: const NeverScrollableScrollPhysics(),
            scrollDirection: Axis.horizontal,
            shrinkWrap: true,
            itemCount: 5,
            itemBuilder: (context, index) {
              return FadeTransAnimation(
                delayInMillisecond: 500 + (100 * index),
                child: paymentCard(cards[index], index),
              );
            },
            separatorBuilder: (BuildContext context, int index) {
              return const SizedBox(
                width: 15,
              );
            },
          ),
        ),
        SizedBox(
          height: ScreensHelper.of(context).fromHeight(4),
        ),
        const Text(
          "بيانات الدفع",
          style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w600,
              color: GlobalColors.blackText),
        ),
        SizedBox(
          height: ScreensHelper.of(context).fromHeight(2),
        ),

        //////////
        Container(
          width: ScreensHelper.of(context).fromWidth(90),
          // height: ScreensHelper.of(context).fromHeight(100),
          padding: EdgeInsets.symmetric(
            horizontal: ScreensHelper.of(context).fromWidth(3),
            vertical: ScreensHelper.of(context).fromHeight(2),
          ),
          decoration: const BoxDecoration(
            color: Color(0xffeeeeee),
            borderRadius: BorderRadius.all(Radius.circular(2)),
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              CircleAvatar(
                radius: ScreensHelper.of(context).fromWidth(8),
                backgroundColor: Colors.transparent,
                child: Padding(
                  padding: const EdgeInsets.all(1.5), // Border radius
                  child: Transform.scale(
                    scale: 2,
                    child: Image.asset(
                      cards[cartIndex],
                      width: 25,
                      height: 25,
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  height: ScreensHelper.of(context).fromWidth(15),
                  padding: EdgeInsets.symmetric(
                      horizontal: ScreensHelper.of(context).fromHeight(1.5)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: const [
                      Text(
                        "credit card",
                        style: TextStyle(
                            fontSize: 17,
                            color: GlobalColors.blackText,
                            fontWeight: FontWeight.w600),
                        overflow: TextOverflow.ellipsis,
                      ),
                      Text(
                        "4552 2558 123 25",
                        style: TextStyle(
                            fontSize: 16, color: GlobalColors.greyText),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
              ),
              const Text(
                "change",
                style: TextStyle(fontSize: 16, color: GlobalColors.greyText),
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
        /////////

        SizedBox(
          height: ScreensHelper.of(context).fromHeight(3),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              waltActive == true
                  ? "إلغاء استخدام محفظة التطبيق"
                  : "استخدام محفظة التطبيق",
              style: const TextStyle(
                  color: GlobalColors.blackText,
                  fontSize: 17,
                  fontWeight: FontWeight.w600),
              textAlign: TextAlign.center,
            ),
            Switch.adaptive(
              value: waltActive,
              onChanged: (bool value) {
                setState(() {
                  waltActive = !waltActive;
                });
              },
              activeColor: GlobalColors.primaryColor,
            ),
          ],
        ),

        SizedBox(
          height: ScreensHelper.of(context).fromHeight(3),
        ),

        Row(
          children: [
            Image.asset(GlobalImages.copons),
            const SizedBox(
              width: 10,
            ),
            Text(
              "هل تريد اضافة كوبون حسم؟",
              style: TextStyle(
                  color: GlobalColors.greyText.withOpacity(0.6), fontSize: 16),
            ),
          ],
        ),

        SizedBox(
          height: ScreensHelper.of(context).fromHeight(3),
        ),

        const Text(
          "الفاتورة",
          style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w600,
              color: GlobalColors.blackText),
        ),

        SizedBox(
          height: ScreensHelper.of(context).fromHeight(2),
        ),

        ListView.separated(
          physics: const NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: listInfo.length,
          itemBuilder: (context, index) {
            return FadeTransAnimation(
              delayInMillisecond: 500 + (100 * index),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    keyList.elementAt(index),
                    style: const TextStyle(
                      fontSize: 17,
                      color: GlobalColors.blackText,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  Text(
                    listInfo[keyList.elementAt(index)],
                    style: const TextStyle(
                      fontSize: 17,
                      color: GlobalColors.blackText,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
            );
          },
          separatorBuilder: (BuildContext context, int index) {
            return SizedBox(
              height: ScreensHelper.of(context).fromHeight(2),
            );
          },
        ),
        SizedBox(
          height: ScreensHelper.of(context).fromHeight(2),
        ),
        const SizedBox(
          height: 2,
          child: Divider(
            color: Color(0xffE5E5E5),
            thickness: 1,
          ),
        ),

        SizedBox(
          height: ScreensHelper.of(context).fromHeight(2),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Text(
              "المجموع",
              style: TextStyle(
                fontSize: 17,
                color: GlobalColors.blackText,
                fontWeight: FontWeight.w400,
              ),
            ),
            Text(
              listInfo[keyList.elementAt(2)],
              style: const TextStyle(
                fontSize: 17,
                color: GlobalColors.primaryColor,
                fontWeight: FontWeight.w600,
              ),
            ),
          ],
        ),

        SizedBox(
          height: ScreensHelper.of(context).fromHeight(6),
        ),

        Center(
          child: Container(
            width: ScreensHelper.of(context).fromWidth(55),
            padding: const EdgeInsets.symmetric(vertical: 1.5, horizontal: 1.5),
            decoration: const BoxDecoration(
              color: GlobalColors.primaryColor,
              borderRadius: BorderRadius.all(Radius.circular(6)),
            ),
            child: Row(
              children: [
                Container(
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(6)),
                  ),
                  child: Image.asset(
                    cards[cartIndex],
                    width: 37,
                    height: 37,
                  ),
                ),
                Expanded(
                  child: InkWell(
                      onTap: () {
                        stepIndex.value++;
                      },
                      child: const Text(
                        "ادفع",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                            fontSize: 16),
                      )),
                )
              ],
            ),
          ),
        ),
        SizedBox(
          height: ScreensHelper.of(context).fromWidth(10),
        )
      ],
    );
  }
}
