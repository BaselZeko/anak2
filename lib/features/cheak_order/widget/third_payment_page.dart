import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/resources/images.dart';
import 'package:anak/core/services/navigation_service.dart';
import 'package:anak/core/ui/pages/root_page.dart';
import 'package:anak/core/ui/widgets/button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ThirdPaymentWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        bottom: ScreensHelper.of(context).fromHeight(8)
      ),

      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          // SizedBox(
          //   height: ScreensHelper.of(context).fromHeight(8),
          // ),
          Column(
            children: [
              Image.asset(GlobalImages.paymentSuccessfully),
              SizedBox(
                height: ScreensHelper.of(context).fromHeight(3),
              ),
              const Text(
                "تمت عملية الدفع بنجاح",
                style: TextStyle(
                    fontSize: 18,
                    color: GlobalColors.blackText,
                    fontWeight: FontWeight.w500),
                overflow: TextOverflow.ellipsis,
              ),
              const SizedBox(
                height: 5,
              ),
              const Text(
                "رقم الطلب #359883",
                style: TextStyle(
                    fontSize: 18,
                    color: GlobalColors.blackText,
                    fontWeight: FontWeight.w500),
                overflow: TextOverflow.ellipsis,
              ),
              SizedBox(
                height: ScreensHelper.of(context).fromHeight(3),
              ),
              Padding(
                padding:
                const EdgeInsets.symmetric(horizontal: 12.0,vertical: 5),
                child: Button(
                  title: "متابعة التسوق",
                  width:
                  ScreensHelper.of(context).fromWidth(30),
                  color: GlobalColors.white,
                  color2: GlobalColors.primaryColor,
                  onPressed: () {
                    NavigationService().navigateTo(context, RootPage());
                  },
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
