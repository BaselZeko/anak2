import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/ui/animation/fade_tans_animation.dart';
import 'package:anak/core/ui/widgets/button.dart';
import 'package:anak/core/ui/widgets/selector_method.dart';
import 'package:anak/features/cheak_order/presentation/cheak_payment.dart';
import 'package:anak/features/cheak_order/widget/TextFieldWidget.dart';
import 'package:anak/features/cheak_order/widget/ordar_card.dart';
import 'package:flutter/material.dart';

class FistPaymentWidget extends StatefulWidget {
  @override
  _FistPaymentWidgetState createState() => _FistPaymentWidgetState();
}

enum DelivaryInfo { callMe, leaveOut }
enum DelivaryType { delivary, byHand }

class _FistPaymentWidgetState extends State<FistPaymentWidget> {
  DelivaryInfo _delivaryInfo = DelivaryInfo.callMe;
  DelivaryType _delivaryType = DelivaryType.delivary;

  Widget radioWidget<T>(
      T groupId, T value, Color color, double scale, Function onChange) {
    return Transform.scale(
      scale: scale,
      child: Radio<T>(
        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
        visualDensity: const VisualDensity(
          horizontal: VisualDensity.minimumDensity,
          vertical: VisualDensity.minimumDensity,
        ),
        fillColor: MaterialStateColor.resolveWith((states) => color),
        focusColor: MaterialStateColor.resolveWith((states) => color),
        value: value,
        groupValue: groupId,
        onChanged: (T value) {
          onChange();
        },
      ),
    );
  }

  Widget getOption<T>(IconData icon, double scale, String option, T groupId,
      T value, Color color, Function onChange) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 2.0),
      child: Card(
          color: GlobalColors.primaryColor,
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20),
            child: Row(
              children: [
                Icon(
                  icon,
                  color: Colors.white,
                  size: 27,
                ),
                SizedBox(
                  width: ScreensHelper.of(context).fromWidth(4),
                ),
                Text(
                  option,
                  style:
                      const TextStyle(color: GlobalColors.white, fontSize: 16),
                ),
                const Spacer(),
                radioWidget(groupId, value, color, scale, onChange),
              ],
            ),
          )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 15,
        ),
        const Text(
          "تفاصيل الطلب",
          style: TextStyle(fontSize: 18),
        ),
        const SizedBox(
          height: 10,
        ),
        SizedBox(
          // color: Colors.blueAccent,
          height: 170,
          child: ListView.separated(
            physics: const NeverScrollableScrollPhysics(),
            scrollDirection: Axis.horizontal,
            shrinkWrap: true,
            itemCount: 5,
            itemBuilder: (context, index) {
              return FadeTransAnimation(
                delayInMillisecond: 500 + (100 * index),
                child: OrderCardWidget(),
              );
            },
            separatorBuilder: (BuildContext context, int index) {
              return const SizedBox(
                width: 15,
              );
            },
          ),
        ),
        const SizedBox(
          height: 15,
        ),
        const Text(
          "طريقة التوصيل",
          style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w500,
              color: GlobalColors.blackText),
        ),
        SizedBox(
          height: ScreensHelper.of(context).fromHeight(1),
        ),
        getOption<DelivaryType>(Icons.delivery_dining, 1.2, "Delivary",
            _delivaryType, DelivaryType.delivary, GlobalColors.white, () {
          setState(() {
            _delivaryType = DelivaryType.delivary;
          });
        }),
        getOption<DelivaryType>(Icons.clean_hands_outlined, 1.2, "By Hand",
            _delivaryType, DelivaryType.byHand, GlobalColors.white, () {
          setState(() {
            _delivaryType = DelivaryType.byHand;
          });
        }),
        SizedBox(
          height: ScreensHelper.of(context).fromHeight(3),
        ),
        const Text(
          "العنوان",
          style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w500,
              color: GlobalColors.blackText),
        ),

        Transform.translate(
          offset: Offset(0.0, -5 * MediaQuery.of(context).viewInsets.bottom),
          child: TextFieldWidget(
            icon: Icons.edit,
            initValue: "السعودية جدة الشارع رقم 12",
          ),
        ),
        SizedBox(
          height: ScreensHelper.of(context).fromHeight(4),
        ),
        const Text(
          "تفاصيل التوصيل",
          style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w500,
              color: GlobalColors.blackText),
        ),
        SizedBox(
          height: ScreensHelper.of(context).fromHeight(2),
        ),
        Row(
          children: [
            Row(
              children: [
                const Text("call me",
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 16,
                        color: GlobalColors.greyText)),
                const SizedBox(width: 8),
                radioWidget<DelivaryInfo>(_delivaryInfo, DelivaryInfo.callMe,
                    GlobalColors.primaryColor, 1.0, () {
                  setState(() {
                    _delivaryInfo = DelivaryInfo.callMe;
                  });
                }),
              ].reversed.toList(),
            ),
            SizedBox(
              width: ScreensHelper.of(context).fromWidth(20),
            ),
            Row(
              children: [
                const Text(
                  "leave out",
                  style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 16,
                      color: GlobalColors.greyText),
                ),
                const SizedBox(width: 8),
                radioWidget<DelivaryInfo>(
                    _delivaryInfo,
                    DelivaryInfo.leaveOut,
                    GlobalColors.primaryColor,
                    1.0, () {
                  setState(() {
                    _delivaryInfo = DelivaryInfo.leaveOut;
                  });
                }),
              ].reversed.toList(),
            ),
          ],
        ),
        SizedBox(
          height: ScreensHelper.of(context).fromWidth(10),
        ),
        Center(
          child: Button(
            title: "متابعة",
            width: ScreensHelper.of(context).fromWidth(30),
            color: GlobalColors.white,
            color2: GlobalColors.primaryColor,
            onPressed: () {
              print(stepIndex.value);
              stepIndex.value++;
            },
          ),
        ),
        SizedBox(
          height: ScreensHelper.of(context).fromWidth(10),
        )
      ],
    );
  }
}
