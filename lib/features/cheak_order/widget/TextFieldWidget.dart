// ignore_for_file: file_names

import 'dart:ui';

import 'package:anak/core/resources/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TextFieldWidget extends StatelessWidget {


  // String lableName;
  TextEditingController textEditingController = TextEditingController();

  final String Function(String) validator;
  int maxLines;
  int minLines;
  Function(String) onChanged;
  String initValue = "";
  TextInputType keyboardType;
  bool enabled;
  String hintText;
  IconData icon;


  TextFieldWidget(
      {
        this.initValue = "",
        this.hintText = "",
        this.keyboardType = TextInputType.text,
        this.validator,
        this.enabled = true,
        this.icon,
        @required this.onChanged,
        this.maxLines = 1,
        this.minLines = 1
      }) {
    initValue==null||initValue==""?textEditingController.text="":textEditingController.text = initValue;
    return;
  }

  @override
  Widget build(BuildContext context) {

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: TextFormField(
        style: TextStyle(color: GlobalColors.greyText.withOpacity(0.5),fontSize: 16),
        keyboardType: keyboardType,
        minLines: minLines,
        maxLines: maxLines,
        decoration: InputDecoration(
          hintText:hintText ,
          hintStyle: TextStyle(fontSize: 16,color: GlobalColors.greyText.withOpacity(0.5)) ,
          labelStyle: TextStyle(fontSize: 16,color:GlobalColors.greyText.withOpacity(0.5)),
          focusedBorder: getBorder(color: GlobalColors.greyText.withOpacity(0.5)),
          enabledBorder:getBorder(color: GlobalColors.greyText.withOpacity(0.5)),
          errorBorder: getBorder(),
          focusedErrorBorder: getBorder(),
        ),
        controller: ((textEditingController.text ?? "") == "") ? null :textEditingController,
        validator: validator,
        onChanged: this.onChanged,
      ),
    );
  }
}

OutlineInputBorder getBorder({Color color = GlobalColors.greyText}){
  return  OutlineInputBorder(
    borderSide: BorderSide(
      color: color,
    ),
    borderRadius: BorderRadius.circular(5.0),
  );
}
