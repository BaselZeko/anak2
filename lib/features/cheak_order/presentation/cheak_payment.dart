import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/features/cart/presentation/pages/cart_page.dart';
import 'package:anak/features/cheak_order/widget/first_payment_page.dart';
import 'package:anak/features/cheak_order/widget/second_payment_page.dart';
import 'package:anak/features/cheak_order/widget/third_payment_page.dart';
import 'package:flutter/material.dart';
import 'package:status_change/status_change.dart';

class CheakPayment extends StatefulWidget {
  @override
  _CheakPaymentState createState() => _CheakPaymentState();
}

ValueNotifier<int> stepIndex = ValueNotifier<int>(0);
const todoColor = Color(0xffd1d2d7);

class _CheakPaymentState extends State<CheakPayment> {

  List<String> stepInfo = ["الشحن", "الدفع", "اتمام العمية"];

  Color getColor(int index) {
    if (index == stepIndex.value) {
      return GlobalColors.primaryColor;
    } else if (index < stepIndex.value) {
      return GlobalColors.primaryColor;
    } else {
      return todoColor;
    }
  }


  Widget getBody(int processIndex) {
    if (stepIndex.value == 0) {
      return FistPaymentWidget();
    } else if (stepIndex.value == 1) {
      return SecondPaymentWidget();
    } else {
      return ThirdPaymentWidget();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Container(
            height: ScreensHelper.of(context).fromHeight(100),
            width: ScreensHelper.of(context).fromWidth(100),
            color: GlobalColors.primaryColor,
          ),
          Positioned(
              top: ScreensHelper.of(context).fromHeight(7),
              child: SizedBox(
                  width: ScreensHelper.of(context).fromWidth(100),
                  child: const Center(
                    child: Text(
                      "متابعة الطلب",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                        color: GlobalColors.white,
                      ),
                    ),
                  ))),
          Positioned(
            top: ScreensHelper.of(context).fromHeight(12),
            child: ClipPath(
              clipper: BackgroundClipper(),
              child: Container(
                height: ScreensHelper.of(context).fromHeight(90),
                width: ScreensHelper.of(context).fromWidth(100),
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(50),
                      topRight: Radius.circular(50),
                    )),

                padding: EdgeInsets.only(top: ScreensHelper.of(context).fromHeight(3)),
                child: SingleChildScrollView(
                  child: ValueListenableBuilder(
                    valueListenable: stepIndex,
                    builder: (context, int index, _) {
                      return Column(
                        children: [
                          SizedBox(
                            height: ScreensHelper.of(context).fromHeight(15),
                            child: Align(
                              alignment: Alignment.topCenter,
                              child: StatusChange.tileBuilder(
                                theme: StatusChangeThemeData(
                                  direction: Axis.horizontal,
                                  connectorTheme: const ConnectorThemeData(
                                      space: 1.0, thickness: 1.0),
                                ),
                                builder: StatusChangeTileBuilder.connected(
                                  itemWidth: (_) =>
                                  MediaQuery.of(context).size.width /3,
                                  contentWidgetBuilder: (context, index) {
                                    return const SizedBox();
                                  },
                                  nameWidgetBuilder: (context, index) {
                                    return Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 20, vertical: 8),
                                      child: Text(
                                        stepInfo[index],
                                        style: TextStyle(
                                          fontWeight: FontWeight.w600,
                                          color: getColor(index),
                                        ),
                                      ),
                                    );
                                  },
                                  indicatorWidgetBuilder: (_, index) {

                                    if(index == stepIndex.value){
                                      return Container(
                                        width: 30.0,
                                        height: 30,
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                                color: GlobalColors.primaryColor,
                                                width: 2),
                                            borderRadius: const BorderRadius.all(Radius.circular(50))
                                        ),
                                      );
                                    }

                                    if (index < stepIndex.value) {
                                      return DotIndicator(
                                        size: 30.0,
                                        border: Border.all(
                                            color: GlobalColors.primaryColor,
                                            width: 4),
                                        child: Padding(
                                          padding: const EdgeInsets.all(4.0),
                                          child: Container(
                                            decoration: const BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: GlobalColors.primaryColor,
                                            ),
                                          ),
                                        ),
                                      );
                                    } else {
                                      return const OutlinedDotIndicator(
                                        size: 30,
                                        borderWidth: 1.0,
                                        color: todoColor,
                                        backgroundColor: todoColor,
                                      );
                                    }
                                  },
                                  lineWidgetBuilder: (index) {

                                    if (index > 0) {
                                      if (index == stepIndex.value) {
                                        final prevColor = getColor(index - 1);
                                        final color = getColor(index);
                                        var gradientColors;
                                        gradientColors = [
                                          prevColor,
                                          Color.lerp(prevColor, color, 0.5)
                                        ];
                                        return DecoratedLineConnector(
                                          decoration: BoxDecoration(
                                            gradient: LinearGradient(
                                              colors: gradientColors,
                                            ),
                                          ),
                                        );
                                      } else {
                                        return SolidLineConnector(
                                          color: getColor(index),
                                        );
                                      }
                                    } else {
                                      return null;
                                    }
                                  },
                                  itemCount: 3,
                                ),
                              ),
                            ),
                          ),

                          Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal:
                              ScreensHelper.of(context).fromWidth(5),
                            ),
                            child: getBody(stepIndex.value),
                          ),

                        ],
                      );
                    },
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}