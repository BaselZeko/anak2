// ignore_for_file: file_names

import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:flutter/material.dart';

class OptionItemWidget extends StatelessWidget {
  final Function() onTap;
  final String lable;
  final String imageURL;

  OptionItemWidget({this.onTap,this.lable,this.imageURL});

  @override
  Widget build(BuildContext context) {

    return InkWell(
      onTap: onTap,
        child: Padding(
          padding: const EdgeInsets.only(top: 15,bottom: 10),
          child: Row(
            children: [
              Text(lable,style: TextStyle(color: GlobalColors.blackText,fontWeight: FontWeight.w500,fontSize: 16)),
              SizedBox(width: ScreensHelper.of(context).fromWidth(4),),
              Image.asset(imageURL),
            ].reversed.toList(),
           ),
        ),
    );
  }
}
