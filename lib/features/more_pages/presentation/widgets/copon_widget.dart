
import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/resources/images.dart';
import 'package:flutter/material.dart';

class CoponWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreensHelper.of(context).fromHeight(14),
      width: ScreensHelper.of(context).fromWidth(100),
      decoration: BoxDecoration(
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.grey.withOpacity(0.2),
                blurRadius: 1.0,
                spreadRadius: 1,
                offset: Offset(
                    -2, 1) // changes position of shadow
            ),
          ],
          color: Colors.white
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [


          SizedBox(
            width: ScreensHelper.of(context).fromWidth(20),
            child: Stack(
              fit: StackFit.expand,
              children: [
                Image.asset(
                  GlobalImages.coponsComponant,
                  fit: BoxFit.cover,
                ),

                const Align(
                  alignment: Alignment.center,
                  child:Text(
                      "50%",
                      style: TextStyle(
                      fontSize: 27,
                      color: GlobalColors.white,
                      fontWeight: FontWeight.w400),
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.center,
                ) ,
                )
              ],
            ),
          ),


          Expanded(
            child: Container(
              height: ScreensHelper.of(context).fromHeight(14),
              padding: EdgeInsets.symmetric(
                  horizontal: ScreensHelper.of(context).fromHeight(1.5)
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children:  const[
                   Text(
                    "copon1",
                    style: TextStyle(
                        fontSize: 20,
                        color: GlobalColors.primaryColor,
                        fontWeight: FontWeight.w600),
                    overflow: TextOverflow.ellipsis,
                  ),
                   Text(
                    'صالحة من 2/2/2022 الى 1/3/2022',
                    style: TextStyle(fontSize: 16,color: GlobalColors.greyText),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
