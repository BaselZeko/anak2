
import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/resources/images.dart';
import 'package:anak/core/ui/animation/fade_tans_animation.dart';
import 'package:flutter/material.dart';

class WaltInfoWidget extends StatelessWidget {
  EdgeInsets padding ;
  Widget titleWidget;
  Map<String,String> listInfo;

  WaltInfoWidget({this.padding,
  this.titleWidget,this.listInfo});

  @override
  Widget build(BuildContext context) {
    List<String> keyList = listInfo.keys.toList();
    return Stack(
      children: [

        Container(
          decoration: BoxDecoration(
            color: Color(0xffF8F8F8),
            borderRadius: const BorderRadius.all(Radius.circular(12)),
            boxShadow: [
              BoxShadow(
                  color: Colors.grey.withOpacity(0.2),
                  blurRadius: 1.0,
                  spreadRadius: 1,
                  offset: const Offset(-2, 1) // changes position of shadow
              ),
            ],
          ),
          // width: ScreensHelper.of(context).fromWidth(90),
          padding: padding,
          child: ListView.separated(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: listInfo.length,
            itemBuilder: (context, index) {

              return FadeTransAnimation(
                delayInMillisecond: 500 + (100 * index),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(keyList.elementAt(index),
                      style: const TextStyle(
                        fontSize: 17,
                        color: GlobalColors.blackText,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    Text(listInfo[keyList.elementAt(index)],
                      style: const TextStyle(
                        fontSize: 17,
                        color: GlobalColors.blackText,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
              );
            }, separatorBuilder: (BuildContext context, int index) {
            return SizedBox(
              height: ScreensHelper.of(context).fromHeight(2),
            );
          },
          ),
        ),

        Positioned.fill(
          child: Align(
            alignment: Alignment.topCenter,
            child: Transform.scale(
              scale: 1.01,
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: const BorderRadius.all(Radius.circular(15)),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey.withOpacity(0.2),
                        blurRadius: 3.0,
                        spreadRadius: 2,
                        offset: const Offset(-3, 1) // changes position of shadow
                    ),
                  ],
                ),
                width: ScreensHelper.of(context).fromWidth(100),
                padding: EdgeInsets.symmetric(vertical:  ScreensHelper.of(context).fromWidth(3),
                horizontal:  ScreensHelper.of(context).fromWidth(3)
                ),
                child: titleWidget),
            ),
          ),
        ),
      ],
    );
  }
}
