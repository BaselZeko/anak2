import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/resources/images.dart';
import 'package:flutter/material.dart';

class PaymentCardWidget extends StatelessWidget {
  String imageUrl;
  String cardName;
  String number;
  PaymentCardWidget({this.imageUrl, this.cardName, this.number});
  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [

        CircleAvatar(
          radius: ScreensHelper.of(context).fromWidth(8),
          backgroundColor: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(1.5), // Border radius
            child: Image.asset(imageUrl),
          ),
        ),


        Expanded(
          child: Container(
            height: ScreensHelper.of(context).fromWidth(15),
            padding: EdgeInsets.symmetric(
                horizontal: ScreensHelper.of(context).fromHeight(1.5)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  cardName,
                  style: const TextStyle(
                      fontSize: 17,
                      color: GlobalColors.blackText,
                      fontWeight: FontWeight.w600),
                  overflow: TextOverflow.ellipsis,
                ),
                Text(
                 number,
                  style: const TextStyle(fontSize: 16, color: GlobalColors.greyText),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
