import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:flutter/material.dart';

class ProfileInfoItemWidget extends StatelessWidget {
  final Function() onTap;
  String info;
  String title;

  ProfileInfoItemWidget({this.onTap, this.info, this.title});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
          vertical: ScreensHelper.of(context).fromHeight(1.5)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: const TextStyle(
                color: GlobalColors.blackText,
                fontSize: 19,
                letterSpacing: 1.5,
                fontWeight: FontWeight.w900),
          ),
          SizedBox(height:ScreensHelper.of(context).fromHeight(2),),
          Container(
            width: ScreensHelper.of(context).fromWidth(90),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10.0),
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey.withOpacity(0.3),
                      blurRadius: 10.0,
                      offset: const Offset(
                          -2, 1),
                  )

                ]),
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 17, horizontal: 20),
              child: Row(
                children: [
                  Text(info,
                      style: const TextStyle(
                          color: GlobalColors.greyText,
                          fontWeight: FontWeight.w400,
                          fontSize: 16)),
                  const Spacer(),
                  InkWell(onTap: onTap,child: const Icon(Icons.edit,color: GlobalColors.primaryColor,size: 27,))
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
