import 'package:anak/core/helper/extensions/translation.dart';
import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/resources/images.dart';
import 'package:anak/core/services/navigation_service.dart';
import 'package:anak/core/ui/animation/fade_tans_animation.dart';
import 'package:anak/features/cart/presentation/pages/cart_page.dart';
import 'package:anak/features/cart/presentation/widgets/cart_widget.dart';
import 'package:anak/features/more_pages/presentation/pages/about_app.dart';
import 'package:anak/features/more_pages/presentation/pages/common_quastion.dart';
import 'package:anak/features/more_pages/presentation/pages/favorite_product.dart';
import 'package:anak/features/more_pages/presentation/pages/setting_page.dart';
import 'package:anak/features/more_pages/presentation/pages/user_profile.dart';
import 'package:anak/features/more_pages/presentation/pages/walt_page.dart';
import '../widgets/OptionItemWidget.dart';
import 'package:anak/features/more_pages/presentation/pages/copons_page.dart';
import 'package:anak/features/more_pages/presentation/pages/payment_method.dart';
import 'package:flutter/material.dart';

class MoreOptionsPage extends StatelessWidget {
  //
  Widget dividerWidget(){
    return const SizedBox(
      height: 2,
      child: Divider(
        color: Color(0xffE5E5E5),
        thickness: 1,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            height: ScreensHelper.of(context).fromHeight(100),
            width: ScreensHelper.of(context).fromWidth(100),
            color: GlobalColors.primaryColor,
          ),
          Positioned(
              top: ScreensHelper.of(context).fromHeight(5),
              child: SizedBox(
                  width: ScreensHelper.of(context).fromWidth(100),
                  child: Center(
                      child: Image.asset(GlobalImages.logo,width: 50,height: 50,)))),
          Positioned(
            top: ScreensHelper.of(context).fromHeight(12),
            child: ClipPath(
              clipper: BackgroundClipper(),
              child: Container(
                height: ScreensHelper.of(context).fromHeight(90),
                width: ScreensHelper.of(context).fromWidth(100),
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(60),
                      topRight: Radius.circular(60),
                    )),
                padding: EdgeInsets.symmetric(
                    horizontal: ScreensHelper.of(context).fromWidth(6),
                    vertical: ScreensHelper.of(context).fromHeight(5)),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(
                        height: 15,
                      ),

                      OptionItemWidget(
                        onTap: () async {
                          NavigationService().navigateTo(context, UserProfilePage());
                          },
                        lable: "حسابي",
                        imageURL: GlobalImages.person,
                      ),

                      dividerWidget(),


                      OptionItemWidget(
                        onTap: () async {
                          NavigationService().navigateTo(context, PaymentMethodPage());
                        },
                        lable: "طرق الدفع",
                        imageURL: GlobalImages.paymenet,
                      ),
                      dividerWidget(),

                      OptionItemWidget(
                        onTap: () async {
                          },
                        lable: "الطلبات",
                        imageURL: GlobalImages.orders,
                      ),

                      dividerWidget(),

                      OptionItemWidget(
                        onTap: () async {
                          NavigationService().navigateTo(context, CoponsPage());
                        },
                        lable: "الكوبونات",
                        imageURL: GlobalImages.copons,
                      ),
                      dividerWidget(),


                      OptionItemWidget(
                        onTap: () async {
                          NavigationService().navigateTo(context, WaltPage());
                        },
                        lable: "المحفظة",
                        imageURL: GlobalImages.walt,
                      ),
                      dividerWidget(),


                      OptionItemWidget(
                        onTap: () async {
                          NavigationService().navigateTo(context, FavoriteProductPage());
                        },
                        lable: "قائمة للمفضلة",
                        imageURL: GlobalImages.fav,
                      ),
                      dividerWidget(),


                      OptionItemWidget(
                        onTap: () async {
                          NavigationService().navigateTo(context, SettingPage());
                        },
                        lable: "الاعدادات",
                        imageURL: GlobalImages.setting,
                      ),
                      dividerWidget(),


                      OptionItemWidget(
                        onTap: () async {
                          NavigationService().navigateTo(context, AboutAppPage());
                        },
                        lable: "عن التطبيق",
                        imageURL: GlobalImages.about,
                      ),
                      dividerWidget(),


                      OptionItemWidget(
                        onTap: () async {
                          },
                        lable: "مشاركة التطبيق",
                        imageURL: GlobalImages.share,
                      ),
                      dividerWidget(),

                      OptionItemWidget(
                        onTap: () async {
                          },
                        lable: "تقديم مقترح",
                        imageURL: GlobalImages.sugg,
                      ),
                      dividerWidget(),

                      OptionItemWidget(
                        onTap: () async {
                          },
                        lable: "تسجيل خروج",
                        imageURL: GlobalImages.logoutIcon,
                      ),

                      SizedBox(
                        height: ScreensHelper.of(context).fromWidth(20),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
