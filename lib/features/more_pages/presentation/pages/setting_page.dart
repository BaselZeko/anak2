import 'package:anak/core/services/navigation_service.dart';
import 'package:anak/features/more_pages/presentation/pages/common_quastion.dart';
import 'package:flutter/material.dart';
import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/resources/images.dart';
import 'package:anak/core/ui/animation/fade_tans_animation.dart';
import 'package:anak/features/cart/presentation/pages/cart_page.dart';
import 'package:anak/features/more_pages/presentation/widgets/payment_card_widget.dart';

class SettingPage extends StatefulWidget {
  @override
  _SettingPageState createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  int currentIndex = 0;
  bool notifiActive = true;

  Widget getOption(int index, String option) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            option,
            style: const TextStyle(
                color: GlobalColors.blackText,
                fontSize: 17,
                fontWeight: FontWeight.w400),
            textAlign: TextAlign.center,
          ),
          index == currentIndex
              ? Container(
                  decoration: const BoxDecoration(
                      color: GlobalColors.primaryColor,
                      borderRadius: BorderRadius.all(Radius.circular(4))),
                  width: 18,
                  height: 18,
                  child: const Icon(
                    Icons.check,
                    color: Colors.white,
                    size: 16,
                  ))
              : Container(
                  decoration: const BoxDecoration(
                      color: GlobalColors.lightGrey,
                      borderRadius: BorderRadius.all(Radius.circular(4))),
                  width: 18,
                  height: 18,
                )
        ],
      ),
    );
  }

  Widget dividerWidget(context) {
    return Center(
      child: SizedBox(
        // height: 2,
        width: ScreensHelper.of(context).fromWidth(84),
        child: const Divider(
          color: Color(0xffE5E5E5),
          thickness: 1,
        ),
      ),
    );
  }

  Widget optionWidget({Function() onTap, String lable}) {
    return InkWell(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.only(top: 15, bottom: 10),
        child: Text(lable,
            style: const TextStyle(
                color: GlobalColors.blackText,
                fontWeight: FontWeight.w500,
                fontSize: 16)),
      ),
    );
  }

  Widget accountSetting({Function() onTap, String lable,String title}) {
    return Padding(
      padding: const EdgeInsets.only(top: 15, bottom: 10),
      child: Column(
        children: [

          Text(
            title,
            style:const TextStyle(
                color: GlobalColors.primaryColor,
                fontSize: 19,
                letterSpacing: 1.5,
                fontWeight: FontWeight.w600),
          ),
          const SizedBox(
            height: 8,
          ),

          InkWell(
            onTap: onTap,
            child: Text(lable,
                style: const TextStyle(
                    color: GlobalColors.blackText,
                    fontWeight: FontWeight.w500,
                    fontSize: 16)),
          ),
        ],
      ),
    );
  }

  Widget notificationWidget({String title}) {
    return Padding(
      padding: const EdgeInsets.only(top: 15, bottom: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style:const TextStyle(
                color: GlobalColors.primaryColor,
                fontSize: 19,
                letterSpacing: 1.5,
                fontWeight: FontWeight.w600),
          ),
          const SizedBox(
            height: 5,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                notifiActive == true
                    ? "إلغاء تفعيل الاشعارات"
                    : "تفعيل الاشعارات",
                style: const TextStyle(
                    color: GlobalColors.blackText,
                    fontSize: 17,
                    fontWeight: FontWeight.w400),
                textAlign: TextAlign.center,
              ),
              Switch.adaptive(
                value: notifiActive,
                onChanged: (bool value) {
                  setState(() {
                    notifiActive = !notifiActive;
                  });
                },
                activeColor: GlobalColors.primaryColor,
              ),
            ],
          ),

        ],
      ),
    );
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            height: ScreensHelper.of(context).fromHeight(100),
            width: ScreensHelper.of(context).fromWidth(100),
            color: GlobalColors.primaryColor,
          ),
          Positioned(
              top: ScreensHelper.of(context).fromHeight(6),
              child: SizedBox(
                  width: ScreensHelper.of(context).fromWidth(100),
                  child: const Center(
                      child: Text(
                    "الإعدادات",
                    style: TextStyle(fontSize: 22, color: Colors.white),
                  )))),
          Positioned(
              left: ScreensHelper.of(context).fromWidth(43),
              top: ScreensHelper.of(context).fromHeight(6),
              child: SizedBox(
                  width: ScreensHelper.of(context).fromWidth(100),
                  child: Center(
                      child: InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: const Icon(
                            Icons.arrow_back,
                            size: 24,
                            color: Colors.white,
                          ))))),
          Positioned(
            top: ScreensHelper.of(context).fromHeight(12),
            child: ClipPath(
              clipper: BackgroundClipper(),
              child: Container(
                height: ScreensHelper.of(context).fromHeight(90),
                width: ScreensHelper.of(context).fromWidth(100),
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(60),
                    topRight: Radius.circular(60),
                  ),
                ),
                padding: EdgeInsets.symmetric(
                  horizontal: ScreensHelper.of(context).fromWidth(5),
                  vertical: ScreensHelper.of(context).fromHeight(8),
                ),
                child: Center(
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: ScreensHelper.of(context).fromHeight(2),
                        ),
                        const Text(
                          "لغة التطبيق",
                          style: TextStyle(
                              color: GlobalColors.primaryColor,
                              fontSize: 19,
                              letterSpacing: 1.5,
                              fontWeight: FontWeight.w600),
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            InkWell(
                                onTap: () {
                                  setState(() {
                                    currentIndex = 0;
                                  });
                                },
                                child: getOption(0, "العربية")),
                            InkWell(
                                onTap: () {
                                  setState(() {
                                    currentIndex = 1;
                                  });
                                },
                                child: getOption(1, "الانكليزية")),
                          ],
                        ),
                        dividerWidget(context),

                        accountSetting(
                          onTap: (){},
                          title: "إعدادات الحساب",
                          lable: "تغيير كلمة المرور"
                        ),

                        dividerWidget(context),
                        notificationWidget(

                          title: "إعدادات الإشعارات",
                        ),

                        dividerWidget(context),
                        optionWidget(
                            onTap: (){},
                          lable: "الشروط والأحكام"
                        ),

                        dividerWidget(context),
                        optionWidget(
                            onTap: (){},
                          lable: "سياسة الشحن والاسترجاع"
                        ),

                        dividerWidget(context),
                        optionWidget(
                            onTap: (){},
                          lable: "سياسة الخصوصية"
                        ),

                        dividerWidget(context),
                        optionWidget(
                            onTap: (){},
                          lable: "معلومات الدعم الفني"
                        ),

                        dividerWidget(context),
                        optionWidget(
                            onTap: (){
                              NavigationService().navigateTo(context, CommonQuastionPage());
                            },
                          lable: "الأسئلة الشائعة"
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
