import 'package:anak/core/services/navigation_service.dart';
import 'package:anak/core/ui/animation/fade_tans_animation.dart';
import 'package:anak/features/more_pages/presentation/widgets/copon_widget.dart';
import 'package:anak/features/more_pages/presentation/widgets/profile_info_item_widget.dart';
import 'package:flutter/material.dart';
import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/resources/images.dart';
import 'package:anak/features/cart/presentation/pages/cart_page.dart';

class UserProfilePage extends StatefulWidget {
  @override
  _UserProfilePageState createState() => _UserProfilePageState();
}


///address
class _UserProfilePageState extends State<UserProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            height: ScreensHelper.of(context).fromHeight(100),
            width: ScreensHelper.of(context).fromWidth(100),
            color: GlobalColors.primaryColor,
          ),
          Positioned(
              top: ScreensHelper.of(context).fromHeight(6),
              child: SizedBox(
                  width: ScreensHelper.of(context).fromWidth(100),
                  child: const Center(
                      child: Text(
                        "حسابي",
                        style: TextStyle(fontSize: 22, color: Colors.white),
                      )))),

          Positioned(
              left: ScreensHelper.of(context).fromWidth(43),
              top: ScreensHelper.of(context).fromHeight(6),
              child: SizedBox(
                  width: ScreensHelper.of(context).fromWidth(100),
                  child: Center(
                      child: InkWell(onTap: (){
                        Navigator.pop(context);
                      }, child: const Icon(Icons.arrow_back,size: 24,color: Colors.white,))))),

          Positioned(
            top: ScreensHelper.of(context).fromHeight(12),
            child: ClipPath(
              clipper: BackgroundClipper(),
              child: Container(
                height: ScreensHelper.of(context).fromHeight(90),
                width: ScreensHelper.of(context).fromWidth(100),
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(60),
                      topRight: Radius.circular(60),
                    )),

                padding: EdgeInsets.symmetric(
                    horizontal: ScreensHelper.of(context).fromWidth(4),
                    vertical: ScreensHelper.of(context).fromHeight(7)
                ),

                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [


                      Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.red,
                              borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  blurRadius: 2.0,
                                  spreadRadius: 1,
                                  offset: const Offset(-2, 1) // changes position of shadow
                              ),
                            ],
                          ),

                          height: ScreensHelper.of(context).fromHeight(32),
                          width: ScreensHelper.of(context).fromWidth(90),
                          child: Image.asset(
                            GlobalImages.intro1,
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),

                      const SizedBox(
                        height: 15,
                      ),

                      ProfileInfoItemWidget(
                        title: "الاسم الكامل",
                        info: "باسل سليمان",
                      ),

                      ProfileInfoItemWidget(
                        title: "رقم الجوال",
                        info: "0987654321",
                      ),

                      ProfileInfoItemWidget(
                        title: "البريد الالكتروني",
                        info: "basel@gmail.com",
                      ),

                      ProfileInfoItemWidget(
                        title: "تاريخ الميلاد",
                        info: "1/1/1999",
                      ),

                      ProfileInfoItemWidget(
                        title: "الجنس",
                        info: "ذكر",
                      ),

                      ProfileInfoItemWidget(
                        title: "العنوان",
                        info: "دمشق",
                      ),



                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
