import 'package:anak/core/helper/extensions/translation.dart';
import 'package:anak/core/services/navigation_service.dart';
import 'package:anak/core/ui/animation/fade_tans_animation.dart';
import 'package:anak/features/more_pages/presentation/widgets/copon_widget.dart';
import 'package:anak/features/more_pages/presentation/widgets/walt_info_widget.dart';
import 'package:flutter/material.dart';
import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/resources/images.dart';
import 'package:anak/features/cart/presentation/pages/cart_page.dart';
import '../widgets/OptionItemWidget.dart';

class WaltPage extends StatefulWidget {
  @override
  _WaltPageState createState() => _WaltPageState();
}

class _WaltPageState extends State<WaltPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            height: ScreensHelper.of(context).fromHeight(100),
            width: ScreensHelper.of(context).fromWidth(100),
            color: GlobalColors.primaryColor,
          ),
          Positioned(
              top: ScreensHelper.of(context).fromHeight(6),
              child: SizedBox(
                  width: ScreensHelper.of(context).fromWidth(100),
                  child: const Center(
                      child: Text(
                    "المحفظة",
                    style: TextStyle(fontSize: 22, color: Colors.white),
                  )))),
          Positioned(
              left: ScreensHelper.of(context).fromWidth(43),
              top: ScreensHelper.of(context).fromHeight(6),
              child: SizedBox(
                  width: ScreensHelper.of(context).fromWidth(100),
                  child: Center(
                      child: InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: const Icon(
                            Icons.arrow_back,
                            size: 24,
                            color: Colors.white,
                          ))))),
          Positioned(
            top: ScreensHelper.of(context).fromHeight(12),
            child: ClipPath(
              clipper: BackgroundClipper(),
              child: Container(
                height: ScreensHelper.of(context).fromHeight(90),
                width: ScreensHelper.of(context).fromWidth(100),
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(60),
                    topRight: Radius.circular(60),
                  ),
                ),
                padding: EdgeInsets.symmetric(
                  horizontal: ScreensHelper.of(context).fromWidth(4),
                  vertical: ScreensHelper.of(context).fromHeight(8),
                ),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: ScreensHelper.of(context).fromHeight(2),
                      ),
                      WaltInfoWidget(
                        listInfo: const {
                          "عدد الأغراض المستردة": "4",
                          "تاريخ اخر استرداد": "3/2/2022"
                        },
                        padding: EdgeInsets.only(
                          top: ScreensHelper.of(context).fromHeight(5),
                          bottom: ScreensHelper.of(context).fromHeight(2),
                          left: ScreensHelper.of(context).fromWidth(10),
                          right: ScreensHelper.of(context).fromWidth(10),
                        ),
                        titleWidget: Row(
                          children: [
                            Text("60.45" " " + "sar".tr(context),
                                style: const TextStyle(
                                    color: GlobalColors.blackText,
                                    fontWeight: FontWeight.w600,
                                    fontSize: 16)),
                            SizedBox(
                              width: ScreensHelper.of(context).fromWidth(30),
                            ),
                            const Text("قيمة المحفظة",
                                style: TextStyle(
                                    color: GlobalColors.blackText,
                                    fontWeight: FontWeight.w600,
                                    fontSize: 16)),
                            SizedBox(
                              width: ScreensHelper.of(context).fromWidth(4),
                            ),
                            Image.asset(GlobalImages.walt),
                          ].reversed.toList(),
                        ),
                      ),
                      SizedBox(
                        height: ScreensHelper.of(context).fromHeight(10),
                      ),
                      WaltInfoWidget(
                        listInfo: {
                          "حذاء كعب عالي": "20.5" " " + "sar".tr(context),
                          "جاكيت نسائي اسود قصير": "40.3" " " + "sar".tr(context),
                          "حقيبة يد جلد": "10.0" " " + "sar".tr(context),
                          "قميص قطن": "13.2" " " + "sar".tr(context)
                        },
                        padding: EdgeInsets.only(
                          top: ScreensHelper.of(context).fromHeight(5),
                          bottom: ScreensHelper.of(context).fromHeight(2),
                          left: ScreensHelper.of(context).fromWidth(3),
                          right: ScreensHelper.of(context).fromWidth(3),
                        ),
                        titleWidget: const Text("تفاصيل المحفظة",
                            style: TextStyle(
                                color: GlobalColors.blackText,
                                fontWeight: FontWeight.w600,
                                fontSize: 17)),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
