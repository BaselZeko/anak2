import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/resources/images.dart';
import 'package:anak/core/ui/animation/fade_tans_animation.dart';
import 'package:anak/features/cart/presentation/pages/cart_page.dart';
import 'package:anak/features/more_pages/presentation/widgets/payment_card_widget.dart';
import 'package:flutter/material.dart';

class AboutAppPage extends StatefulWidget {
  @override
  _AboutAppPageState createState() => _AboutAppPageState();
}

class _AboutAppPageState extends State<AboutAppPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: GlobalColors.primaryColor,
      body: Center(
        child: Container(
            width: ScreensHelper.of(context).fromWidth(90),
            height: ScreensHelper.of(context).fromHeight(85),
            padding: EdgeInsets.symmetric(
                horizontal: ScreensHelper.of(context).fromWidth(4),
                vertical: ScreensHelper.of(context).fromHeight(4)),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: const BorderRadius.all(Radius.circular(12)),
              boxShadow: [
                BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    blurRadius: 2.0,
                    spreadRadius: 1,
                    offset: const Offset(-2, 1) // changes position of shadow
                    ),
              ],
            ),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    "عن التطبيق",
                    style: TextStyle(
                      fontSize: 19,
                      color: GlobalColors.primaryColor,
                      fontWeight: FontWeight.w600,
                    ),
                  ),

                  SizedBox(
                    height: ScreensHelper.of(context).fromHeight(5),
                  ),

                  SizedBox(
                    width: ScreensHelper.of(context).fromWidth(100),
                    child: Center(
                      child: Image.asset(
                        GlobalImages.primary_logo,
                        width: ScreensHelper.of(context).fromWidth(20),
                        height: ScreensHelper.of(context).fromWidth(20),
                      ),
                    ),
                  ),

                  SizedBox(
                    height: ScreensHelper.of(context).fromHeight(3),
                  ),

                  Center(
                    child: SizedBox(
                      width: ScreensHelper.of(context).fromWidth(70),
                      child: const Text(
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                        style: TextStyle(
                          fontSize: 18,
                          color: GlobalColors.blackText,
                          fontWeight: FontWeight.w400,
                        ),
                        textAlign: TextAlign.center,
                        // overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ),
                ],
              ),
            )),
      ),
    );
  }
}
