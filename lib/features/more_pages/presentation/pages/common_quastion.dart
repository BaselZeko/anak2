import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/resources/images.dart';
import 'package:anak/core/ui/animation/fade_tans_animation.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';

class CommonQuastionPage extends StatefulWidget {
  @override
  _CommonQuastionPageState createState() => _CommonQuastionPageState();
}

class _CommonQuastionPageState extends State<CommonQuastionPage> {
  Widget quastionWidget({Function() onTap, String quas, String ans}) {
    return Container(
      decoration: BoxDecoration(
        color: Color(0xffF8F8F8),
        borderRadius: const BorderRadius.all(Radius.circular(12)),
        boxShadow: [
          BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              blurRadius: 1.0,
              spreadRadius: 1,
              offset: const Offset(-2, 1) // changes position of shadow
              ),
        ],
      ),
      child: ExpansionTile(
        
          title: Text(
        quas,
        softWrap: true,
        maxLines: 2,
        overflow: TextOverflow.ellipsis,
            style: const TextStyle(
              fontSize: 19,
              color: GlobalColors.primaryColor,
              fontWeight: FontWeight.w600,
            ),
      ),
        children: [
          Padding(
            padding: EdgeInsets.only(
                left: ScreensHelper.of(context).fromWidth(4),
                right: ScreensHelper.of(context).fromWidth(4),
                bottom: ScreensHelper.of(context).fromHeight(2),
            ),
            child: Text(
              ans,
              softWrap: true,
              style: const TextStyle(
                fontSize: 17,
                color: GlobalColors.blackText,
                fontWeight: FontWeight.w400,
              ),
            ),
          )
        ],
      ) ,

    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: GlobalColors.primaryColor,
      body: Center(
        child: Container(
            width: ScreensHelper.of(context).fromWidth(90),
            height: ScreensHelper.of(context).fromHeight(85),
            padding: EdgeInsets.symmetric(
                horizontal: ScreensHelper.of(context).fromWidth(3),
                vertical: ScreensHelper.of(context).fromHeight(4)),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: const BorderRadius.all(Radius.circular(12)),
              boxShadow: [
                BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    blurRadius: 2.0,
                    spreadRadius: 1,
                    offset: const Offset(-2, 1) // changes position of shadow
                    ),
              ],
            ),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: ScreensHelper.of(context).fromHeight(2),
                  ),
                  const Text(
                    "الأسئلة الشائعة",
                    style: TextStyle(
                      fontSize: 19,
                      color: GlobalColors.primaryColor,
                      fontWeight: FontWeight.w600,
                    ),
                  ),

                  ListView.separated(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: 10,
                    itemBuilder: (context, index) {
                      return FadeTransAnimation(
                        delayInMillisecond: 500 + (100 * index),
                        child: quastionWidget(
                          quas: "orem ipsum dolo ?",
                          ans: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore "
                        ),
                      );
                    }, separatorBuilder: (BuildContext context, int index) {
                      return SizedBox(
                        height: ScreensHelper.of(context).fromHeight(2.5),
                      );
                  },
                  )
                ],
              ),
            )),
      ),
    );
  }
}
