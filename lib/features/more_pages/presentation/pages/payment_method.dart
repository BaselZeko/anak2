import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/resources/images.dart';
import 'package:anak/core/ui/animation/fade_tans_animation.dart';
import 'package:anak/features/cart/presentation/pages/cart_page.dart';
import 'package:anak/features/more_pages/presentation/widgets/payment_card_widget.dart';
import 'package:flutter/material.dart';

class PaymentMethodPage extends StatefulWidget {
  @override
  _PaymentMethodPageState createState() => _PaymentMethodPageState();
}

class _PaymentMethodPageState extends State<PaymentMethodPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            height: ScreensHelper.of(context).fromHeight(100),
            width: ScreensHelper.of(context).fromWidth(100),
            color: GlobalColors.primaryColor,
          ),
          Positioned(
              top: ScreensHelper.of(context).fromHeight(6),
              child: SizedBox(
                  width: ScreensHelper.of(context).fromWidth(100),
                  child: const Center(
                      child: Text(
                    "طرق الدفع",
                    style: TextStyle(fontSize: 22, color: Colors.white),
                  )))),
          Positioned(
              left: ScreensHelper.of(context).fromWidth(43),
              top: ScreensHelper.of(context).fromHeight(6),
              child: SizedBox(
                  width: ScreensHelper.of(context).fromWidth(100),
                  child: Center(
                      child: InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: const Icon(
                            Icons.arrow_back,
                            size: 24,
                            color: Colors.white,
                          ))))),
          Positioned(
            top: ScreensHelper.of(context).fromHeight(12),
            child: ClipPath(
              clipper: BackgroundClipper(),
              child: Container(
                height: ScreensHelper.of(context).fromHeight(90),
                width: ScreensHelper.of(context).fromWidth(100),
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(60),
                    topRight: Radius.circular(60),
                  ),
                ),
                padding: EdgeInsets.symmetric(
                  horizontal: ScreensHelper.of(context).fromWidth(2),
                  vertical: ScreensHelper.of(context).fromHeight(8),
                ),

                child: Center(
                  child: Container(
                    width: ScreensHelper.of(context).fromWidth(89),
                    height: ScreensHelper.of(context).fromHeight(100),
                    padding: EdgeInsets.only(
                        left: ScreensHelper.of(context).fromWidth(2),
                        right: ScreensHelper.of(context).fromWidth(2),
                        bottom:
                            ScreensHelper.of(context).fromHeight(4)),
                    decoration: BoxDecoration(
                      color: Color(0xffF4F4F4),
                      borderRadius:
                          BorderRadius.all(Radius.circular(12)),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            blurRadius: 2.0,
                            spreadRadius: 1,
                            offset: Offset(
                                -2, 1) // changes position of shadow
                            ),
                      ],
                    ),
                    child: ListView.separated(
                      shrinkWrap: true,
                      itemCount: 10,
                      itemBuilder: (context, index) {
                        return FadeTransAnimation(
                          delayInMillisecond: 500 + (100 * index),
                          child: PaymentCardWidget(
                            imageUrl: GlobalImages.mastardCard,
                            cardName: "Visa Card",
                            number: "456644 ****",
                          ),
                        );
                      },
                      separatorBuilder:
                          (BuildContext context, int index) {
                        return SizedBox(
                          height:
                              ScreensHelper.of(context).fromHeight(4),
                          child: const Divider(
                            color: Color(0xffffffff),
                            thickness: 1.5,
                          ),
                        );
                      },
                    ),
                  ),
                ),
                  ),
                ),
            ),
        ],
      ),
    );
  }
}
