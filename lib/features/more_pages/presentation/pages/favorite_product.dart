import 'package:anak/core/services/navigation_service.dart';
import 'package:anak/core/ui/animation/fade_tans_animation.dart';
import 'package:anak/features/home_page/data/model/data.dart';
import 'package:anak/features/home_page/widgets/porducts.dart';
import 'package:anak/features/home_page/widgets/prands_widgets/prand_card.dart';
import 'package:anak/features/more_pages/presentation/widgets/copon_widget.dart';
import 'package:flutter/material.dart';
import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/resources/images.dart';
import 'package:anak/features/cart/presentation/pages/cart_page.dart';
import '../widgets/OptionItemWidget.dart';

class FavoriteProductPage extends StatefulWidget {
  @override
  _FavoriteProductPageState createState() => _FavoriteProductPageState();
}

class _FavoriteProductPageState extends State<FavoriteProductPage> {
  int currentIndex = 0;

  Widget getOption(int index, String option) {
    if (index == currentIndex) {
      return Container(
        width: ScreensHelper.of(context).fromWidth(37),
          padding: EdgeInsets.symmetric(
              vertical: ScreensHelper.of(context).fromHeight(2)),
          decoration: const BoxDecoration(
            border: Border(
              bottom: BorderSide(width: 3.0, color: GlobalColors.primaryColor),
            ),
          ),
          child: Text(
            option,
            style:const  TextStyle(color: GlobalColors.primaryColor,
                fontSize: 19,
                fontWeight: FontWeight.w900),
            textAlign: TextAlign.center,
          ));
    } else {
      return Container(
        padding: EdgeInsets.symmetric(
            vertical: ScreensHelper.of(context).fromHeight(2)),
        width: ScreensHelper.of(context).fromWidth(37),
        child: Text(
          option,
          style: const TextStyle(
            color: GlobalColors.blackText,
            fontSize: 19,
            fontWeight: FontWeight.w900,
          ),
          textAlign: TextAlign.center,
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            height: ScreensHelper.of(context).fromHeight(100),
            width: ScreensHelper.of(context).fromWidth(100),
            color: GlobalColors.primaryColor,
          ),
          Positioned(
              top: ScreensHelper.of(context).fromHeight(6),
              child: SizedBox(
                  width: ScreensHelper.of(context).fromWidth(100),
                  child: const Center(
                      child: Text(
                    "المفضلة",
                    style: TextStyle(fontSize: 22, color: Colors.white),
                  )))),
          Positioned(
              left: ScreensHelper.of(context).fromWidth(43),
              top: ScreensHelper.of(context).fromHeight(6),
              child: SizedBox(
                  width: ScreensHelper.of(context).fromWidth(100),
                  child: Center(
                      child: InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: const Icon(
                            Icons.arrow_back,
                            size: 24,
                            color: Colors.white,
                          ))))),
          Positioned(
            top: ScreensHelper.of(context).fromHeight(12),
            child: ClipPath(
              clipper: BackgroundClipper(),
              child: Container(
                height: ScreensHelper.of(context).fromHeight(90),
                width: ScreensHelper.of(context).fromWidth(100),
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(60),
                      topRight: Radius.circular(60),
                    )),
                padding: EdgeInsets.symmetric(
                    vertical: ScreensHelper.of(context).fromHeight(5),
                    horizontal: ScreensHelper.of(context).fromWidth(5)),
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      SizedBox(
                        height: ScreensHelper.of(context).fromHeight(3),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          InkWell(
                              onTap: () {
                                setState(() {
                                  currentIndex = 0;
                                });
                              },
                              child: getOption(0, "المنتجات")),
                          InkWell(
                              onTap: () {
                                setState(() {
                                  currentIndex = 1;
                                });
                              },
                              child: getOption(1, "المتاجر")),
                        ],
                      ),
                      
                      SizedBox(
                        height: ScreensHelper.of(context).fromHeight(2),
                      ),

                      currentIndex==1?Center(
                        child: ListView.separated(
                          physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: 10,
                          itemBuilder: (context, index) {
                            return FadeTransAnimation(
                              delayInMillisecond: 500 + (100 * index),
                                child: PrandCardWidget(isFav: true,),
                            );
                          }, separatorBuilder: (BuildContext context, int index) {
                            return SizedBox(
                              height: ScreensHelper.of(context).fromHeight(2),
                              child: const Divider(
                                color: Color(0xffeeeeee),
                                thickness: 1.2,
                              ),
                            );
                        },
                        ),
                      ):Center(
                        child: GridView.builder(
                            padding: const EdgeInsets.only(bottom: 60),
                            physics: const BouncingScrollPhysics(),
                            shrinkWrap: true,
                            gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2,
                              childAspectRatio: 0.65,
                            ),
                            itemCount: productList.length,
                            itemBuilder: (context, index) {
                              return Transform.translate(
                                offset: const Offset(0.0, 15),
                                child: FadeTransAnimation(
                                  delayInMillisecond: 500 + (100 * index),
                                  child: Product(
                                    index: index,
                                  ),
                                ),
                              );
                            }),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
