import 'package:anak/core/services/navigation_service.dart';
import 'package:anak/core/ui/animation/fade_tans_animation.dart';
import 'package:anak/features/more_pages/presentation/widgets/copon_widget.dart';
import 'package:flutter/material.dart';
import 'package:anak/core/helper/screen_util/screen_helper.dart';
import 'package:anak/core/resources/colors.dart';
import 'package:anak/core/resources/images.dart';
import 'package:anak/features/cart/presentation/pages/cart_page.dart';
import '../widgets/OptionItemWidget.dart';

class CoponsPage extends StatefulWidget {
  @override
  _CoponsPageState createState() => _CoponsPageState();
}

class _CoponsPageState extends State<CoponsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            height: ScreensHelper.of(context).fromHeight(100),
            width: ScreensHelper.of(context).fromWidth(100),
            color: GlobalColors.primaryColor,
          ),
          Positioned(
              top: ScreensHelper.of(context).fromHeight(6),
              child: SizedBox(
                  width: ScreensHelper.of(context).fromWidth(100),
                  child: const Center(
                      child: Text(
                        "الكوبونات",
                        style: TextStyle(fontSize: 22, color: Colors.white),
                      )))),

          Positioned(
            left: ScreensHelper.of(context).fromWidth(43),
              top: ScreensHelper.of(context).fromHeight(6),
              child: SizedBox(
                  width: ScreensHelper.of(context).fromWidth(100),
                  child: Center(
                      child: InkWell(onTap: (){
                        Navigator.pop(context);
                      }, child: const Icon(Icons.arrow_back,size: 24,color: Colors.white,))))),

          Positioned(
            top: ScreensHelper.of(context).fromHeight(12),
            child: ClipPath(
              clipper: BackgroundClipper(),
              child: Container(
                height: ScreensHelper.of(context).fromHeight(90),
                width: ScreensHelper.of(context).fromWidth(100),
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(60),
                      topRight: Radius.circular(60),
                    )),
                child: Center(
                  child: Container(
                    padding: EdgeInsets.symmetric(
                        horizontal: ScreensHelper.of(context).fromWidth(2),
                        vertical: ScreensHelper.of(context).fromHeight(7)
                    ),
                    child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: 10,
                      itemBuilder: (context, index) {
                        return FadeTransAnimation(
                          delayInMillisecond: 500 + (100 * index),
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: ScreensHelper.of(context).fromWidth(2),
                                vertical: ScreensHelper.of(context).fromHeight(1),
                            ),
                            child: CoponWidget(),
                          ),
                        );
                      },
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
