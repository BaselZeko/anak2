import 'package:anak/app.dart';
import 'package:flutter/material.dart';

import 'di.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const MyApp());
}
