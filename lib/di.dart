// import 'package:get_it/get_it.dart';
//
// import 'core/services/device_uid_provider.dart';
// import 'core/services/geocoder_service.dart';
// import 'core/services/user_local_management.dart';
//
// import 'features/user_managment/data/datasources/concrete_user_remote_data_source.dart';
// import 'features/user_managment/data/repositories/concrete_user_repository.dart';
//
// final sl = GetIt.instance;
//
// Future<void> setupInjector() async {
//   sl.registerLazySingleton<ConcreteUserRepository>(() => ConcreteUserRepository(
//       remoteDataSource: sl<ConcreteUserRemoteDataSource>(),
//       userLocalManagement: sl<UserLocalManagement>()));
//
//   sl.registerLazySingleton<ConcreteUserRemoteDataSource>(
//       () => ConcreteUserRemoteDataSource());
//
//   sl.registerLazySingleton<UserLocalManagement>(() => UserLocalManagement());
//
//   sl.registerLazySingleton<DeviceUidProvider>(() => DeviceUidProvider());
//
// }
